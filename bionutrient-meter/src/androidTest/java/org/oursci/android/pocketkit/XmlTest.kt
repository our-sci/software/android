package org.oursci.android.pocketkit

import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.oursci.android.common.survey.SurveyLoader
import java.io.File

/**
 * Created by Manuel Di Cerbo on 29.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
@RunWith(AndroidJUnit4::class)
class XmlTest {
    @Test
    fun metaReader() {
        val reader = SurveyLoader.MetaReader(File("/sdcard/our_scikit/surveys/reflectometer for bfa/instances/instance_2017-10-27 17:45:46.xml"))
        val modified = reader.read("created")
        println("created: %s".format(modified))

        val uploaded = reader.read("uploaded")
        println("uploaded: %s".format(uploaded))
    }
}
