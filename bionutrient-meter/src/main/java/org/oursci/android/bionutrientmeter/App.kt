package org.oursci.android.bionutrientmeter

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.oursci.android.bionutrientmeter.ui.AppIntroViewModel
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.BaseNavigationManager
import org.oursci.android.common.ui.welcome.WelcomeAdapter

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
class App : BaseApplication() {

    override val filesDirName = "bionutrient-meter"

    override val WEB_SERVER_PORT = 9095
    override val navigationManager: BaseNavigationManager = NavigationManager(R.id.content_main)
    override val TAG = "Bionutrient Meter"
    override val allowOrganizations: Boolean = false
    override val hasLegacyOfflineSurveys: Boolean = false
    override val organizationURL = "https://app.our-sci.net/#/organization/real-food-campaign-community"
    override val defaultOrganizationId = "real-food-campaign-community"


    override val limitSurveysToOrganization = arrayOf(
            "real-food-campaign-community"
    )

    override val viewModelFactory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AppIntroViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return AppIntroViewModel(this@App) as T
            }

            return super@App.createViewModel(modelClass)
        }
    }

    override val welcomeItems = listOf(
            WelcomeAdapter.WelcomeItem(
                    title = "Milk Taste Test",
                    subtitle = "Community Taste Test Experiment",
                    leftAction = "Metadata Form" to {
                        navigationManager.navigateByIdOrTitle("build_Milk-Metadata-Form_1542313859" to "Milk Metadata Form")
                    },
                    middleAction = "Milk Test" to {
                        navigationManager.navigateByIdOrTitle("build_Milk-Taste-Test-Form_1542313319" to "Milk Taste Test Form")
                    },
                    rightAction = "Results" to {
                        openWebsite("https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-milk-taste")
                    }
            ),
            WelcomeAdapter.WelcomeItem(
                    title = "Fruit Taste Test",
                    subtitle = "Community Taste Test Experiment",
                    leftAction = "Metadata Form" to {
                        navigationManager.navigateByIdOrTitle("build_Fruit-Metadata-Form_1542217066" to "Fruit Metadata Form")
                    },
                    middleAction = "Fruit Test" to {
                        navigationManager.navigateByIdOrTitle("build_Fruit-Taste-Test-Form_1542217511" to "Fruit Taste Test Form")
                    },
                    rightAction = "Results" to {
                        openWebsite("https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-fruit-taste")

                    }
            ),
            WelcomeAdapter.WelcomeItem(
                    title = "Organic Matter",
                    subtitle = "Map Organic Matter in Your Fields",
                    leftAction = "Organic Matter Mapping Form" to {
                        navigationManager.navigateByIdOrTitle("build_Organic-Matter-Mapping-Form_1542826200" to "Organic Matter Mapping Form")
                    },
                    middleAction = "Results" to {
                        openWebsite("https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-organic-matter")
                    }
            ),
            WelcomeAdapter.WelcomeItem(
                    title = "Documentation",
                    subtitle = "Online Documentation on Experiments",
                    leftAction = "Open" to {
                        openWebsite("https://our-sci.gitlab.io/documentation/#/home")
                    }
            )
    )

    private fun openWebsite(website: String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(website))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

}


