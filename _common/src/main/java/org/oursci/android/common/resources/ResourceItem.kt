package org.oursci.android.common.resources

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 21.03.19.
 */
data class ResourceItem(val id: String, val name: String, val description: String)