package org.oursci.android.common.ui.survey.external

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.survey.ControlViewModel

/**
 * Created by Manuel Di Cerbo on 16.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class ExternalViewModel(app: BaseApplication) : ControlViewModel<ExternalControl>(app) {
}