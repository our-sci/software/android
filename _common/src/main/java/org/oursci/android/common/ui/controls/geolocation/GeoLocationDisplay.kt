package org.oursci.android.common.ui.controls.geolocation

import android.text.Spanned
import org.oursci.android.common.html
import org.oursci.android.common.vo.survey.geolocation.GeoLocationAnswer

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
open class GeoLocationDisplay(
        val control: GeoLocationControl
) {
    var progressText: String = ""
    var progress: Int = 0


    fun textTitle(): Spanned {
        return html("${control.group?.description?.let { "<small><font color='black'>$it</small><br>" }
                ?: ""}${control.label}")
    }

    open fun hint(): Spanned = html("${control.hint}<br>" +
            "${control.answer()?.split(";")?.take(2)?.joinToString()}")

}