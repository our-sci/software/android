package org.oursci.android.common.ui.devices

import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentSerialDebugBinding
import org.oursci.android.common.ui.BaseFragment
import android.text.method.ScrollingMovementMethod
import android.view.inputmethod.EditorInfo
import org.oursci.android.common.R
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import android.content.Context.CLIPBOARD_SERVICE
import android.widget.Toast
import org.oursci.android.common.app


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.10.17.
 */
class DeviceDebugFragment : BaseFragment() {

    lateinit var binding: FragmentSerialDebugBinding
    val viewModel by lazyViewModel<DeviceDebugViewModel>()

    private val refreshObserver = ConsumableObserver<Void> { t: Void?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        binding.textView.text = ""
        binding.textView.append(viewModel.data)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.refreshResult.observe(this, refreshObserver)
        viewModel.refreshLog()
        viewModel.registerCallbacks()

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup??, savedInstanceState: Bundle?): View? {
        binding = FragmentSerialDebugBinding.inflate(inflater, container, false)
        binding.btRefresh.setOnClickListener {
            viewModel.refreshLog()
        }

        binding.tvSend.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                val ending = when (binding.grpLineEnding.checkedRadioButtonId) {
                    R.id.radio_none -> ""
                    R.id.radio_n -> "\n"
                    R.id.radio_rn -> "\r\n"
                    else -> ""
                }
                viewModel.sendData("%s%s".format(v.text.toString(), ending))
                v.text = ""
                v.invalidate()
                false
            } else true
        }
        binding.textView.movementMethod = ScrollingMovementMethod()
        binding.textView.setOnLongClickListener {
            val clipboard = app().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = ClipData.newPlainText("debug output", viewModel.data.toString())
            Toast.makeText(app(), "Copied to Clipboard", Toast.LENGTH_SHORT).show()
            true
        }
        return binding.root
    }
}