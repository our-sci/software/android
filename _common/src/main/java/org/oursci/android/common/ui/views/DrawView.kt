package org.oursci.android.common.ui.views

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */


/*
 * Original File of ODK Collect
 * Copyright 2017 Nafundi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



class DrawView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var isSignature: Boolean = false
    private var bitmap: Bitmap? = null
    private var canvas: Canvas? = null

    private var currentPath: Path? = null
    private var offscreenPath: Path? = null // Adjusted for position of the bitmap in the view

    private var bitmapPaint: Paint? = null
    private var paint: Paint? = null
    private var pointPaint: Paint? = null

    private var backgroundBitmapFile: File? = null

    private var valueX: Float = 0.toFloat()
    private var valueY: Float = 0.toFloat()

    var color = 0xFF000000.toInt()
        set(color) {
            field = color
            paint!!.color = color
            pointPaint!!.color = color
        }

    fun setupView(c: Context, isSignature: Boolean, f: File) {
        this.isSignature = isSignature
        backgroundBitmapFile = f

        bitmapPaint = Paint(Paint.DITHER_FLAG)
        currentPath = Path()
        offscreenPath = Path()
        backgroundBitmapFile = File("background.png")

        paint = Paint()
        paint!!.isAntiAlias = true
        paint!!.isDither = true
        paint!!.color = color
        paint!!.style = Paint.Style.STROKE
        paint!!.strokeJoin = Paint.Join.ROUND
        paint!!.strokeWidth = 10f

        pointPaint = Paint()
        pointPaint!!.isAntiAlias = true
        pointPaint!!.isDither = true
        pointPaint!!.color = color
        pointPaint!!.style = Paint.Style.FILL_AND_STROKE
        pointPaint!!.strokeWidth = 10f
    }

    fun reset() {
        val metrics = resources.displayMetrics
        val screenWidth = metrics.widthPixels
        val screenHeight = metrics.heightPixels
        resetImage(screenWidth, screenHeight)
    }

    fun resetImage(w: Int, h: Int) {
        var w = w
        var h = h
        if (backgroundBitmapFile != null && backgroundBitmapFile!!.exists()) {
            // Because this activity is used in a fixed landscape mode only, sometimes resetImage()
            // is called upon with flipped w/h (before orientation changes have been applied)
            if (w > h) {
                val temp = w
                w = h
                h = temp
            }

            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            // bitmap =
            // Bitmap.createScaledBitmap(BitmapFactory.decodeFile(backgroundBitmapFile.getDir()),
            // w, h, true);
            canvas = Canvas(bitmap!!)
        } else {
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            canvas = Canvas(bitmap!!)
            canvas!!.drawColor(0xFFFFFFFF.toInt())
            if (isSignature) {
                drawSignLine()
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        resetImage(w, h)
    }

    override fun onDraw(canvas: Canvas) {
        drawOnCanvas(canvas, bitmapLeft.toFloat(), bitmapTop.toFloat())
    }

    fun drawOnCanvas(canvas: Canvas, left: Float, top: Float) {
        canvas.drawColor(0xFFAAAAAA.toInt())
        canvas.drawBitmap(bitmap!!, left, top, bitmapPaint)
        canvas.drawPath(currentPath!!, paint!!)
    }

    private fun touch_start(x: Float, y: Float) {
        currentPath!!.reset()
        currentPath!!.moveTo(x, y)

        offscreenPath!!.reset()
        offscreenPath!!.moveTo(x - bitmapLeft, y - bitmapTop)

        valueX = x
        valueY = y
    }

    fun drawSignLine() {
        canvas!!.drawLine(0f, (canvas!!.height * .7).toInt().toFloat(),
                canvas!!.width.toFloat(), (canvas!!.height * .7).toInt().toFloat(), paint!!)
    }

    private fun touch_move(x: Float, y: Float) {
        currentPath!!.quadTo(valueX, valueY, (x + valueX) / 2, (y + valueY) / 2)
        offscreenPath!!.quadTo(valueX - bitmapLeft, valueY - bitmapTop,
                (x + valueX) / 2 - bitmapLeft, (y + valueY) / 2 - bitmapTop)
        valueX = x
        valueY = y
    }

    private fun touch_up() {
        if (currentPath!!.isEmpty) {
            canvas!!.drawPoint(valueX, valueY, pointPaint!!)
        } else {
            currentPath!!.lineTo(valueX, valueY)
            offscreenPath!!.lineTo(valueX - bitmapLeft, valueY - bitmapTop)

            // commit the dir to our offscreen
            canvas!!.drawPath(offscreenPath!!, paint!!)
        }
        // kill this so we don't double draw
        currentPath!!.reset()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                touch_start(x, y)
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                touch_move(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                touch_up()
                invalidate()
            }
        }
        return true
    }

    val bitmapHeight: Int
        get() = bitmap!!.height

    val bitmapWidth: Int
        get() = bitmap!!.width

    private // Centered horizontally
    val bitmapLeft: Int
        get() = (width - bitmap!!.width) / 2

    private // Centered vertically
    val bitmapTop: Int
        get() = (height - bitmap!!.height) / 2
}