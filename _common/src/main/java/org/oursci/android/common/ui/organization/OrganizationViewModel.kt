package org.oursci.android.common.ui.organization

import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.organization.OrganizationDb
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.vo.Organization
import timber.log.Timber
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 09.05.18.
 */
class OrganizationViewModel(app: BaseApplication) : BaseViewModel(app) {

    val isLoading = AtomicBoolean(false)
    val loadResult = ConsumableLiveData<List<Organization>>()

    fun loadOrganizations() {
        thread {
            var token: String? = null
            FirebaseAuth.getInstance()?.currentUser?.getIdToken(false)?.let {
                try {
                    token = Tasks.await(it, 10, TimeUnit.SECONDS).token
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            val db = OrganizationDb.of(app).db.orgainzationDao()
            try {
                token?.let {
                    val response = app.api.fetchOrganizations(it).execute()
                    response.body()?.let {
                        db.deleteAllInfos()
                        db.insertOrganizations(it.toList())

                        app.getCurrentContributionOrg()?.let { persisted ->
                            it.find { it.organizationId == persisted.id }
                                    ?: app.setCurrentContributionOrg(null)
                        }

                        loadResult.postValue(it.toList())
                    }
                } ?: loadResult.postValue(emptyList())
            } catch (e: Exception) {
                e.printStackTrace()
                loadResult.postValue(
                        db.getAllOrganizations()
                )
            }
        }
    }

}