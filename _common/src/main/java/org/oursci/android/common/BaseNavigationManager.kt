package org.oursci.android.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.core.view.ViewCompat
import android.view.View
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.survey.SurveyDb
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.IBackPressable
import org.oursci.android.common.ui.survey.browse.BrowseSurveysFragment
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.external.ExternalControlAdapter
import org.oursci.android.common.ui.controls.geolocation.GeoLocationAdapter
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.measurement.MeasurementAdapter
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceAdapter
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextAdapter
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.ui.devices.DeviceDebugFragment
import org.oursci.android.common.ui.devices.DeviceInfoFragment
import org.oursci.android.common.ui.devices.DeviceListFragment
import org.oursci.android.common.ui.envvar.EnvVarListFragment
import org.oursci.android.common.ui.geopicker.GeoPickerFragment
import org.oursci.android.common.ui.measurement.results.MeasurementResultListFragment
import org.oursci.android.common.ui.measurement.scripts.MeasurementScriptListFragment
import org.oursci.android.common.ui.organization.OrganizationListFragment
import org.oursci.android.common.ui.profile.ProfileFragment
import org.oursci.android.common.ui.qrscanner.QrScannerFragment
import org.oursci.android.common.ui.survey.geolocation.GeoLocationFragment
import org.oursci.android.common.ui.survey.geolocation.GeoLocationResultFragment
import org.oursci.android.common.ui.survey.group.GroupFragment
import org.oursci.android.common.ui.survey.list.SurveyListFragment
import org.oursci.android.common.ui.survey.list.SurveyResultListFragment
import org.oursci.android.common.ui.survey.loading.LoadingFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementResultFragment
import org.oursci.android.common.ui.survey.overview.SurveyOverviewFragment
import org.oursci.android.common.ui.transition.Direction
import org.oursci.android.common.vo.Organization
import org.oursci.android.common.vo.survey.SurveyListItem
import org.oursci.android.common.webapi.Api
import org.w3c.dom.Text
import timber.log.Timber
import java.io.File
import kotlin.concurrent.thread


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 02.07.17.
 */
abstract class BaseNavigationManager(private val mainContainer: Int, var mainActivity: BaseActivity? = null) {

    private var nextDirection: Direction = Direction.FROM_RIGHT
    private var popping = false
    val keyPopDirection = "_pop_direction"
    var popDirection = Direction.FROM_RIGHT.reverse()

    var disableEntryAnimation: Boolean = false


    private val fm: FragmentManager
        get() = mainActivity!!.supportFragmentManager


    companion object {
        val TAG_FINAL_OVERVIEW: String = "__survey_final_overview"
        val TAG_OVERVIEW: String = "__survey_overview"
        val TAG_SURVEY_START = "__survey_start"
        val TAG_SURVEY_PREFIX = "__survey_"
        val RESULT_LIST = "__survey_result_list"


        fun of(app: BaseApplication): BaseNavigationManager = app.navigationManager
    }


    fun currentFragment(): BaseFragment? {
        return try {
            fm.findFragmentById(mainContainer) as? BaseFragment
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }


    fun Fragment.nav(
            args: Bundle? = null,
            direction: Direction = Direction.FROM_RIGHT,
            popDirection: Direction? = null,
            name: String? = null,
            sharedElement: Pair<View, String>? = null,
            disableEntryAnimation: Boolean = false) {


        disableEntryAnimation(disableEntryAnimation)

        if (mainActivity == null || mainActivity!!.supportFragmentManager == null) {
            return
        }

        try {
            Answers.getInstance().logContentView(ContentViewEvent()
                    .putContentId(this::class.java.simpleName))
        } catch (e: Exception) {
            Timber.e(e)
        }



        popping = false

        arguments = (arguments ?: Bundle()).apply {
            args?.let(this::putAll)
            putString(keyPopDirection, popDirection?.name ?: direction.reverse().name)
        }


        direction(direction)

        val transaction = fm.beginTransaction()


        if (sharedElement != null) {
            ViewCompat.setTransitionName(sharedElement.first, sharedElement.second)
            transaction?.addSharedElement(sharedElement.first, sharedElement.second)
        }

        transaction.setReorderingAllowed(true)

        transaction.replace(mainContainer, this)

        transaction?.addToBackStack(name)

        mainActivity?.fragmentDefaults()

        transaction?.commit()
    }


    fun registerActivity(mainActivity: BaseActivity) {
        this.mainActivity = mainActivity
//        mainActivity.supportFragmentManager.addOnBackStackChangedListener(backstackListener)
    }

    fun unregisterActivity() {
//        mainActivity?.supportFragmentManager?.removeOnBackStackChangedListener(backstackListener)
        mainActivity = null
    }


    fun logout() {
        mainActivity?.supportFragmentManager?.popBackStack()

        mainActivity?.supportFragmentManager?.fragments?.forEach {
            if (it is ProfileFragment) {
                mainActivity?.supportFragmentManager?.beginTransaction()?.remove(it)?.commit()
            }
        }

    }

    fun profile() {
        val profileFragment = ProfileFragment()
        profileFragment.nav()
    }


    fun     startSurvey(fromSnack: Boolean = false, answers: Map<String, String>? = null) {

        val survey = mainActivity?.let {
            SurveyManager.of(it.app).currentSurvey
        }

        if (survey != null && answers != null) {
            survey.controls.forEach { control ->
                answers.forEach { pair ->
                    if (control.dataName == "/data/%s:label".format(pair.key)) {
                        when (control) {
                            is TextControl -> {
                                control.answer = control.answer.create(pair.value)
                            }
                            is MultipleChoiceControl -> {
                                control.answer = control.answer.create(pair.value.split(";").toTypedArray())
                            }
                            is ExternalControl -> {
                                control.answer = control.answer.create(pair.value)
                            }
                            is MeasurementControl -> {
                                control.answer = control.answer.create(pair.value)
                            }
                            is GeoLocationControl -> {
                                control.answer = control.answer.create(pair.value)
                            }
                        }
                    }
                }

            }
        }

        val fragment = try {
            resolveFragment(0)
        } catch (e: Exception) {
            Timber.e(e)
            null
        }

        if (fragment == null) {
            Timber.e("unable to resolve fragment for control idx, " +
                    "missing a call for adding it to the registry?")
            return
        }

        val args = Bundle()
        args.putInt("idx", 0)

        if (fromSnack) {
            fm.popBackStack("home", 0)
            navigateToSurveyResultsList(false)
        } else {
            fm.popBackStack()
        }
        fragment.nav(name = TAG_SURVEY_START, args = args)
    }

    fun surveyOverview() {
        if (mainActivity == null || mainActivity!!.supportFragmentManager == null) {
            return
        }


        val idx = surveyItemIndexFromBackstack()
        if (idx >= 0) {
            try {
                rebuildSurveyStack(idx)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }


        val surveyOverviewFragment = SurveyOverviewFragment()
        surveyOverviewFragment.nav(direction = Direction.FROM_LEFT)
    }

    private fun handleSurveyBackPressed(): Boolean {

        val entry = fm.getBackStackEntryAt(fm.backStackEntryCount - 1)

        if (entry.name == null) {
            Timber.d("entry is null at index ${fm.backStackEntryCount - 1}")
            return false
        }

        Timber.d("name of backstack item %s", entry.name)

        return when (FragmentType.from(entry.name)) {
            BaseNavigationManager.FragmentType.OVERVIEW -> {
                val idx = surveyItemIndexFromBackstack()
                rebuildSurveyStack(idx)
                true
            }
            BaseNavigationManager.FragmentType.SURVEY_START -> {
                fm.popBackStack()
                true
            }
            BaseNavigationManager.FragmentType.SURVEY_ITEM -> {
                fm.popBackStack()
                true
            }
            BaseNavigationManager.FragmentType.UNKNOWN -> false
            null -> false
        }
    }

    private fun surveyItemIndexFromBackstack(): Int {

        val stopIdx = (fm.backStackEntryCount - 1 downTo 0).firstOrNull {
            fm.getBackStackEntryAt(it).name?.startsWith(TAG_SURVEY_PREFIX) ?: false
        } ?: return -1

        val startIdx = (fm.backStackEntryCount - 1 downTo 0).firstOrNull {
            fm.getBackStackEntryAt(it).name == TAG_SURVEY_START
        } ?: return -1

        return stopIdx - startIdx
    }


    enum class FragmentType {
        OVERVIEW,
        SURVEY_START,
        SURVEY_ITEM,
        UNKNOWN;

        companion object {
            fun from(name: String?): FragmentType {
                name ?: return UNKNOWN

                return when (name) {
                    TAG_SURVEY_START -> SURVEY_START
                    TAG_OVERVIEW -> OVERVIEW
                    prefix(name, TAG_SURVEY_PREFIX) -> SURVEY_ITEM
                    else -> UNKNOWN
                }
            }

            private fun prefix(name: String, prefix: String): String {
                if (name.startsWith(prefix)) {
                    return prefix
                }
                return ""
            }
        }
    }

    fun moveToNextSurveyQuestion(idx: Int) {


        // relevance
        val nextIdx = relevantIndex(idx)
        if (nextIdx == -1) { // no more relevant question
            finalOverview()
            return
        }

        val fragment = resolveFragment(nextIdx)

        if (fragment == null) {
            Timber.e("unable to resolve fragment for control idx, " +
                    "missing a call for adding it to the registry?")
            return
        }

        val args = Bundle()
        args.putInt("idx", nextIdx)

        fragment.nav(name = "$TAG_SURVEY_PREFIX$nextIdx", args = args)
    }

    private fun relevantIndex(currentIndex: Int): Int {

        val survey = mainActivity?.let {
            SurveyManager.of(it.app).currentSurvey
        } ?: return -1

        for (idx in currentIndex until survey.controls.size) {
            val control = survey.controls[idx]

            val formIndex = survey.indexMap.entries.find { it.key.dataName == control.dataName }?.value
                    ?: continue

            if (survey.fec.model.isIndexRelevant(formIndex)) {
                return idx
            } else {
                control.answer()?.let {
                    if (it.isNotBlank()) {
                        return idx
                    }
                }
            }
        }

        return -1
    }


    fun movePastGroup(idx: Int) {
        val controls = SurveyManager.of(mainActivity!!.app).currentSurvey?.controls
                ?: return moveToNextSurveyQuestion(idx)
        val current = controls.getOrNull(idx) ?: return moveToNextSurveyQuestion(idx)
        val grpid = current.group?.id ?: return moveToNextSurveyQuestion(idx)

        val target = controls.slice(idx until controls.size).find {
            it.group?.let {
                it.id != grpid
            } ?: true
        }?.let {
            controls.indexOf(it)
        } ?: return finalOverview()


        rebuildSurveyStack(target)

    }

    fun rebuildSurveyStack(idx: Int, sharedView: View? = null, pop: Boolean = false,
                           moveToFirstControlInGroup: Boolean = false,
                           autoRun: Boolean = false) {
        Timber.d("rebuilding survey stack with idx: %d", idx)

        val finalOverview = (fm.getBackStackEntryAt(fm.backStackEntryCount - 1)?.name
                ?: "") == TAG_FINAL_OVERVIEW
        val direction = if (pop) Direction.FROM_LEFT else Direction.FROM_RIGHT
        val popDirection = if (pop) Direction.FROM_LEFT else direction.reverse()


        fm.popBackStack(TAG_SURVEY_START, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fm.popBackStack(RESULT_LIST, 0)

        for (i in 0..idx) {
            val fragment = resolveFragment(i, false)
            val groupFragment = resolveFragment(i, true) as? GroupFragment

            if (fragment == null) {
                Timber.e("unable to resolve fragment for control idx, " +
                        "missing a call for adding it to the registry?")
                return
            }
            val args = Bundle()
            args.putInt("idx", i)
            args.putBoolean("autoRun", autoRun)

            val sharedElement = sharedView?.let { sharedView to "heading" }

            Timber.d("adding fragment with idx: %d", i)

            val navigated = groupFragment?.nav(
                    name = "$TAG_SURVEY_PREFIX$i",
                    args = args,
                    direction = direction,
                    popDirection = popDirection,
                    sharedElement = sharedElement
            )?.let { true } ?: false

            if (!moveToFirstControlInGroup && navigated && i == idx) { // if the last fragment is inside a group, navigate to the group instead
                break
            }


            fragment.nav(
                    name = if (i == 0) TAG_SURVEY_START else "$TAG_SURVEY_PREFIX$i",
                    args = args,
                    direction = direction,
                    popDirection = popDirection,
                    sharedElement = sharedElement
            )
        }
    }

    private fun resolveFragment(surveyIndex: Int, withGroups: Boolean? = null): BaseFragment? {

        val idx = surveyIndex
        val app = mainActivity!!.app
        val survey = SurveyManager.of(mainActivity!!.app).currentSurvey ?: return null
        val controls = survey.controls
        val model = survey.fec.model


        if (withGroups ?: (currentFragment() !is GroupFragment)) {

            if (idx == 0) {
                controls.getOrNull(0)?.let {
                    if (it.group != null) {
                        return GroupFragment()
                    }
                }
            }

            controls.getOrNull(idx - 1)?.let { before ->
                controls.getOrNull(idx)?.let { after ->
                    after.group?.let { grpAfter ->
                        before.group?.let { grpBefore ->
                            if (grpBefore.id != grpAfter.id) {
                                return GroupFragment()
                            }
                        } ?: return GroupFragment()
                    }
                }
            }
        }

        return controls.getOrNull(idx)?.let { control ->
            when (control) {
                is TextControl -> TextAdapter
                is MeasurementControl -> MeasurementAdapter
                is MultipleChoiceControl -> MultipleChoiceAdapter
                is GeoLocationControl -> GeoLocationAdapter
                is ExternalControl -> ExternalControlAdapter
                else -> null
            }?.resolve(app, control)
        }
    }

    fun finalOverview(autoRun: Boolean = false) {

        val fragment = SurveyOverviewFragment()
        val args = Bundle()
        args.putBoolean("final", true)
        args.putBoolean("autoRun", autoRun)

        fragment.nav(args = args, name = TAG_FINAL_OVERVIEW, direction = Direction.FROM_RIGHT)
    }


    fun navigateToSurveyList() {
        val surveyListFragment = SurveyListFragment()
        surveyListFragment.nav(name = "survey_list")
    }


    fun loadSurvey(sharedView: View? = null, item: Api.SurveyBody? = null, answers: String? = null) {

        val surveyLoadFragment = LoadingFragment()

//        val sharedElement = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            surveyLoadFragment.enterTransition = Slide(Gravity.RIGHT)
//            surveyLoadFragment.exitTransition = Slide(Gravity.LEFT)
//            surveyLoadFragment.reenterTransition = Slide(Gravity.LEFT)
//            surveyLoadFragment.returnTransition = Slide(Gravity.RIGHT)
//            surveyLoadFragment.sharedElementEnterTransition = AutoTransition()
//            sharedView?.let {
//                it to "heading"
//            }
//        } else null


        val bundle = Bundle()
        bundle.putString("title", item?.formTitle ?: "")
        bundle.putString("description", item?.description ?: "")
        // bundle.putString("description", descriptionItem?.description ?: "")
        bundle.putString("survey_name", item?.formId ?: "")
        bundle.putString("survey_id", item?.formId ?: "")
        answers?.let { bundle.putString("answers", it) }


        surveyLoadFragment.nav(args = bundle)
    }

    fun loadSurvey(sharedView: View? = null, descriptionItem: SurveyListItem? = null) {

        val surveyLoadFragment = LoadingFragment()

//        val sharedElement = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            surveyLoadFragment.enterTransition = Slide(Gravity.RIGHT)
//            surveyLoadFragment.exitTransition = Slide(Gravity.LEFT)
//            surveyLoadFragment.reenterTransition = Slide(Gravity.LEFT)
//            surveyLoadFragment.returnTransition = Slide(Gravity.RIGHT)
//            surveyLoadFragment.sharedElementEnterTransition = AutoTransition()
//            sharedView?.let {
//                it to "heading"
//            }
//        } else null


        val bundle = Bundle()
        bundle.putString("title", descriptionItem?.title ?: "")
        bundle.putString("description", descriptionItem?.description ?: "")
        bundle.putString("survey_name", descriptionItem?.surveyName ?: "")
        bundle.putString("survey_id", descriptionItem?.surveyId ?: "")


        surveyLoadFragment.nav(args = bundle)
    }

    fun navigateToMeasurement(measurementId: String, toResult: Boolean = false, instanceFile: File? = null,
                              idx: Int = -1, autoRun: Boolean = false) {
        val measurementFragment = if (toResult) MeasurementResultFragment() else MeasurementFragment()


        val bundle = Bundle()
        bundle.putString("id", measurementId)

        if (toResult && instanceFile == null && idx >= 0) {
            val surveyManager = SurveyManager.of(mainActivity!!.app)
            val measurementLoader = MeasurementLoader.of(mainActivity!!.app)

            val filePath = surveyManager.currentSurvey?.let { survey ->
                survey.controls[idx].let {
                    (it as MeasurementControl).let {
                        val answer = it.answer.answer ?: ""
                        val fileName = resolvePathFromMeasurementAnswer(answer)
                        measurementLoader.resolveMeasurementFile(it.id, fileName)?.absolutePath
                                ?: ""

                    }
                }
            } ?: ""
            bundle.putString("file", filePath)
        } else {
            bundle.putString("file", instanceFile?.absolutePath ?: "")
        }
        bundle.putInt("idx", idx)
        bundle.putBoolean("autoRun", autoRun)

        if (idx != -1) {
            fm.popBackStack()
        }

        measurementFragment.nav(args = bundle, name = if (toResult) "measurement_result" else "measurement_script")
    }

    fun navigateToSummaryFragment() {
        val measurementFragment = MeasurementResultFragment()
        val bundle = Bundle()
        val id = SurveyManager.of(mainActivity!!.app).currentSurvey?.summaryScript ?: return
        bundle.putString("id", id)
        bundle.putBoolean("summary", true)
        measurementFragment.nav(args = bundle, name = "measurement_result")
    }

    fun deviceList() {
        DeviceListFragment().nav(name = "device_list", direction = Direction.FROM_TOP)
    }

    fun pop() {
        popping = true
        popDirection = currentFragment()?.popDirection() ?: Direction.FROM_RIGHT.reverse()
        fm.popBackStack()
    }

    fun popDeviceList() {
        fm.popBackStack()
        val fragment = DeviceListFragment()
        fragment.nav(name = "device_list", direction = Direction.FROM_LEFT, popDirection = Direction.FROM_BOTTOM)
    }

    fun deviceInfo() {
        DeviceInfoFragment().nav(name = "device_info", direction = Direction.FROM_TOP)
    }


    fun deviceInfoFromDeviceList() {
        fm.popBackStack()
        val fragment = DeviceInfoFragment()
        fragment.nav(direction = Direction.FROM_RIGHT, popDirection = Direction.FROM_BOTTOM)
    }

    fun navigateToMeasurementScriptsList() {
        MeasurementScriptListFragment().nav(name = "measurement_scripts_list")
    }


    private fun currentFragmentAnimOverridable(): BaseFragment? {
        if (currentFragment() is BaseFragment) {
            return currentFragment() as BaseFragment
        }
        return null
    }

    fun navigateToMeasurementResultsList() {
        fm.popBackStack("home", 0)
        MeasurementResultListFragment().nav(name = "measurement_result_list",
                direction = Direction.FROM_RIGHT)
    }

    fun overviewFromSurveyResult() {
        SurveyOverviewFragment().nav(name = RESULT_LIST)
    }


    fun navigateToSurveyResultsList(finish: Boolean, surveyDeleted: Boolean = false, fromSurvey: String? = null) {
        if (surveyDeleted) {
            fm.popBackStack(TAG_SURVEY_START, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fm.popBackStack(RESULT_LIST, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = SurveyResultListFragment()
            fragment.nav(name = RESULT_LIST, direction = Direction.FROM_LEFT)
            return
        }

        val args = Bundle()
        fromSurvey?.let {
            args.putString("fromSurvey", fromSurvey)
        }

        if (finish) {
            fm.popBackStack(TAG_SURVEY_START, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fm.popBackStack(RESULT_LIST, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        args.putBoolean("finish", finish)
        SurveyResultListFragment().nav(name = RESULT_LIST, direction = Direction.FROM_RIGHT, args = args)
    }

    fun navigateToSurveyBrowser() {
        val fragment = BrowseSurveysFragment()
        fragment.nav(name = "surveys_browser")
    }

    fun navigateToDeviceDebug() {
        DeviceDebugFragment().nav(name = "device_debug")
    }


    fun navigateToGeoLocation(lat: Double, lon: Double, idx: Int = -1) {
        val fragment = GeoLocationResultFragment()
        val args = Bundle().apply {
            putDouble("lat", lat)
            putDouble("lon", lon)
            putInt("idx", idx)
        }

        if (idx != -1) {
            fm.popBackStack()
        }

        fragment.nav(args = args, name = "geo_location_result")
    }

    fun navigateToPickLocation(idx: Int) {
        val fragment = GeoLocationFragment()
        val args = Bundle().apply {
            putInt("idx", idx)
        }

        fragment.nav(args = args, name = "geo_location")
    }

    open fun onBackPressed(): Boolean {
        currentFragment()?.let {
            when (it) {
                is IBackPressable -> if (it.onBackPressed()) return true
            }
        }

        popping = true
        popDirection = currentFragment()?.popDirection() ?: Direction.FROM_RIGHT.reverse()


        if (handleSurveyBackPressed()) {
            return true
        }

        return false
    }

    private fun direction(next: Direction) {
        nextDirection = next
    }

    fun getDirection(): Direction =
            if (popping) popDirection else nextDirection

    fun navigateToScanner() {
        QrScannerFragment().nav(name = "barcode_scanner")
    }

    private fun disableEntryAnimation(disable: Boolean) {
        disableEntryAnimation = disable
    }


    fun popRoot() {
        fm.popBackStack("home", 0)
    }

    fun moveToOrganization(organization: Organization) {

    }

    fun usersOrganizationList() {
        OrganizationListFragment().nav()
    }


    fun navigateToEnvVars() {
        EnvVarListFragment().nav()
    }

    fun navigateToQrSearch() {
        QrScannerFragment().nav(name = "barcode_scanner", args = Bundle().apply {
            putBoolean("to_qr_search", true)
        })
    }

    fun navigateByIdOrTitle(mapping: Pair<String, String>) {
        thread {
            val online = SurveyDb.of(mainActivity!!.app).db.surveyInfoDao().getAllSurveys()
            val target = (
                    online.find {
                        it.formId == mapping.first
                    } ?: online.find {
                        it.formTitle == mapping.second
                    })

            target?.let {
                loadSurvey(null, it)
            }
        }
    }

    fun standaloneGeolocationPicker() {
        val fragment = GeoPickerFragment()
        fragment.nav()
    }

//    val backstackListener = FragmentManager.OnBackStackChangedListener {
//        val fm: FragmentManager = mainActivity!!.supportFragmentManager
//
//        val idx = (fm.backStackEntryCount - 1 downTo 0).firstOrNull {
//            fm.getBackStackEntryAt(it).name == BaseTAG_RECOVERY
//        } ?: return@OnBackStackChangedListener
//
//    }
}