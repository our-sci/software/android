package org.oursci.android.common.ui.controls.geolocation

import android.os.Bundle
import org.javarosa.core.model.data.GeoPointData
import org.javarosa.form.api.FormEntryPrompt
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.IControlAdapter
import org.oursci.android.common.ui.controls.questionId
import org.oursci.android.common.ui.survey.geolocation.GeoLocationFragment
import org.oursci.android.common.ui.survey.geolocation.GeoLocationResultFragment
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.geolocation.GeoLocationAnswer
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 14.11.17.
 */
object GeoLocationAdapter : IControlAdapter<GeoLocationControl> {


    override fun produce(prompt: FormEntryPrompt, group: Group?): GeoLocationControl? {

        val answerValue = prompt.answerValue
        val answer = (answerValue as? GeoPointData)?.displayText?.replace(" ", ";")

        return GeoLocationControl(
                answer = GeoLocationAnswer(answer = answer),
                defaultValue = "",
                label = prompt.longText ?: "",
                readOnly = prompt.isReadOnly,
                required = prompt.isRequired,
                requiredText = "this input is required",
                hint = prompt.helpText ?: "",
                dataName = prompt.questionId(),
                group = group
        )
    }

    override fun resolve(app: BaseApplication, control: BaseControl<*>) = (control as? GeoLocationControl)?.let {
        it.answer.answer?.let {
            try {
                val args = Bundle()
                val values = it.split(";")
                args.putDouble("lat", values[0].toDouble())
                args.putDouble("lon", values[1].toDouble())

                GeoLocationResultFragment().apply {
                    arguments = args
                }
            } catch (e: Exception) {
                Timber.e(e)
                GeoLocationFragment()
            }
        } ?: GeoLocationFragment()
    } ?: GeoLocationFragment()

}