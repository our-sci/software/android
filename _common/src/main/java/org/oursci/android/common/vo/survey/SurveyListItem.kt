package org.oursci.android.common.vo.survey

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyListItem(
        val title: String,
        val description: String,
        val surveyName: String,
        val large: Boolean = false,
        val surveyId: String,
        var archived: Boolean = false
)