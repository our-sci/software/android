package org.oursci.android.common.envvars

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 14.05.18.
 */

@Entity(tableName = "environment_variables")
data class EnvVar(

        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "variable_key")
        val key: String,

        @ColumnInfo(name = "value")
        val value: String,

        @ColumnInfo(name = "description")
        val description: String,

        @ColumnInfo(name = "created")
        val modified: Long
)