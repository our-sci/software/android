package org.oursci.android.common.ui.controls.external

import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.text.TextControlAnswer

/**
 * Created by Manuel Di Cerbo on 16.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class ExternalControl(
        override var answer: TextControlAnswer = TextControlAnswer(),
        override val hint: String = "",
        override val defaultValue: String = "",
        override val label: String = "",
        override val dataName: String = "",
        override val readOnly: Boolean = false,
        override val required: Boolean = false,
        override val requiredText: String = "",
        override val group: Group? = null,
        val params: ExternalControl.Params) : BaseControl<TextControlAnswer>() {
    override fun answer(): String? {
        return answer.answer
    }

    data class Params(
            val type: Type,
            val url: String,
            val extra: String = ""
    )

    enum class Type {
        GENERIC,
        ONTOLOGY,
        FARMOS_AREA,
        FARMOS_PLANTING
    }
}