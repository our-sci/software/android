package org.oursci.android.common

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.icu.util.Measure
import android.os.Build
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import android.text.Html
import android.text.Layout
import android.text.Spanned
import android.view.LayoutInflater
import androidx.core.view.LayoutInflaterCompat
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import com.crashlytics.android.answers.CustomEvent
import io.fabric.sdk.android.services.network.HttpRequest
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormatterBuilder
import org.json.JSONObject
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyResultInfo
import org.oursci.android.common.vo.survey.measurement.MeasurementAnswer
import timber.log.Timber
import java.io.File
import java.net.NetworkInterface
import java.net.SocketException


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 27.07.17.
 */

fun Fragment.app(): BaseApplication {
    return context!!.applicationContext as BaseApplication
}

@SuppressLint("InlinedApi")
fun html(text: String): Spanned = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
    else -> Html.fromHtml(text)
}

fun Application.pref(key: Int): String {
    return PreferenceManager.getDefaultSharedPreferences(this).getString(
            resources.getString(key), "")
}

fun Application.pref(key: Int, default: Int): String {
    return PreferenceManager.getDefaultSharedPreferences(this).getString(
            resources.getString(key), resources.getString(default))
}

fun Application.storePref(key: Int, value: String) {
    PreferenceManager.getDefaultSharedPreferences(this).edit().putString(resources.getString(key), value).apply()
}


fun Application.booleanPref(key: Int, default: Boolean = false): Boolean {
    return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
            resources.getString(key), default)
}

fun Application.ipAddresses(): Array<String> {
    try {
        return NetworkInterface.getNetworkInterfaces().toList().flatMap {
            it.inetAddresses.toList().filter { !it.isLoopbackAddress }.mapNotNull {
                if (it.address.size <= 4) {
                    it.hostAddress
                } else null
            }
        }.toTypedArray()
    } catch (ex: Throwable) {
        Timber.e(ex)
    }

    return arrayOf()
}

fun Application.extractFile(assetName: String, dest: File) {
    dest.outputStream().use {
        val out = it
        assets.open(assetName).use {
            it.copyTo(out)
        }
    }
}

fun resolvePathFromMeasurementAnswer(answer: String): String {
    try {
        var found = false
        try {
            if (File(answer).exists()) {
                found = true
            }
        } catch (e: Exception) {
            found = false
        }

        // could be the new format

        val json = try {
            JSONObject(answer)
        } catch (e: Exception) {
            Timber.e(e)
            JSONObject()
        }

        Timber.d("moving to result, found file path in measurement control")
        return if (found) answer else json.optString("filename",
                json.optJSONObject("meta")?.optString("filename") ?: "")
    } catch (e: Exception) {
        Timber.e(e)
    }

    return ""
}

fun logSurveyEvent(id: String) {
    try {
        Answers.getInstance().logCustom(CustomEvent("survey")
                .putCustomAttribute("id", id))
    } catch (e: Exception) {
        Timber.e(e)
    }
}

fun logTakeMeasurementEvent(id: String) {
    try {
        Answers.getInstance().logCustom(CustomEvent("measurement")
                .putCustomAttribute("id", id))
    } catch (e: Exception) {
        Timber.e(e)
    }
}


fun logSuccessfulUpload(id: String) {
    try {
        Answers.getInstance().logCustom(CustomEvent("upload")
                .putCustomAttribute("id", id))
    } catch (e: Exception) {
        Timber.e(e)
    }
}


fun Survey.isControlRelevant(control: BaseControl<*>): Boolean {
    val formIndex = this.indexMap[control]
    return formIndex?.let { this.fec.model.isIndexRelevant(it) } ?: true
}

fun Survey.allRequiredAnswered() = controls.all { control ->
    val formIndex = this.indexMap[control]
    val relevant = formIndex?.let { this.fec.model.isIndexRelevant(it) } ?: true


    if (!control.required || !relevant) {
        return@all true
    }

    when (control) {
        is MeasurementControl -> control.answered()
        else -> {
            control.answer()?.isNotBlank() ?: false
        }
    }
}

fun Survey.allRelevantScriptsDone() = controls.filterIsInstance<MeasurementControl>().all { control ->
    val formIndex = this.indexMap[control]
    val relevant = formIndex?.let { this.fec.model.isIndexRelevant(it) } ?: true


    if (!control.required || !relevant) {
        return@all true
    }


    if (control.answered()) {
        return@all true
    }

    return@all false
}

fun Survey.nextScript() = controls.filterIsInstance<MeasurementControl>().first { control ->
    val formIndex = this.indexMap[control]
    val relevant = formIndex?.let { this.fec.model.isIndexRelevant(it) } ?: true

    if (!control.required || !relevant) {
        return@first false
    }

    if (control.answered()) {
        return@first false
    }

    return@first true
}

fun Survey.hasMeasurementWarningsOrErrors() = controls.filterIsInstance<MeasurementControl>().any {
    when (it.status()) {
        MeasurementControl.Companion.Status.WARNING,
        MeasurementControl.Companion.Status.ERROR -> true
        else -> false
    }
}

fun MeasurementControl.answered(): Boolean {
    this.answer()?.let { answer ->
        if (answer.isBlank()) {
            return false
        }

        val json = try {
            JSONObject(answer)
        } catch (e: Exception) {
            return false
        }

        val success = MeasurementControl.Companion.Status.SUCCESS.state
        val state = json.optJSONObject("meta")?.optString("state", success)
        state?.let {
            if(it.isBlank()){
                return true;
            }

            return it.equals(success, true)
        }

        return false
    }
    return false
}

fun MeasurementControl.status(): MeasurementControl.Companion.Status? {
    val answer = this.answer() ?: return MeasurementControl.Companion.Status.PENDING
    if (answer.isBlank()) {
        return MeasurementControl.Companion.Status.PENDING
    }
    val json = try {
        JSONObject(answer)
    } catch (e: Exception) {
        return MeasurementControl.Companion.Status.PENDING
    }

    val success = MeasurementControl.Companion.Status.SUCCESS.state
    val state = json.optJSONObject("meta")?.optString("state", success)
            ?: return MeasurementControl.Companion.Status.PENDING
    if (state.isBlank()) {
        return MeasurementControl.Companion.Status.SUCCESS
    }

    return MeasurementControl.Companion.Status.from(state)

}

fun Long.timeAgo(): String {
    val period = Period(DateTime(this), DateTime())
    return formatter.print(period).split(" ").take(2).joinToString(" ") {
        it.split("#").joinToString(" ")
    }.let { "%s ago".format(it) }
}

private val formatter = PeriodFormatterBuilder()
        .appendYears().appendSuffix("#years ")
        .appendMonths().appendSuffix("#months ")
        .appendWeeks().appendSuffix("#weeks ")
        .appendDays().appendSuffix("#days ")
        .appendHours().appendSuffix("#h ")
        .appendMinutes().appendSuffix("#min ")
        .appendSeconds().appendSuffix("#s ")
        .printZeroNever()
        .toFormatter()

data class Dialog(
        var theme: Int,
        var title: String = "",
        var message: String = "",
        var positive: Pair<String, () -> Unit>? = null,
        var neutral: Pair<String, () -> Unit>? = null,
        var negative: Pair<String, () -> Unit>? = "Cancel" to {},
        var layout: Int? = null
) {
    fun action(label: String, callback: () -> Unit): Pair<String, () -> Unit> {
        return label to callback
    }
}


fun dialog(activity: BaseActivity, autoShow: Boolean = true, block: Dialog.() -> Unit): AlertDialog {
    val d = Dialog(activity.alertDialogTheme)

    d.apply(block)

    val builder = AlertDialog.Builder(
            activity, activity.alertDialogTheme)
            .setTitle(d.title)
            .setMessage(d.message)

    val positive = d.positive
    if (positive != null) {
        builder.setPositiveButton(positive.first) { dialog, which ->
            positive.second()
            dialog.dismiss()
        }
    }

    val neutral = d.neutral
    if (neutral != null) {
        builder.setNeutralButton(neutral.first) { dialog, which ->
            neutral.second()
            dialog.dismiss()
        }
    }

    val negative = d.negative
    if (negative != null) {
        builder.setNegativeButton(negative.first) { dialog, which ->
            negative.second()
            dialog.dismiss()
        }
    }

    val layout = d.layout
    if (layout != null) {
        val inflater = LayoutInflater.from(activity)
        val view = inflater.inflate(layout, null)
        builder.setView(view)
    }

    return builder.create().apply { if (autoShow) show() }
}

fun BaseFragment.dialog(autoShow: Boolean = true, block: Dialog.() -> Unit) = dialog(activity, autoShow, block)