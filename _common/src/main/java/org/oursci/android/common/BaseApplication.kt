package org.oursci.android.common

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.location.Location
import android.net.Uri
import android.os.Environment
import android.preference.PreferenceManager
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.lifecycle.ViewModelProvider
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import io.fabric.sdk.android.Fabric
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.farmos.FarmOsDatabase
import org.oursci.android.common.farmos.FarmOsDb
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.resources.ExternalResource
import org.oursci.android.common.resources.ResourcesProvider
import org.oursci.android.common.server.WebServer
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.ui.survey.browse.BrowseSurveysViewModel
import org.oursci.android.common.ui.credentials.FarmOsViewModel
import org.oursci.android.common.ui.devices.DeviceDebugViewModel
import org.oursci.android.common.ui.devices.DeviceInfoViewModel
import org.oursci.android.common.ui.devices.DeviceListViewModel
import org.oursci.android.common.ui.envvar.EnvVarViewModel
import org.oursci.android.common.ui.measurement.results.MeasurementResultListViewModel
import org.oursci.android.common.ui.measurement.scripts.MeasurementScriptListViewModel
import org.oursci.android.common.ui.organization.OrganizationViewModel
import org.oursci.android.common.ui.qrscanner.QrScannerViewModel
import org.oursci.android.common.ui.survey.external.ExternalViewModel
import org.oursci.android.common.ui.survey.geolocation.GeoLocationViewModel
import org.oursci.android.common.ui.survey.group.GroupViewModel
import org.oursci.android.common.ui.survey.list.SurveyListViewModel
import org.oursci.android.common.ui.survey.loading.LoadingViewModel
import org.oursci.android.common.ui.survey.measurement.MeasurementViewModel
import org.oursci.android.common.ui.survey.mutliplechoice.MultipleChoiceViewModel
import org.oursci.android.common.ui.survey.overview.SurveyOverviewViewModel
import org.oursci.android.common.ui.survey.results.SurveyResultListViewModel
import org.oursci.android.common.ui.survey.text.TextControlViewModel
import org.oursci.android.common.ui.welcome.WelcomeAdapter
import org.oursci.android.common.vo.LoginUser
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.webapi.Api
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.io.File
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
abstract class BaseApplication : Application() {

    /**
     * add viewmodel classes here so they are instantiated with baseapp as argument
     */
    private val viewModelClasses: ArrayList<Class<out BaseViewModel>> = arrayListOf(
            TextControlViewModel::class.java,
            SurveyOverviewViewModel::class.java,
            SurveyListViewModel::class.java,
            MultipleChoiceViewModel::class.java,
            LoadingViewModel::class.java,
            MeasurementViewModel::class.java,
            DeviceListViewModel::class.java,
            DeviceInfoViewModel::class.java,
            MeasurementScriptListViewModel::class.java,
            MeasurementResultListViewModel::class.java,
            SurveyResultListViewModel::class.java,
            BrowseSurveysViewModel::class.java,
            DeviceDebugViewModel::class.java,
            GeoLocationViewModel::class.java,
            QrScannerViewModel::class.java,
            GroupViewModel::class.java,
            OrganizationViewModel::class.java,
            EnvVarViewModel::class.java,
            ExternalViewModel::class.java,
            FarmOsViewModel::class.java)

    lateinit var api: Api
    abstract val WEB_SERVER_PORT: Int

    abstract val navigationManager: BaseNavigationManager
    abstract val TAG: String
    abstract val filesDirName: String

    open val limitSurveysToOrganization = emptyArray<String>()
    open val allowOrganizations: Boolean = true
    open val hasLegacyOfflineSurveys: Boolean = true
    open val organizationURL = "https://app.our-sci.net"
    open val welcomeItems: Collection<WelcomeAdapter.WelcomeItem> = emptyList()
    open val defaultOrganizationId: String = ""


    lateinit var auth: FirebaseAuth
    lateinit var user: LoginUser

    private var activity: BaseActivity? = null

    lateinit var appFilesDir: File
    val ACTION_USB_PERMISSION = "org.oursci.android.common.ACTION_USB_PERMISSION"

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var farmOsDb: FarmOsDatabase


    override fun onCreate() {
        super.onCreate()
        Timber.tag(TAG)
        Timber.plant(Timber.DebugTree())


        Crashlytics.Builder()
                .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build()
                .also { crashlyticsKit ->
                    Fabric.with(this, crashlyticsKit)
                }


        // Crashlytics.log("here is a log!!!")
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        Timber.d("server url is: %s", pref(R.string.server_url))
        appFilesDir = Environment.getExternalStorageDirectory()?.let { File(it, filesDirName) }
                ?: filesDir
        Timber.d("Files Dir is: %s", appFilesDir)

        makeFilesDir()

        auth = FirebaseAuth.getInstance()
        updateUser()

        if (booleanPref(R.string.enable_webserver)) {
            try {
                WebServer.of(this).start()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }


        BtManager.of(this).init()
        UManager.of(this).init()

        //BtManager.of(this).scan()

        Timber.d("url is: %s", currentUrl())
        api = Retrofit.Builder()
                .baseUrl(currentUrl())
                .addConverterFactory(MoshiConverterFactory.create())
                .build().create(Api::class.java)!!

        registerReceiver(mUsbReceiver, IntentFilter(ACTION_USB_PERMISSION))

        farmOsDb = FarmOsDb.of(this).db

        fetchOntologies()
    }

    val monitorThread = Thread(Runnable {
        while (true) {
            try {
                Thread.sleep(15000)
            } catch (e: Exception) {
                // interrupted means immediately run test
            }
            if (!monitorInternet.get()) {
                continue
            }

            try {
                val urlc = URL("https://app.our-sci.net/api/profile/self").openConnection() as HttpURLConnection
                urlc.setRequestProperty("Connection", "close")
                urlc.connectTimeout = 1500
                urlc.connect()
                online.set(true)
            } catch (e: Exception) {
                online.set(false)
            }
        }
    }).apply { start() }

    val monitorInternet = AtomicBoolean(false)

    fun onActivtyCreated(baseActivity: BaseActivity) {
        navigationManager.registerActivity(baseActivity)
        activity = baseActivity
        monitorInternet.set(true)
        monitorThread.interrupt()
    }

    fun onActivityDestroyed() {
        navigationManager.unregisterActivity()
        activity = null
        monitorInternet.set(false)
    }

    abstract val viewModelFactory: ViewModelProvider.Factory

    fun updateUser() {
        auth.currentUser?.let {
            user = LoginUser(it.displayName ?: "", it.photoUrl ?: Uri.EMPTY, it.uid)
        }

        if (auth.currentUser == null) {
            user = LoginUser("", Uri.EMPTY, "")
        }

    }


    @Suppress("LeakingThis")
    val map: Map<Class<out BaseViewModel>, BaseViewModel> by lazy {
        viewModelClasses.associate {
            it to createViewModelInstance(it)
        }
    }

    fun <T : BaseViewModel> createViewModelInstance(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return modelClass.constructors.first {
            it.parameterTypes.all {
                it.isAssignableFrom(BaseApplication::class.java)
            }
        }.newInstance(this) as T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> createViewModel(modelClass: Class<T>): T {

        val key = map.keys.firstOrNull {
            it.isAssignableFrom(modelClass)
        } ?: throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)

        return map[key] as T
    }

//    fun preloadSurveyImages() {
//
//        val targets = mutableListOf<FutureTarget<out Any>>()
//        listOf("https://www.what-dog.net/Images/faces2/scroll0015.jpg").forEach {
//            val target = Glide.with(this)
//                    .downloadOnly()
//                    .load(it)
//                    .submit()
//
//            target.onResourceReady(null, { _, _ ->
//
//                true
//            })
//            targets += target
//        }
//    }

    fun onMeasurementScriptUploaded(measurement: MeasurementScript) {
        activity?.onMeasurementScriptLoaded(measurement)
    }

    fun onMeasurementLoadError(error: String) {
        activity?.showErrorDialog("Error loading Measurement", error)
    }

    fun onSurveyLoadError(error: String) {
        activity?.showErrorDialog("Error loading Survey", error)
    }

    fun onSurveyLoaded(survey: Survey) {
        activity?.onSurveyUploaded(survey)
    }

    fun trashDir() = File(appFilesDir, "deleted_surveys")
    fun trashMeasurementsDir() = File(appFilesDir, "deleted_measurements")


    fun makeFilesDir() {
        try {
            appFilesDir.mkdirs()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun updateWebApplicationUrl(s: String) {
        api = Retrofit.Builder()
                .baseUrl(s)
                .addConverterFactory(MoshiConverterFactory.create())
                .build().create(Api::class.java)!!

        Timber.d("updated webapplication TYPE_URL_HINT: %s", s)
    }

    fun currentUrl(): String = pref(R.string.server_url, R.string.default_url)



    private val mUsbReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                val action = it.action ?: return

                if (action != ACTION_USB_PERMISSION) {
                    return
                }

                val device = it.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)

                if (it.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    usbPermissionCallback?.invoke(device)
                } else {
                    Timber.e("unable to get devcie from intent")
                    usbPermissionCallback?.invoke(null)

                }
            }
        }

    }


    var usbPermissionCallback: ((device: UsbDevice?) -> Unit)? = null

    fun registerPermissionCallback(callback: (device: UsbDevice?) -> Unit) {
        usbPermissionCallback = callback
    }

    fun snackWithAction(message: String, action: String, cb: () -> Unit) {
        activity?.snackWithAction(message, action, cb)
    }

    private val rememberMap = mutableMapOf<String, String?>()

    fun resolveRemember(dataName: String) = SurveyManager.of(this).currentSurvey?.let {
        rememberMap[it.formId + ">" + dataName]
    }

    fun storeRemember(dataName: String, remember: String?) {
        SurveyManager.of(this).currentSurvey?.let {
            rememberMap.put(it.formId + ">" + dataName, remember)
        }
    }

    private val anonmap = mutableMapOf<String, Boolean>()

    fun rememberAnon(dataName: String) = SurveyManager.of(this).currentSurvey?.let {
        anonmap[it.formId + ">" + dataName]
    }

    fun setRememberAnon(dataName: String, remember: Boolean) {
        SurveyManager.of(this).currentSurvey?.let {
            anonmap.put(it.formId + ">" + dataName, remember)
        }
    }

    data class PersistedOrg(val id: String, val name: String, val desc: String)

    fun setCurrentContributionOrg(org: PersistedOrg?) {
        PreferenceManager.getDefaultSharedPreferences(this).edit {
            org?.apply {
                putString("org-id", id)
                putString("org-name", name)
                putString("org-desc", desc)
            } ?: remove("org-id")
        }
    }

    fun getCurrentContributionOrg(): PersistedOrg? {
        PreferenceManager.getDefaultSharedPreferences(this).let {
            return it.getString("org-id", null)?.let { id ->
                PersistedOrg(
                        id,
                        it.getString("org-name", ""),
                        it.getString("org-desc", "")
                )
            }
        }
    }

    val defaultOursciDevices = listOf(
            "reflectometer"
    )

    fun isOurSciDevice(): Boolean {
        val prefName = currentDeviceInfo()?.let(::prefNameFromDevcie) ?: return false
        val deviceName = (currentDeviceInfo() as? BluetoothDevice)?.name ?: ""
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(prefName, defaultOursciDevices.contains(deviceName))
    }

    fun setOurSciDevice(b: Boolean) {
        val prefName = currentDeviceInfo()?.let(::prefNameFromDevcie) ?: return
        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean(prefName, b)
        }
    }

    private fun prefNameFromDevcie(device: Any) = when (device) {
        is BluetoothDevice -> {
            "bt_${device.address}"
        }
        is UsbDevice -> {
            "usb_%04X_%04X".format(device.vendorId, device.productId)
        }
        else -> {
            null
        }
    }

    private fun currentDeviceInfo() = if (BtManager.of(this).hasDevice()) {
        BtManager.of(this).currentDevice()
    } else {
        UManager.of(this).currentDevice()
    }


    val webAnswerData = ConsumableLiveData<String>()


    fun answerFromWebserver(answer: String) {
        Timber.d("received answer from webserver: %s", answer)
        webAnswerData.postValue(answer)
    }


    fun gpsLock(enable: Boolean): Boolean {

        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean("gps-lock", enable)
        }

        if (!enable) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
            return true
        }

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 1000

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        return true
    }

    val locationCallback = object : LocationCallback() {

    }

    fun lastLocation(callback: (location: Location?) -> Unit) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            callback(null)
            return
        }

        fusedLocationClient.lastLocation.addOnSuccessListener {
            callback(it)
        }

        fusedLocationClient.lastLocation.addOnFailureListener {
            callback(null)
        }
    }

    fun fetchOntologies() {
        thread {
            try {
                fetchOntologiesSync()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    @Throws(Exception::class)
    fun fetchOntologiesSync() {
        val provider = ResourcesProvider("11427416")
        val commit = provider.fetchLastCommit()
        val lastSha = PreferenceManager.getDefaultSharedPreferences(this).getString("oursci-gitlab-resources-sha", "")
        if (lastSha == commit.commitId) {
            return
        }


        provider.fetchRepository(File(filesDir, "resources"))

        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putString("oursci-gitlab-resources-sha", commit.commitId)
            putString("oursci-gitlab-resources-date", commit.createdAt)
        }
    }

    fun resourceCommitInformation(): ResourcesProvider.CommitInformation {
        val mgr = PreferenceManager.getDefaultSharedPreferences(this)
        return ResourcesProvider.CommitInformation(
                mgr.getString("oursci-gitlab-resources-sha", "") ?: "",
                mgr.getString("oursci-gitlab-resources-date", "") ?: "")
    }

    fun getAllOntologies(): List<ExternalResource> =
            ResourcesProvider("11427416").allResources(File(filesDir, "resources"))

    fun refreshFarmOs() {
        /**
         * this behaves buggy, creates second entries for plantings
         */
        // activity?.refreshFarmos()
    }


    val hasBeenSelectedKey = "is_organization_selected"

    fun isOrganizationSelected() = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(hasBeenSelectedKey, false)
    fun setOrganizationSelected(hasBeenSelected: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean(hasBeenSelectedKey, hasBeenSelected)
        }
    }

    fun publishRecentResult(result: String) {

        WebServer.of(this).publishResult(result);
    }


    var online = AtomicBoolean(true)


}