package org.oursci.android.common.ui.survey

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.databinding.ControlDrawBinding
import org.oursci.android.common.ui.BaseFragment
import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */

class DrawControlFragment : BaseFragment() {

    lateinit var binding: ControlDrawBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ControlDrawBinding.inflate(inflater, container, false)
        binding.drawerView.setupView(activity, false, File(""))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}