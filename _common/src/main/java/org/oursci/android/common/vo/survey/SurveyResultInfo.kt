package org.oursci.android.common.vo.survey

import androidx.room.*
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormatterBuilder
import timber.log.Timber
import java.io.File
import java.net.URLDecoder
import java.net.URLEncoder

/**
 * Created by Manuel Di Cerbo on 13.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
@Entity(tableName = "survey_result_infos")
class SurveyResultInfo(
        @ColumnInfo(name = "title")
        val title: String,

        @ColumnInfo(name = "description")
        val description: String,

        @ColumnInfo(name = "state")
        @TypeConverters(Converters::class)
        val state: State,

        @ColumnInfo(name = "name")
        val name: String,

        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "instance")
        @TypeConverters(Converters::class)
        val instance: File,

        @ColumnInfo(name = "selected")
        var selected: Boolean = false,

        @ColumnInfo(name = "done")
        val done: Boolean = false,

        @ColumnInfo(name = "exported")
        val exported: Boolean = false,

        @ColumnInfo(name = "textsearch")
        val textSearch: String = "",

        @ColumnInfo(name = "organizationName")
        val organizationName: String = ""
) {

    fun textSearchAsList() = textSearch.split('/').map { URLDecoder.decode(it, "UTF-8") }

    enum class State(val id: Long) {
        ONGOING(1),
        FINISHED(2),
        UPLOADING(3),
        UPLOADED(4),
        EXPORTED(5),
        SCRIPT_ERROR(6),
        SCRIPT_WARNING(7),
        INVALID(-1);

        companion object {
            fun fromId(id: Long?): State =
                    State.values().firstOrNull { it.id == id } ?: State.INVALID
        }
    }

    class Converters {
        @TypeConverter
        fun fileFromString(value: String?): File? {
            return File(value)
        }

        @TypeConverter
        fun stringFromFile(value: File?): String? {
            return value?.absolutePath
        }

        @TypeConverter
        fun stateFromLong(value: Long?): State? {
            return State.fromId(value)
        }

        @TypeConverter
        fun longFromState(value: State?): Long? {
            return value?.id
        }
    }

}