package org.oursci.android.common.ui.devices

import android.view.View

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 10.10.17.
 */
class DeviceListDisplay {

    enum class State {
        IDLE,
        SCANNING,
        SCAN_DONE,
        CONNECTING,
        PAIRING
    }

    var state: State = State.IDLE

    fun visibilityProgressBar() = when (state) {
        State.SCANNING, State.PAIRING -> View.VISIBLE
        else -> View.GONE
    }


    fun visibilityRecycleView() = when (state) {
        State.CONNECTING, State.PAIRING -> View.GONE
        else -> View.VISIBLE
    }

    fun visibilityScanButton() = when (state) {
        State.IDLE, State.SCAN_DONE -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityStopButton() = when (state) {
        State.SCANNING -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityConnecting() = when (state) {
        State.CONNECTING, State.PAIRING -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityMissingDevice() = when (state) {
        State.SCAN_DONE -> View.VISIBLE
        else -> View.GONE
    }

    fun textConnecting() = when (state) {
        State.PAIRING -> "Pairing"
        else -> "Connecting"
    }

}