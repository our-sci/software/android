package org.oursci.android.common.survey

/**
 * Created by Manuel Di Cerbo on 30.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
enum class ControlType(val id: Int) {

    UNTYPED(-1),
    INPUT(1),
    SELECT_ONE(2),
    SELECT_MULTI(3),
    TEXTAREA(4),
    SECRET(5),
    RANGE(6),
    UPLOAD(7),
    SUBMIT(8),
    TRIGGER(9),
    IMAGE_CHOOSE(10),
    LABEL(11),
    AUDIO_CAPTURE(12),
    VIDEO_CAPTURE(13),
    OSM_CAPTURE(14),
    FILE_CAPTURE(15);


    companion object {
        fun from(id: Int): ControlType = ControlType.values().find {
            it.id == id
        } ?: UNTYPED
    }
    
}