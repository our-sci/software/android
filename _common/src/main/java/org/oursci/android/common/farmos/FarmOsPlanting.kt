package org.oursci.android.common.farmos

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "farmos_planting")
data class FarmOsPlanting(
        @PrimaryKey(autoGenerate = true)
        val _id: Int = 0,
        val id: Long,
        val type: String,
        val name: String,
        val farmOsServer: Long
) {
    override fun toString(): String {
        return "$name ($id)"
    }
}