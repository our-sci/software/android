package org.oursci.android.common

import android.Manifest
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.preference.PreferenceManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentManager
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import okhttp3.ResponseBody
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.survey.SurveyDb
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.survey.SurveyResultInfoDb
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.BaseSettingsFragment
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.ui.credentials.FarmOsViewModel
import org.oursci.android.common.ui.devices.DeviceInfoFragment
import org.oursci.android.common.ui.devices.DeviceListFragment
import org.oursci.android.common.ui.devices.DeviceListViewModel
import org.oursci.android.common.ui.envvar.EnvVarListFragment
import org.oursci.android.common.ui.qrscanner.QrScannerFragment
import org.oursci.android.common.ui.survey.browse.BrowseSurveysViewModel
import org.oursci.android.common.ui.survey.external.ExternalFragment
import org.oursci.android.common.ui.survey.geolocation.GeoLocationFragment
import org.oursci.android.common.ui.survey.group.GroupFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementResultFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementViewModel
import org.oursci.android.common.ui.survey.mutliplechoice.MultipleChoiceFragment
import org.oursci.android.common.ui.survey.overview.SurveyOverviewFragment
import org.oursci.android.common.ui.survey.text.TextControlFragment
import org.oursci.android.common.vo.LoginUser
import org.oursci.android.common.vo.Organization
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyListItem
import org.oursci.android.common.webapi.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
abstract class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    abstract val contentView: Int
    abstract val toolbar: Toolbar
    abstract var openDrawerContentDesc: Int
    abstract var closeDrawerContentDesc: Int
    abstract val drawerLayout: DrawerLayout
    abstract val alertDialogTheme: Int
    abstract val loginLogo: Int
    abstract val loginTheme: Int?
    abstract val defaultSubtitle: String

    var startedViaLink = false

    private val surveyOverviewVisible = AtomicBoolean(false)
    val deviceVisible = AtomicBoolean(true)
    val showQrSearch = AtomicBoolean(false)
    val newSurveyInstance = AtomicBoolean(false)

    private val settingsVisible = AtomicBoolean(true)
    private val menusEnabled = AtomicBoolean(true)


    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private lateinit var drawerArrowDrawable: DrawerArrowDrawable

    private lateinit var menuItemSurveyOverview: MenuItem
    var searchView: SearchView? = null

    var fragmentMenuItems = mutableListOf<MenuItemInfo>()


    companion object {
        val RC_SIGN_IN = 1000
        val RC_BLUETOOTH_ENABLE = 2000
        val RC_BLUETOOTH_ENABLE_FROM_MEASUREMENT = 2100
    }

    var menu: Menu? = null


    private lateinit var farmOsViewModel: FarmOsViewModel
    private lateinit var browseSurveysViewModel: BrowseSurveysViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        init()
    }

    fun loadFromArguments() {

        val data: Uri = intent?.data ?: return

        val survey = data.pathSegments.getOrNull(0)?.replace('/', '_') ?: return


        data.queryParameterNames.forEach {
            Timber.d("%10s => %10s".format(it, data.getQueryParameter(it)))
        }

        val progressDialog = dialog(this, false) {
            title = "Loading"
            message = "Downloading $survey"
            layout = R.layout.layout_loading
        }

        thread {
            val match = SurveyManager.of(app).loadSurveyListItem(survey)


            if (match == null) {
                // check online

                browseSurveysViewModel = ViewModelProviders.of(this, app.viewModelFactory).get(BrowseSurveysViewModel::class.java)

                try {
                    val res = app.api.findSurveys(survey).execute()
                    val body = res.body()?.content?.find { it.formId == survey }
                            ?: throw Exception("Unable to find formId")
                    runOnUiThread {
                        progressDialog.show()
                    }
                    browseSurveysViewModel.downloadSync(listOf(body))
                } catch (e: Exception) {
                    runOnUiThread {
                        progressDialog.dismiss()
                        dialog(this) {
                            title = "Error"
                            message = "Form could not be downloaded: $survey, ${e.message}"
                        }
                    }
                    Timber.e(e)
                    return@thread

                }

                try {
                    val downloaded = SurveyManager.of(app).loadSurveyListItem(survey)
                            ?: return@thread

                    app.navigationManager.loadSurvey(item = Api.SurveyBody(
                            id = -1,
                            formId = downloaded.surveyId,
                            formTitle = downloaded.title,
                            description = downloaded.description,
                            date = -1,
                            archived = false,
                            formFile = "",
                            formVersion = null,
                            picture = null,
                            user = null
                    ), answers = data.query)

                } catch (e: Exception) {
                    Timber.e(e)
                }

                progressDialog.dismiss()

                return@thread
            }



            app.navigationManager.loadSurvey(item = Api.SurveyBody(
                    id = -1,
                    formId = match.surveyId,
                    formTitle = match.title,
                    description = match.description,
                    date = -1,
                    archived = false,
                    formFile = "",
                    formVersion = null,
                    picture = null,
                    user = null
            ), answers = data.query)
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        loadFromArguments()
    }

    fun init() {
        app.onActivtyCreated(this)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(contentView)
        setSupportActionBar(toolbar)

        supportFragmentManager.addOnBackStackChangedListener(fragmentChangeListener)

        actionBarDrawerToggle = object : ActionBarDrawerToggle(
                this, drawerLayout, toolbar, openDrawerContentDesc, closeDrawerContentDesc) {


            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, if (menusEnabled.get()) 0f else 1f)
            }

            override fun onDrawerClosed(drawerView: View) {
                drawerArrowDrawable.progress = if (menusEnabled.get()) 0f else 1f
            }
        }

        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        drawerArrowDrawable = DrawerArrowDrawable(this)
        drawerArrowDrawable.color = ContextCompat.getColor(this, R.color.colorPrimary)


        //actionBarDrawerToggle.setHomeAsUpIndicator(drawerArrowDrawable)
        toolbar.setNavigationOnClickListener {
            Timber.d("nav click")
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else if (actionBarDrawerToggle.drawerArrowDrawable.progress == 1f) {
                hideKeyboard()
                onBackPressed()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        // app.preloadSurveyImages()

        fragmentChangeListener.onBackStackChanged()

        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.subtitle = defaultSubtitle

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("gps-lock", false)) {
            app.gpsLock(true)
        }

        farmOsViewModel = ViewModelProviders.of(this, app.viewModelFactory).get(FarmOsViewModel::class.java)
        farmOsViewModel.errors.observe(this, ConsumableObserver<List<String>> { errors, consumed ->
            if (consumed) return@ConsumableObserver

            errors?.let {
                showErrorDialog("Error fetching farmOS data", errors.joinToString { e -> "$e\n" })
            }
        })
        farmOsViewModel.init()
        loadFromArguments()

        val data: Uri = intent?.data ?: return

        Timber.d("uri data: $data")

        startedViaLink = data.toString() == "surveystack://measurement"

    }

    fun hideKeyboard() {
        val imm = app.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        currentFocus?.windowToken?.let { imm.hideSoftInputFromWindow(it, 0) }
    }

    override fun onDestroy() {
        app.onActivityDestroyed()
        supportFragmentManager.removeOnBackStackChangedListener(fragmentChangeListener)
        super.onDestroy()
    }

    override fun onBackPressed() {

        if (app.navigationManager.onBackPressed()) {
            return
        }


        fragmentDefaults()
        hideKeyboard()
        //supportFragmentManager.popBackStack()

        super.onBackPressed()
    }

    fun lockMenus() {
        enableMenus(false)
    }

    fun unlockMenus() {
        enableMenus(true)
    }


    private fun enableMenus(enable: Boolean) {
        menusEnabled.set(enable)
        Timber.d("enable menus: %s", enable)

        drawerLayout.setDrawerLockMode(
                if (enable) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        ObjectAnimator.ofFloat(actionBarDrawerToggle.drawerArrowDrawable,
                "progress", if (enable) 0f else 1f).start()

        for (i in 0 until toolbar.menu.size()) {
            toolbar.menu.getItem(i)?.isEnabled = enable
        }

        menu?.let {
            for (i in 0 until it.size()) {
                it.getItem(i).isEnabled = enable
            }
        }
    }

    val app get() = applicationContext as BaseApplication

    abstract fun isInIntro(): Boolean
    abstract fun introDone()

    var permissionSuccess: (() -> Unit)? = null
    var permissionFailure: (() -> Unit)? = null

    private val permissions = listOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.CAMERA
// for the time beeing we don't need audio
//   Manifest.permission.RECORD_AUDIO
    )


    fun requiresPermission() = permissions.any {
        Timber.d("%s %d", it, ContextCompat.checkSelfPermission(this, it))
        ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
    }

    fun showPermissionDialog(title: String, message: String, positive: () -> Unit = {}, negative: () -> Unit = {}) {

        permissionSuccess = positive
        permissionFailure = negative



        if (!requiresPermission()) {
            positive()
            return
        }

        val alertDialog = AlertDialog.Builder(this, alertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Yes Allow") { dialog, _ ->
                    dialog.dismiss()
                    requestPermissions(permissions)
                }
                .setNegativeButton("Maybe later", { dialog, _ ->
                    dialog.dismiss()
                    negative()
                })
                .create()
        alertDialog.show()
    }

    private val PERMISSION_REQUEST: Int = 1

    fun requestPermissions(permissions: List<String>) {
        ActivityCompat.requestPermissions(this,
                permissions.toTypedArray(),
                PERMISSION_REQUEST)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST -> {
                if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                    app.makeFilesDir()
                    permissionSuccess?.let {
                        it()
                    }
                } else {
                    permissionFailure?.let { it() }
                }
            }
        }
    }


    private var loginCallback: ((success: Boolean) -> Unit)? = null
    fun login(callback: (Boolean) -> Unit) {
        loginCallback = callback
        val builder =
                AuthUI
                        .getInstance()
                        .createSignInIntentBuilder()
                        .setLogo(loginLogo)
                        .setAvailableProviders(mutableListOf(
                                AuthUI.IdpConfig.GoogleBuilder().build(),
                                AuthUI.IdpConfig.EmailBuilder().build()
                        ))

        loginTheme?.let { builder.setTheme(it) }

        startActivityForResult(
                builder.build(),
                RC_SIGN_IN)
        // Get an instance of AuthUI based on the default app

        //startActivity(auth0Lock.newIntent(this))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_SIGN_IN -> {
                val response = IdpResponse.fromResultIntent(data)

                if (resultCode == Activity.RESULT_OK) {
                    response?.let {
                        app.updateUser()
                    }
                    app.setOrganizationSelected(false)
                    loginCallback?.let {
                        app.auth.currentUser?.getIdToken(false)?.result?.token?.let {
                            Timber.d("signup with token")
                            val res = app.api.signUp(it)
                            res.enqueue(object : Callback<ResponseBody> {
                                override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                                    if (response == null) {
                                        Timber.e("response null")
                                        return
                                    }

                                    if (!response.isSuccessful) {
                                        val err = "unable to log in: %s".format(response.errorBody()?.string()
                                                ?: "<no error body>")
                                        Timber.e(err)
                                        runOnUiThread {
                                            Toast.makeText(app, err, Toast.LENGTH_SHORT).show()
                                        }
                                        return
                                    }

                                    runOnUiThread {
                                        Toast.makeText(app, "success logging in", Toast.LENGTH_SHORT).show()
                                    }

                                    farmOsViewModel.fetchData()
                                }

                                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                                    Timber.e(t)
                                }

                            })
                        }
                        it(true)
                    }

                    return
                }

                response?.let {

                    when (it.error?.errorCode) {
                        ErrorCodes.NO_NETWORK -> {
                            snack("Network connection not available")
                            Timber.d("no network available")
                        }
                        ErrorCodes.UNKNOWN_ERROR -> {
                            snack("Error signing in")
                            Timber.d("unknown error")
                        }
                        else -> {
                            Timber.e("Error is null")
                            return@let
                        }
                    }
                }

                loginCallback?.let {
                    it(false)
                }
            }
            RC_BLUETOOTH_ENABLE -> {
                val vw = ViewModelProviders.of(this, app.viewModelFactory).get(DeviceListViewModel::class.java)
                if (resultCode == RESULT_OK) {
                    vw.startDeviceScan()
                } else {
                    // TODO: display bluetooth is turned off
                    vw.deviceScanDone.postValue(null)
                }
            }

            RC_BLUETOOTH_ENABLE_FROM_MEASUREMENT -> {
                val vm = ViewModelProviders.of(this, app.viewModelFactory).get(MeasurementViewModel::class.java)
                if (resultCode == RESULT_OK) {
                    vm.connectToLastDevice()
                } else {
                    // TODO: display bluetooth is turned off
                }
            }
        }
    }

    fun snack(text: String) {
        //Snackbar.make(findViewById(R.id.coordinator), text, Snackbar.LENGTH_SHORT)
    }

    fun snackWithAction(text: String, action: String, callback: () -> Unit) {
        val sn = Snackbar.make(findViewById(R.id.coordinator), text, Snackbar.LENGTH_SHORT)
        sn.setAction(action, {
            callback()
        })
        sn.show()
    }

    fun allowOverview(allow: Boolean) {
        Timber.d("allow survey overview: %s", allow)
        surveyOverviewVisible.set(allow)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.surveyOverview)?.isVisible = surveyOverviewVisible.get()
        menu?.findItem(R.id.item_device)?.isVisible = deviceVisible.get()
        menu?.findItem(R.id.action_settings)?.isVisible = deviceVisible.get()
        menu?.findItem(R.id.qr_search)?.isVisible = showQrSearch.get()
        menu?.findItem(R.id.new_survey_instance)?.isVisible = newSurveyInstance.get()

        searchViewItem = menu?.findItem(R.id.search)!!

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.base, menu)
        searchView = (menu?.findItem(R.id.search)?.actionView as? SearchView)

        fragmentMenuItems.clear()

        val fragment = app.navigationManager.currentFragment()
        when (fragment) {
            is BaseFragment -> {
                Timber.d("clearing menus")
                fragmentMenuItems.addAll(fragment.menuItems())
                menu?.findItem(R.id.search)?.let { searchItem ->
                    searchView?.let {
                        it.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD


                        it.setOnQueryTextFocusChangeListener { v, hasFocus ->
                            Timber.d("focus changed: %s", hasFocus)
                        }


                        it.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                            override fun onQueryTextSubmit(query: String?): Boolean {
                                Timber.d("clicked search")
                                query?.let {
                                    fragment.onSearchClicked(it)
                                }
                                return true
                            }

                            override fun onQueryTextChange(newText: String?): Boolean {
                                return false
                            }
                        })

                        it.setOnCloseListener {
                            Timber.d("on search close")

                            searchView?.setIconifiedByDefault(true)
                            searchView?.isIconified = true
                            fragment.onSearchClosed()
                            hideKeyboard()

                            true
                        }



                        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                                Timber.d("on expand")
                                //searchView?.setQuery(fragment.searchText(), false)
                                for (i in 0 until menu.size()) {
                                    if (menu.getItem(i).itemId == R.id.qr_code ||
                                            menu.getItem(i).itemId == R.id.search ||
                                            menu.getItem(i).itemId == R.id.menu_upload ||
                                            menu.getItem(i).itemId == R.id.menu_item_delete ||
                                            menu.getItem(i).itemId == R.id.menu_select_all
                                    ) {
                                        continue
                                    }
                                    menu.getItem(i).isVisible = false
                                }
                                menu.findItem(R.id.qr_code)?.isVisible = true

                                return true
                            }

                            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                                Timber.d("on collapse")
                                menu.findItem(R.id.qr_code)?.isVisible = false
                                for (i in 0 until menu.size()) {
                                    if (menu.getItem(i).itemId == R.id.qr_code ||
                                            menu.getItem(i).itemId == R.id.search) {
                                        continue
                                    }
                                    // TODO: fix bug where this shows survey overview in BrowseSurveysFragment
                                    menu.getItem(i).isVisible = true
                                }

                                fragment.onSearchClosed()
                                return true
                            }
                        })
                    }
                    searchItem.isVisible = fragment.searchable

                    findViewById<View>(android.R.id.content).post {
                        fragment.onMenuRendered()
                    }
                }

            }
            else -> {
                menu?.findItem(R.id.search)?.isVisible = false
                menu?.findItem(R.id.qr_code)?.isVisible = false
            }
        }

        fragmentMenuItems.forEach {
            val item = menu?.add(Menu.NONE, it.id, it.order, it.name)
            if (it.icon != -1) {
                item?.setIcon(it.icon)?.setShowAsAction(
                        if (it.visible && it.showAsIcon) MenuItem.SHOW_AS_ACTION_ALWAYS else MenuItem.SHOW_AS_ACTION_NEVER) // TODO: determine what 'visible' is used for
            }
        }

        return true
    }

    open fun logout() {
        AuthUI.getInstance().signOut(this).addOnCompleteListener {
            app.user = LoginUser("", Uri.EMPTY, "")
            app.setCurrentContributionOrg(null)
            app.navigationManager.logout()
            farmOsViewModel.clear()
            onLoggedOut()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        val fragment = app.navigationManager.currentFragment()
        if (fragment is BaseFragment) {
            item?.let {
                if (fragment.onMenuItemPressed(it.itemId)) {
                    return true
                }
            }
        }

        return when (item?.itemId) {
            R.id.surveyOverview -> {
                app.navigationManager.surveyOverview()
                true
            }
            R.id.item_device -> {
                if (BtManager.of(app).hasDevice() || UManager.of(app).hasDevice()) {
                    //app.navigationManager.deviceInfo()
                    app.navigationManager.deviceList()
                } else {
                    app.navigationManager.deviceList()
                }

                true
            }
            R.id.search -> {
                Timber.d("search pressed")
                menu?.findItem(R.id.qr_code)?.isVisible = true
                true
            }
            R.id.qr_code -> {
                item.expandActionView()
                app.navigationManager.navigateToScanner()
                true
            }
            R.id.qr_search -> {
                app.navigationManager.navigateToQrSearch()
                true
            }
            R.id.new_survey_instance -> {
                SurveyManager.of(app).currentSurvey?.let {
                    SurveyManager.of(app).createSurveyInstance(it.formId)
                    app.navigationManager.navigateToSurveyResultsList(false, false)
                    app.navigationManager.startSurvey(false)
                }
                        ?: Toast.makeText(app, "Unable to create survey, current survey not set", Toast.LENGTH_SHORT).show()

                true
            }
            else -> {
                false
            }
        }
    }

    abstract fun onLoggedOut()

    fun fragmentDefaults() {
        allowOverview(false)
    }

    private val fragmentChangeListener = FragmentManager.OnBackStackChangedListener {

        val fragment = app.navigationManager.currentFragment()
        Timber.d("fragment is $fragment")

        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.subtitle = if (app.online.get()) {
            "$defaultSubtitle (online)"
        } else {
            "$defaultSubtitle (offline)"
        }



        supportActionBar?.show()

        when (fragment) {
            is QrScannerFragment -> {
                deviceVisible.set(false)
                supportActionBar?.hide()
                showQrSearch.set(false)
                newSurveyInstance.set(false)
            }
            is EnvVarListFragment -> {
                deviceVisible.set(false)
                settingsVisible.set(false)
                showQrSearch.set(false)
                newSurveyInstance.set(false)
            }
            is DeviceInfoFragment,
            is DeviceListFragment -> {
                deviceVisible.set(false)
                showQrSearch.set(false)
                newSurveyInstance.set(false)
            }
            is BaseSettingsFragment -> {
                deviceVisible.set(false)
                settingsVisible.set(false)
                showQrSearch.set(false)
                newSurveyInstance.set(false)
            }
            is GroupFragment,
            is SurveyOverviewFragment,
            is TextControlFragment,
            is GeoLocationFragment,
            is MeasurementFragment,
            is MeasurementResultFragment,
            is ExternalFragment,
            is MultipleChoiceFragment -> {
                deviceVisible.set(true)
                settingsVisible.set(true)
                showQrSearch.set(true)
                newSurveyInstance.set(true)
            }
            else -> {
                deviceVisible.set(true)
                settingsVisible.set(true)
                showQrSearch.set(false)
                newSurveyInstance.set(false)
            }
        }

        when (fragment) {
            is GroupFragment,
            is SurveyOverviewFragment,
            is TextControlFragment,
            is GeoLocationFragment,
            is MeasurementFragment,
            is MeasurementResultFragment,
            is ExternalFragment,
            is MultipleChoiceFragment -> {
                val controls = SurveyManager.of(app).currentSurvey?.let {
                    it.controls.filter { control ->
                        when (control) {
                            is TextControl,
                            is MultipleChoiceControl,
                            is ExternalControl ->
                                control.dataName.toLowerCase().contains("id")
                            else -> false
                        }
                    }
                }

                if (controls != null) {
                    val ids = controls.take(3).filter { ctrl ->
                        ctrl.answer()?.isNotBlank() ?: false
                    }.joinToString("|") { ctrl ->
                        ctrl.answer() ?: ""
                    }

                    if (!ids.isBlank()) {
                        supportActionBar?.subtitle = "ID: $ids"
                    }
                }

            }
        }

        invalidateOptionsMenu()
    }

    fun showClearMediaDialog() {
        val alertDialog = AlertDialog.Builder(this, alertDialogTheme)
                .setTitle("Refresh Survey Media")
                .setMessage("Refresh cached images and media displayed during surveys?")
                .setPositiveButton("Refresh") { dialog, _ ->

                    Glide.get(app).clearMemory()

                    thread {
                        Glide.get(app).clearDiskCache()
                        // app.preloadSurveyImages()
                    }

                    dialog.dismiss()
                }
                .setNegativeButton("Cancel", { dialog, _ ->
                    dialog.dismiss()
                })
                .create()
        alertDialog.show()
    }

    fun onMeasurementScriptLoaded(measurement: MeasurementScript) {
        runOnUiThread {
            app.navigationManager.navigateToMeasurement(measurement.id)
        }
    }

    fun showErrorDialog(title: String, error: String) {
        runOnUiThread {
            val alertDialog = AlertDialog.Builder(this, alertDialogTheme)
                    .setTitle(title)
                    .setMessage(error)
                    .setNeutralButton("OK", { dialog, _ ->
                        dialog.dismiss()
                    })
                    .create()
            alertDialog.show()
        }
    }

    fun showDialog(title: String, content: String) {
        runOnUiThread {
            val alertDialog = AlertDialog.Builder(this, alertDialogTheme)
                    .setTitle(title)
                    .setMessage(content)
                    .setNeutralButton("OK") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
            alertDialog.show()
        }
    }

    fun onSurveyUploaded(survey: Survey) {
        runOnUiThread {
            app.navigationManager.loadSurvey(descriptionItem = SurveyListItem(
                    survey.name,
                    "",
                    survey.name,
                    surveyId = survey.formId
            ))
        }
    }

    class MenuItemInfo(val group: Int, val id: Int, val order: Int,
                       val name: String, val icon: Int, var visible: Boolean, val showAsIcon: Boolean = true)


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        app.navigationManager.popRoot()
        return false
    }

    fun showRefreshDbDialog() {
        val alertDialog = AlertDialog.Builder(this, alertDialogTheme)
                .setTitle("Refresh Survey Results DB")
                .setMessage("Refresh cached survey results?")
                .setPositiveButton("Refresh") { dialog, _ ->
                    thread {
                        SurveyResultInfoDb.of(app).db.surveyInfoDao().apply {
                            deleteAllInfos()
                            insertSurveyInfo(SurveyManager.of(app).loadSurveyResultsList())
                            runOnUiThread {
                                Toast.makeText(app, "Database has been refreshed", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel", { dialog, _ ->
                    dialog.dismiss()
                })
                .create()
        alertDialog.show()
    }

    private lateinit var searchViewItem: MenuItem
    fun expandSearchView() {
        Timber.d("expanding search view")
        searchViewItem.expandActionView()
    }

    fun refreshFarmos() {
        farmOsViewModel.fetchData()
    }

    fun showSelectOrganizationDialog(orgs: List<Organization>, selected: (() -> Unit)? = null) {
        data class Item(val name: String, val org: Organization?)

        val items = mutableListOf(
                Item("No Organization", null)
        ).apply {
            addAll(orgs.map {
                Item(it.name, it)
            })
        }




        runOnUiThread {
            var sel = 0
            AlertDialog.Builder(this, this.alertDialogTheme)
                    .setTitle("Select your Organization")
                    //.setMessage("Which organization are you contributing to?")
                    .setSingleChoiceItems(items.map {
                        it.name
                    }.toTypedArray(), 0) { _, which ->
                        sel = which
                    }
                    .setPositiveButton("Select") { dialog, _ ->
                        items[sel].org?.let {
                            app.setCurrentContributionOrg(BaseApplication.PersistedOrg(it.organizationId, it.name, it.description))
                        }
                        app.setOrganizationSelected(true)
                        dialog.dismiss()
                        selected?.let { it() }
                    }
                    .setNegativeButton("Cancel") { dialog, _ ->
                        dialog.dismiss()
                    }.create().show()
        }


    }

    fun onMeasurementEnd() {
        if (startedViaLink) {
            finish()
        }
    }
}