package org.oursci.android.common.farmos

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */

@Entity(tableName = "farmos_server")
data class FarmOsServer(
        @PrimaryKey var id: Long,
        @ColumnInfo(name= "url") var url: String = "",
        @ColumnInfo(name = "description") var description: String = "",
        @ColumnInfo(name = "active") var active: Boolean = false
){
    override fun toString(): String {
        return url
    }
}