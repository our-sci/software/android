package org.oursci.android.common.ui.survey.mutliplechoice

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import kotlinx.android.synthetic.main.item_multiple_choice.view.*
import org.oursci.android.common.R
import org.oursci.android.common.databinding.ItemMultipleChoiceBinding
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceOptionDisplay
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceAdapter(private val onClick: (MultipleChoiceOptionDisplay) -> Unit
) : RecyclerView.Adapter<MultipleChoiceAdapter.ViewHolder>() {


    var items: ArrayList<MultipleChoiceOptionDisplay> = ArrayList()



    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.tag = position
        holder.binding.chkOption.tag = position


        items.getOrNull(position)?.let {
            holder.binding.item = it
            holder.binding.invalidateAll()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMultipleChoiceBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent?.context), R.layout.item_multiple_choice,
                parent, false)

        binding.root.setOnClickListener { view ->
            onChangeListener(view)
        }


        binding.root.chk_option.setOnClickListener { view ->
            onChangeListener(view)
        }

        return ViewHolder(binding)
    }

    private fun onChangeListener(view: View) {
        val idx = view.tag as Int?

        idx?.let {
            items.getOrNull(it)?.let { onClick(it) }
        }
    }

    class ViewHolder(val binding: ItemMultipleChoiceBinding) : RecyclerView.ViewHolder(
            binding.root)

    fun refresh(item: MultipleChoiceOptionDisplay) {
        val idx = items.indexOfFirst {
            item === it
        }

        Timber.d("item with idx: %d changed inside list", idx)
        notifyItemChanged(idx)
    }

}