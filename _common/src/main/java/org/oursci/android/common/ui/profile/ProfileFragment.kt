package org.oursci.android.common.ui.profile

import android.net.Uri
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentProfileBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import org.oursci.android.common.BaseApplication.PersistedOrg
import org.oursci.android.common.farmos.FarmOsServer
import org.oursci.android.common.ui.credentials.FarmOsViewModel
import org.oursci.android.common.ui.organization.OrganizationViewModel

import org.oursci.android.common.vo.Organization
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 03.08.17.
 */

class ProfileFragment : BaseFragment() {

    private fun colorIcon(icon: Int) = ContextCompat.getDrawable(activity, icon)?.let {
        DrawableCompat.setTint(it, ContextCompat.getColor(activity, R.color.colorAccent))
        it
    }

    private val selectedIcon by lazy {
        colorIcon(R.drawable.ic_check_circle_24dp)
    }

    val adapter = GenericItemAdapter<Organization>(
            produceDisplay = {
                val persisted = app().getCurrentContributionOrg()


                val icon = if (persisted != null) {
                    if (persisted.id == it.organizationId) {
                        selectedIcon
                    } else null
                } else (if (it.id == -1L) selectedIcon else null)

                GenericItemDisplay(
                        title = it.name,
                        description = "",
                        iconRight = icon
                )
            },
            onClick = { org, _ ->
                app().setOrganizationSelected(true)
                if (org.id == -1L) {
                    Timber.d("removing current org")
                    app().setCurrentContributionOrg(null)
                } else {
                    app().setCurrentContributionOrg(PersistedOrg(org.organizationId, org.name, org.description))
                }
                updateAdapter()
            }
    )

    lateinit var farmOsAdapter: ArrayAdapter<FarmOsServer>

    private fun updateAdapter() {
        adapter.notifyDataSetChanged()
        adapter.notifyItemChanged(0)
    }


    lateinit var binding: FragmentProfileBinding
    val viewModel by lazyViewModel<OrganizationViewModel>()
    val farmOsViewModel by lazyViewModel<FarmOsViewModel>()


    private val resultObserver = ConsumableObserver<List<Organization>> { orgs, consumed ->

        if (consumed || orgs == null) {
            return@ConsumableObserver
        }

        adapter.items = orgs.toMutableList().apply {
            add(0, Organization(-1L,
                    "", "Contribute to no organization",
                    "Select this to not contribute to any of your Organizations",
                    -1, "", false))
        }
        adapter.notifyDataSetChanged()

    }

    private val serverObserver = ConsumableObserver<List<FarmOsServer>> { servers, consumed ->
        farmOsAdapter.clear()
        farmOsAdapter.addAll(servers)
        farmOsAdapter.notifyDataSetChanged()
        binding.farmos.setSelection(0)
    }

    val farmOsSelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val server = parent?.getItemAtPosition(position) as FarmOsServer

            if (!server.active) {
                farmOsViewModel.setActiveServer(server)
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.lockMenus()
        binding.user = app().user

        if (app().user.imageUri == Uri.EMPTY) {
            binding.invalidateAll()
            Glide.with(view.context).load(R.drawable.owl_red).apply(RequestOptions.circleCropTransform()).into(binding.ivProfile)
            binding.invalidateAll()
        }

        binding.ivFarmosRefresh.setOnClickListener {
            farmOsViewModel.fetchData()
        }

        if (app().allowOrganizations) {
            viewModel.loadResult.observe(this, resultObserver)
            viewModel.loadOrganizations()
        } else {
            binding.recyclerview.visibility = View.GONE
            binding.tvContribute.visibility = View.GONE
        }



        farmOsViewModel.spinner.observe(this, serverObserver)
        farmOsViewModel.servers.observe(this, ConsumableObserver<List<FarmOsServer>> { servers, consumed ->
            if(consumed) {
                return@ConsumableObserver
            }
            val n = servers?.size ?: 0
            if(n == 0) {
                return@ConsumableObserver
            }
            Toast.makeText(activity, "Fetched $n farmOS servers", Toast.LENGTH_SHORT).show()
        })


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.btLogout.setOnClickListener {
            logout()
        }

        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        farmOsAdapter = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item)
        //farmOsAdapter.addAll(FarmOsDb.of(app()).db.farmOsDao().getAllServers())
        binding.farmos.adapter = farmOsAdapter
        binding.farmos.onItemSelectedListener = farmOsSelectListener


        return binding.root
    }

    fun logout() {
        activity.logout()
    }
}
