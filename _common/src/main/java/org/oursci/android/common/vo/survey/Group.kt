package org.oursci.android.common.vo.survey

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 26.04.18.
 */
class Group(
        val id: String,
        val description: String = "",
        val anon: Boolean = false,
        val subjects: MutableList<String> = mutableListOf()
)