package org.oursci.android.common.ui.envvar

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.envvars.EnvVar
import org.oursci.android.common.envvars.EnvVarDb
import org.oursci.android.common.ui.BaseViewModel
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 14.05.18.
 */
class EnvVarViewModel(app: BaseApplication) : BaseViewModel(app) {

    val envVarResult = ConsumableLiveData<List<EnvVar>>()
    val busy = AtomicBoolean(false)

    fun loadEnvVars() {
        if (busy.getAndSet(true)) return

        thread {
            try {
                envVarResult.postValue(EnvVarDb.of(app).db.envVarDao().getAllEnvVars())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            busy.set(false)

        }
    }
}