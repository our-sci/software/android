package org.oursci.android.common.survey

import androidx.room.*
import org.oursci.android.common.vo.survey.SurveyResultInfo

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 11.12.17.
 */
@Dao
interface SurveyResultInfoDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyInfo(info: List<SurveyResultInfo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyInfo(info: SurveyResultInfo)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateSurveyInfo(info: SurveyResultInfo)

    @Query("select * from survey_result_infos")
    fun getAllSurveyInfos(): List<SurveyResultInfo>

    @Delete
    fun deleteSurveyInfo(info: SurveyResultInfo)

    @Query("delete from survey_result_infos")
    fun deleteAllInfos()

    @Query("delete from survey_result_infos where instance = :instance")
    fun delete(instance: String)


    @Query("select count(*) from survey_result_infos")
    fun numSurveyResults(): Int

}