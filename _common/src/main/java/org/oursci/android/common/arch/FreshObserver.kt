package org.oursci.android.common.arch

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 14.09.19.
 */
class FreshObserver<T>(val callback: (arg: T) -> Unit) : ConsumableObserver<T> {
    override fun onChanged(t: T?, consumed: Boolean) {
        if (!consumed && t != null) {
            callback(t)
        }
    }
}