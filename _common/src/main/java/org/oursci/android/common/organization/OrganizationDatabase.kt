package org.oursci.android.common.organization

import androidx.room.Database
import androidx.room.RoomDatabase
import org.oursci.android.common.vo.Organization

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */

@Database(
        entities = [Organization::class],
        version = 1,
        exportSchema = false
)
abstract class OrganizationDatabase : RoomDatabase() {
    abstract fun orgainzationDao(): OrganizationDao
}