package org.oursci.android.common.ui.intro

import androidx.lifecycle.*
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.R
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentIntroBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.IBackPressable
import org.oursci.android.common.vo.IntroSlide
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.07.17.
 */
abstract class IntroFragment : BaseFragment(), IBackPressable {

    lateinit var binding: FragmentIntroBinding
    lateinit var adapter: IntroAdapter


    abstract val viewModel: BaseIntroViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("onViewCreated")

        (activity as? BaseActivity)?.lockMenus()
        (activity as? BaseActivity)?.supportActionBar?.hide()
        viewModel.slides.value?.let {
            adapter.update(it)
        }

        slideObserver.onChanged(viewModel.slides.value, false)
        activate(viewModel.currentPosition)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    val slideObserver = ConsumableObserver<Array<IntroSlide>> { data, consumed ->
        Timber.d("intro fragment: consumed %s", consumed)

        data?.let {
            var circlesView = binding.layoutCircles?.root as LinearLayout
            val inflater = LayoutInflater.from(circlesView.context)

            circlesView.removeAllViews()
            for (i in it.indices) {
                val v: View?
                try {
                    v = inflater?.inflate(R.layout.intro_circle, circlesView)
                    if (i == 0) {
                        v?.isEnabled = true
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            adapter.update(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.slides.observe(this, slideObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro, container, false)


        adapter = IntroAdapter()
        binding.recyclerView.adapter = adapter

        val snapper = PagerSnapHelper()
        snapper.attachToRecyclerView(binding.recyclerView)

        var firstScroll = true
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                var idx = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (firstScroll && idx == slideCount() - 1) {
                    firstScroll = false
                    recyclerView.scrollToPosition(viewModel.currentPosition)
                    recyclerView.visibility = View.VISIBLE
                    return
                }

                if (firstScroll) {
                    return
                }

                if (idx < 0) {
                    idx = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    if (dx > 0) {
                        idx++
                    }
                }
                activate(idx)
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        binding.imageView.setOnClickListener {
            Timber.d("onclick")
            var idx = (binding.recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
            if (idx < 0) {
                idx = (binding.recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            }
            if (idx >= slideCount() - 1) {
                activity.showPermissionDialog("App Permissions",
                        "For using all features, such as Bluetooth, Location and " +
                                "Storage the App requires permissions.", {
                    activity.introDone()
                    Timber.d("intro done, permissions OK")
                }, {
                    activity.introDone()
                    Timber.d("intro done, missing permissions")
                })
                return@setOnClickListener
            }
            binding.recyclerView.smoothScrollToPosition(idx + 1)
        }

        binding.tvSkip.setOnClickListener {
            binding.recyclerView.smoothScrollToPosition(slideCount() - 1)
        }

        binding.recyclerView.setHasFixedSize(true)

        binding.recyclerView.visibility = View.INVISIBLE
        binding.recyclerView.smoothScrollToPosition(slideCount() - 1)

        return binding.root
    }

    fun activate(child: Int) {
        viewModel.currentPosition = if (child >= 0) child else 0
        var circlesView = binding.layoutCircles?.root as LinearLayout

        for (i: Int in 0 until circlesView.childCount) {
            circlesView.getChildAt(i).isEnabled = i == child
        }
    }

    override fun onBackPressed(): Boolean {
        Timber.d("handling onbackpressed")
        val idx = (binding.recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        if (idx == 0) {
            activity.finish()
            return true
        }

        binding.recyclerView.smoothScrollToPosition(idx - 1)
        return true
    }

    fun slideCount(): Int {
        viewModel.slides.value?.let {
            return it.size
        }

        return 0
    }


}