package org.oursci.android.common.measurement

import org.json.JSONObject
import java.io.File

/**
 * Created by Manuel Di Cerbo on 30.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
data class MeasurementScript(
        val id: String,
        val name: String,
        val description: String,
        val version: String,
        val url: String,
        val dir: File,
        val actionName: String? = null,
        val requireDevice: Boolean = true,
        val manifestCreated: Long,
        val manifestWithExtras: JSONObject
)