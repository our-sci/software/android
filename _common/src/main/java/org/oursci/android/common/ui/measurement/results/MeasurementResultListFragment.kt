package org.oursci.android.common.ui.measurement.results

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.measurement.MeasurementResultInfo
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 12.10.17.
 */
class MeasurementResultListFragment : BaseFragment() {

    val viewModel by lazyViewModel<MeasurementResultListViewModel>()
    val adapter = GenericItemAdapter({
        GenericItemDisplay(
                title = it.measurementScript.name,
                description = it.description
        )
    }, { measurementResultInfo: MeasurementResultInfo, _: View ->
        app().navigationManager.navigateToMeasurement(measurementResultInfo.measurementScript.id, true, measurementResultInfo.file)
    })

    lateinit var binding: FragmentGenericListBinding


    private val scriptsResultObserver = ConsumableObserver<Array<MeasurementResultInfo>> { list, consumed ->
        if (consumed || list == null) return@ConsumableObserver
        adapter.items = list.toList().sortedByDescending { it.file.lastModified() }
        adapter.notifyDataSetChanged()
        binding.progress.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.measurementsLoadedResult.observe(this, scriptsResultObserver)

        adapter.items = emptyList()
        activity.lockMenus()
        activity.allowOverview(false)

        binding.progress.visibility = View.VISIBLE

        view.postDelayed({
            viewModel.loadMeasurementInfos()
        }, 500)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        return binding.root
    }
}