package org.oursci.android.common.envvars

import androidx.room.*
import org.oursci.android.common.vo.Organization

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */
@Dao
interface EnvVarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(envVar: EnvVar)

    @Delete
    fun deleteEnvVar(envVar: EnvVar)

    @Query("select * from environment_variables")
    fun getAllEnvVars(): List<EnvVar>

    @Query("select * from environment_variables where variable_key = :key order by variable_key limit 1")
    fun getEnvVar(key: String): EnvVar?
}