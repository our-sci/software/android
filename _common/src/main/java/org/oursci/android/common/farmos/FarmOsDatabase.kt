package org.oursci.android.common.farmos

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */

@Database(entities = [FarmOsServer::class, FarmOsArea::class, FarmOsPlanting::class], version = 4, exportSchema = false)
abstract class FarmOsDatabase : RoomDatabase() {
    abstract fun farmOsDao(): FarmOsDao
}

