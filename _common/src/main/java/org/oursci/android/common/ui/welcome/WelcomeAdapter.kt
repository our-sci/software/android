package org.oursci.android.common.ui.welcome

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_card_homescreen.view.*
import org.oursci.android.common.databinding.ItemCardHomescreenBinding
import org.oursci.android.common.html

/**
 * Created by Manuel Di Cerbo on 01.05.18.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class WelcomeAdapter : RecyclerView.Adapter<WelcomeAdapter.ViewHolder>() {

    val items: MutableList<WelcomeItem> = mutableListOf()

    class WelcomeItem(
            val title: String = "",
            val subtitle: String? = null,
            val content: String? = null,
            val leftAction: (Pair<String, () -> Unit>)? = null,
            val middleAction: (Pair<String, () -> Unit>)? = null,
            val rightAction: (Pair<String, () -> Unit>)? = null
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items[position]

        holder.itemView.apply {
            tv_title.text = item.title

            item.content?.let {
                tv_content.text = html(it)
                tv_content.visibility = View.VISIBLE
            } ?: tv_content.let {
                it.text = ""
                it.visibility = View.GONE
            }
            item.subtitle?.let {
                tv_subtitle.text = it
                tv_subtitle.visibility = View.VISIBLE
            } ?: tv_subtitle.let {
                it.text = ""
                it.visibility = View.GONE
            }

            arrayOf(
                    bt_left to item.leftAction,
                    bt_middle to item.middleAction,
                    bt_right to item.rightAction
            ).forEach {
                buttonSetup(it.first, it.second)
            }

            postInvalidate()
        }
    }

    private fun buttonSetup(b: Button, v: (Pair<String, () -> Unit>)?) {

        v?.let {
            b.text = v.first
            b.setOnClickListener { v.second() }
            b.visibility = View.VISIBLE
        } ?: b.apply {
            text = ""
            visibility = View.GONE
            setOnClickListener(null)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ItemCardHomescreenBinding.inflate(LayoutInflater.from(parent.context), parent, false).let {
                return ViewHolder(it)
            }

    override fun getItemCount(): Int = items.size


    class ViewHolder(itemView: ItemCardHomescreenBinding) : RecyclerView.ViewHolder(itemView.root)
}