package org.oursci.android.common.ui.controls.geolocation

import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.vo.survey.BaseAnswer
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.geolocation.GeoLocationAnswer

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class GeoLocationControl(
        override var answer: GeoLocationAnswer = GeoLocationAnswer(),
        override val hint: String = "",
        override val defaultValue: String = "",
        override val label: String = "",
        override val dataName: String = "",
        override val readOnly: Boolean = false,
        override val required: Boolean = false,
        override val requiredText: String = "",
        override val group: Group? = null) : BaseControl<GeoLocationAnswer>() {

    override fun answer(): String? {
        return answer.answer
    }
}