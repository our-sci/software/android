package org.oursci.android.common.ui.credentials

import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.farmos.FarmOsArea
import org.oursci.android.common.farmos.FarmOsDb
import org.oursci.android.common.farmos.FarmOsPlanting
import org.oursci.android.common.farmos.FarmOsServer
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.webapi.FarmOSClient
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

class FarmOsViewModel(app: BaseApplication) : BaseViewModel(app) {

    // all servers
    val servers = ConsumableLiveData<List<FarmOsServer>>()

    // active areas
    val areas = ConsumableLiveData<List<FarmOsArea>>()

    // active plantings
    val plantings = ConsumableLiveData<List<FarmOsPlanting>>()

    // all servers and active server at top
    val spinner = ConsumableLiveData<List<FarmOsServer>>()

    val errors = ConsumableLiveData<List<String>>()

    val farmOsDb = FarmOsDb.of(app).db

    var initialized: Boolean = false

    fun init() {
        if (!initialized) {
            postValues()
            fetchData()
        }
        initialized = true
    }

    private fun postValues() {
        val all = farmOsDb.farmOsDao().getAllServers()
        val activeAreas = farmOsDb.farmOsDao().getActiveAreas()
        val activePlantings = farmOsDb.farmOsDao().getActivePlantings()
        val activeServer = farmOsDb.farmOsDao().getActiveServers().firstOrNull()

        // spinner needs active server at top
        val s = when {
            activeServer != null -> listOf(activeServer) + all.filterNot { it.id == activeServer.id }
            else -> all
        }

        Timber.d("Posting values")
        s.forEach {
            Timber.d("url = ${it.url}, active = ${it.active}")
        }

        servers.postValue(all)
        areas.postValue(activeAreas)
        plantings.postValue(activePlantings)
        spinner.postValue(s)

    }

    fun clear() {
        farmOsDb.farmOsDao().deleteAllAreas()
        farmOsDb.farmOsDao().deleteAllServers()
        postValues()
    }

    fun setActiveServer(server: FarmOsServer) {
        val all = farmOsDb.farmOsDao().getAllServers()
        all.forEach {
            it.active = false
            if (server.id == it.id) {
                it.active = true
            }
        }

        farmOsDb.farmOsDao().insertServer(all)

        postValues()
    }

    fun fetchData() {
        if (!app.user.loggedIn()) {
            return
        }

        thread {

            val errorMessages = mutableListOf<String>()

            var token: String? = null
            FirebaseAuth.getInstance()?.currentUser?.getIdToken(false)?.let {
                try {
                    token = Tasks.await(it, 10, TimeUnit.SECONDS).token
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            val t = token ?: return@thread

            try {
                val r = app.api.fetchTpCredentials(t).execute()
                val credentials = r.body() ?: throw Exception("error parsing body")

                val activeServerId = farmOsDb.farmOsDao().getActiveServers().firstOrNull()?.id

                farmOsDb.farmOsDao().deleteAllAreas()
                farmOsDb.farmOsDao().deleteAllPlantings()
                farmOsDb.farmOsDao().deleteAllServers()


                val farmOsCredentials = credentials.filter { it.type == "FARMOS" }
                val farmOsServers = farmOsCredentials.map {
                    FarmOsServer(it.id, it.url, it.note)
                }

                // try to re-set the active server
                activeServerId?.let { asi ->
                    val activeServer = farmOsServers.filter { it.id == asi }.firstOrNull()
                    if (activeServer == null) {
                        errorMessages.add("Can not find active farmOS credentials, may have been deleted on the server")
                    } else {
                        activeServer.active = true
                    }
                }

                // if we have only one server, make it active
                if (farmOsServers.size == 1) {
                    farmOsServers.first().active = true
                }

                farmOsDb.farmOsDao().insertServer(farmOsServers)

                farmOsCredentials.forEach {credential ->
                    val farmOSClient = FarmOSClient(credential.url, credential.username, credential.password)

                    try {
                        val areas = farmOSClient.fetchAllAreas()
                                ?: throw Exception("can't fetch areas")
                        val list = areas.map { a ->
                            FarmOsArea(0, a.id, a.type ?: "", a.name, credential.id)
                        }
                        farmOsDb.farmOsDao().insertAreas(list)
                    } catch (e: Exception) {
                        Timber.e(e)
                        errorMessages.add("Can't fetch areas for ${credential.url}")
                    }

                    try {
                        val plantings = farmOSClient.fetchAllPlantings()
                                ?: throw Exception("can't fetch plantings")
                        val list = plantings.map { p ->
                            FarmOsPlanting(0, p.id, p.type ?: "", p.name, credential.id)
                        }
                        farmOsDb.farmOsDao().insertPlantings(list)
                    } catch (e: Exception) {
                        Timber.e(e)
                        errorMessages.add("Can't fetch plantings for ${credential.url}")
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
            }

            if (errorMessages.isNotEmpty()) {
                errors.postValue(errorMessages.toList())
            }

            postValues()
        }
    }

    fun getActiveAreas() =
            farmOsDb.farmOsDao().getActiveAreas()

    fun getActivePlantings() =
            farmOsDb.farmOsDao().getActivePlantings()
}