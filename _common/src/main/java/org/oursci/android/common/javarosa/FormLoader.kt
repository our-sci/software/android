package org.oursci.android.common.javarosa

import org.javarosa.core.model.FormDef
import org.javarosa.core.model.instance.FormInstance
import org.javarosa.core.model.instance.InstanceInitializationFactory
import org.javarosa.core.model.instance.TreeReference
import org.javarosa.core.model.instance.utils.DefaultAnswerResolver
import org.javarosa.core.services.locale.Localizer
import org.javarosa.core.services.transport.payload.ByteArrayPayload
import org.javarosa.form.api.FormEntryController
import org.javarosa.form.api.FormEntryModel
import org.javarosa.model.xform.XFormSerializingVisitor
import org.javarosa.xform.parse.XFormParser
import org.javarosa.xform.util.XFormUtils
import timber.log.Timber
import java.io.*

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 06.09.17.
 */
class FormLoader {

    fun loadForm(formDefinitionInputStream: InputStream,
                 formInstanceFile: File? = null,
                 cacheDir: File? = null,
                 shadowDir: File? = null
    ): FormEntryController {

//        if (!formDefinitionFile.exists() || !formDefinitionFile.isFile) {
//            throw IllegalArgumentException("form definitions file does not exist or is " +
//                    "not a file: ${formDefinitionFile.absolutePath}")
//        }

//        if (!cacheDir.exists() || !cacheDir.isDirectory) {
//            throw IllegalArgumentException("cache dir is not a directory or " +
//                    "does not exist: ${cacheDir.absolutePath}")
//        }
//
//        if (!shadowDir.exists() || !shadowDir.isDirectory) {
//            throw IllegalArgumentException("shadow dir is not a directory or " +
//                    "does not exist: ${shadowDir.absolutePath}")
//        }

        var tmpFormDefinition: FormDef? = null

        tmpFormDefinition = XFormUtils.getFormFromInputStream(formDefinitionInputStream)
        tmpFormDefinition?.mainInstance


        if (tmpFormDefinition == null) {
            throw IllegalArgumentException("Form Definition cannot be loaded")
        }

        val formDefinition: FormDef = tmpFormDefinition!!
        val fem = FormEntryModel(tmpFormDefinition)
        val fec = FormEntryController(fem)

//        if (formInstance == null || !formInstance.exists() || !formInstance.isFile) {
//            formDefinition.initialize(true, InstanceInitializationFactory())
//            return fec
//        }

        //val shadowFile = File(shadowDir, formInstance.name)
        var useShadow = false

//        if (shadowFile.exists() && shadowFile.isFile) {
//            if (shadowFile.lastModified() > formInstance.lastModified()) {
//                useShadow = true
//            }
//        }

//        val instanceFile = if (useShadow) shadowFile else formInstance
//        val instanceFile = formInstance


        formInstanceFile?.let {
            val inputStream = FileInputStream(formInstanceFile)

            var success = true
            inputStream.use {
                if (!initInstance(fec, inputStream)) {
                    formDefinition.initialize(true, InstanceInitializationFactory())
                    success = false
                }
            }

            if (!success) {
                persist(fec, formInstanceFile)
            }

            return fec
        }

        formDefinition.initialize(false, InstanceInitializationFactory())


        return fec
    }

    private fun initInstance(fec: FormEntryController, instanceInputStream: InputStream): Boolean {

        var tmpFormInstance: FormInstance? = null

        try {
            tmpFormInstance = XFormParser.restoreDataModel(instanceInputStream, null)
        } catch (e: Exception) {
            Timber.e(e)
            return false
        }


        if (tmpFormInstance == null) {
            throw IllegalStateException("unable to restore data model")
        }

        val instanceRoot = tmpFormInstance.root
        val defRoot = fec.model.form.instance.root.deepCopy(true)

        if (defRoot.name != instanceRoot.name || instanceRoot.multiplicity != 0) {
            return false
        }

        val tr = TreeReference.rootRef()
        tr.add(defRoot.name, TreeReference.INDEX_UNBOUND)

        // Here we set the Collect's implementation of the IAnswerResolver.
        // We set it back to the default after select choices have been populated.
        XFormParser.setAnswerResolver(ExternalAnswerResolver())
        defRoot.populate(instanceRoot, fec.model.form)
        XFormParser.setAnswerResolver(DefaultAnswerResolver())

        // populated model to current form
        fec.model.form.instance.root = defRoot

        // fix any language issues
        // :
        // http://bitbucket.org/javarosa/main/issue/5/itext-n-appearing-in-restored-instances


        if (fec.model.form.localizer == null) {
            fec.model.form.localizer = Localizer(true, true)
        }
        fec.model.form.localizer?.addAvailableLocale("en")
        fec.model.form.localizer.locale = "en"
        if (fec.model.languages != null) {
            fec.model.form
                    .localeChanged(fec.model.language,
                            fec.model.form.localizer)
        }

        return true
    }


    @Throws(Exception::class)
    fun persist(fec: FormEntryController, file: File) {
        val serializer = XFormSerializingVisitor(false)
        val payload = serializer.createSerializedPayload(fec.model.form.instance) as ByteArrayPayload

        exportXmlFile(payload, file)
    }


    @Throws(IOException::class)
    fun exportXmlFile(payload: ByteArrayPayload, file: File) {
        if (file.exists() && !file.delete()) {
            throw IOException("Cannot overwrite ${file.absolutePath} Perhaps the file is locked?")
        }

        // create data stream
        val inputStream = payload.payloadStream
        val len = payload.length.toInt()

        // read of data stream
        val data = ByteArray(len)
        // try {
        val read = inputStream.read(data, 0, len)
        if (read > 0) {
            // write xml file
            var randomAccessFile: RandomAccessFile? = null
            try {
                // String filename = path + File.separator +
                // dir.substring(dir.lastIndexOf(File.separator) + 1) + ".xml";
                randomAccessFile = RandomAccessFile(file, "rws")
                randomAccessFile.write(data)
            } finally {
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close()
                    } catch (e: IOException) {
                        System.err.printf("Error closing RandomAccessFile: ${file.absolutePath}\n")
                    }

                }
            }
        }
    }

}