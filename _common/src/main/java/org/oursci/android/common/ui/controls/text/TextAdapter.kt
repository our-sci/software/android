package org.oursci.android.common.ui.controls.text

import org.javarosa.core.model.data.DateData
import org.javarosa.core.model.data.DecimalData
import org.javarosa.core.model.data.IntegerData
import org.javarosa.core.model.data.StringData
import org.javarosa.form.api.FormEntryPrompt
import org.joda.time.DateTime
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.IControlAdapter
import org.oursci.android.common.ui.controls.questionId
import org.oursci.android.common.ui.survey.text.TextControlFragment
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.text.TextControlAnswer

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 14.11.17.
 */
object TextAdapter : IControlAdapter<TextControl> {
    override fun resolve(app: BaseApplication, control: BaseControl<*>) = TextControlFragment()

    override fun produce(prompt: FormEntryPrompt, group: Group?): TextControl? {
        val answerValue = prompt.answerValue

        val answer = when (answerValue) {
            is DateData -> DateTime(answerValue.value).toString("yyyy-MM-dd")
            is StringData -> answerValue.value.toString()
            is DecimalData -> answerValue.value.toString()
            is IntegerData -> answerValue.value.toString()
            else -> try {
                answerValue.value.toString()
            } catch (e: Exception) {
                null
            }
        }

        return TextControl(
                answer = TextControlAnswer(answer = answer),
                invalidText = "input out of bounds",
                constraint = prompt.constraintText ?: "",
                defaultValue = prompt.answerText ?: "",
                label = prompt.longText ?: "",
                readOnly = prompt.isReadOnly,
                required = prompt.isRequired,
                requiredText = "this input is required",
                hint = prompt.helpText ?: "",
                dataName = prompt.questionId(),
                group = group,
                dataType = prompt.dataType,
                appearanceHint = prompt.appearanceHint ?: ""
        )

    }
}