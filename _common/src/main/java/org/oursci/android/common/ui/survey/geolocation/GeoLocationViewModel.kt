package org.oursci.android.common.ui.survey.geolocation

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.survey.ControlViewModel
import org.oursci.android.common.vo.survey.geolocation.GeoLocationAnswer
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class GeoLocationViewModel(app: BaseApplication) : ControlViewModel<GeoLocationControl>(app) {

    data class PickLocationRes(val lat: Double, val lon: Double)

    val updateAnon = ConsumableLiveData<Boolean>()


    fun redoLocation(idx: Int) {
        SurveyManager.of(app).currentSurvey?.let {
            thread {
                (it.controls[idx] as? GeoLocationControl)?.let {
                    it.answer = GeoLocationAnswer()
                    SurveyManager.of(app).saveCurrentSurvey()
                }
                app.navigationManager.rebuildSurveyStack(idx, pop = true)
            }
        }
    }

    fun locationPicked(idx: Int, lat: Double, lon: Double) {
        thread {
            SurveyManager.of(app).currentSurvey?.let outer@{
                it.controls[idx].let {
                    if (it !is GeoLocationControl) {
                        return@outer
                    }

                    it.answer = it.answer.create("$lat;$lon;-1;-1", it.answer.anon)
                }
                SurveyManager.of(app).saveSurvey(it)
            }

            // remove fragment from stack and put a result fragment onto the stack
            app.navigationManager.navigateToGeoLocation(lat, lon, idx)

            if (hasMoreQuestions(idx)) {
                app.navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app.navigationManager.finalOverview()
            }
        }

    }


    fun updateAnon(idx: Int, anon: Boolean) {
        thread {
            SurveyManager.of(app).currentSurvey?.let outer@{
                it.controls[idx].let {
                    if (it !is GeoLocationControl) {
                        updateAnon.postValue(false)
                        return@outer
                    }

                    it.answer = it.answer.create(it.answer() ?: "", anon)
                }
                SurveyManager.of(app).saveSurvey(it)
                SurveyManager.of(app).updateSurveyInfoDb(it)
                updateAnon.postValue(true)
            } ?: updateAnon.postValue(false)

        }
    }
}