package org.oursci.android.common.ui.survey.text


import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.os.SystemClock
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.DatePicker
import androidx.core.view.GestureDetectorCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.whiteelephant.monthpicker.MonthPickerDialog
import org.javarosa.core.model.Constants
import org.joda.time.DateTime
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlTextBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.ui.controls.text.TextControlValidator
import org.oursci.android.common.ui.qrscanner.QrScannerViewModel
import timber.log.Timber
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 31.08.17.
 */
class TextControlFragment : BaseFragment() {

    override val searchable = true
    var idx = -1
    private val checkFromUi = AtomicBoolean(false)

    lateinit var binding: LayoutControlFragmentBinding
    private lateinit var controlBinding: ControlTextBinding
    private lateinit var mTextControlValidator: TextControlValidator

    private var control: TextControl? = null


    private val textControlViewModel: TextControlViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(TextControlViewModel::class.java)
    }

    private val scannerViewModel by lazyViewModel<QrScannerViewModel>()
    private var remembered: String? = null


    private val dateObserver = ConsumableObserver<String?> { date, consumed ->
        if (consumed || date == null) {
            return@ConsumableObserver
        }

        controlBinding.etInput.setText(date)
        onConfirmed()
    }

    private val textControlObserver = ConsumableObserver<TextControl> { control, consumed ->

        if (consumed || control == null) {
            return@ConsumableObserver
        }

        this.control = control

        remembered = app().resolveRemember(control.dataName)

        if (control.answer().isNullOrEmpty() && !textControlViewModel.surveyUploaded()) { // only override if null or empty
            remembered?.let {
                val newAnswer = control.answer.create(it, control.answer.anon)
                control.answer = newAnswer
                textControlViewModel.saveSurvey()
            } ?: controlBinding.etInput.postDelayed({
                if (control.dataType == Constants.DATATYPE_DATE
                        || control.readOnly) {
                    return@postDelayed
                }

                val et = controlBinding.etInput
                et.isFocusable = true
                et.isFocusableInTouchMode = true
                et.requestFocus()
                val time = SystemClock.uptimeMillis()
                et.dispatchTouchEvent(MotionEvent.obtain(time, time, MotionEvent.ACTION_DOWN, 0f, 0f, 0))
                et.dispatchTouchEvent(MotionEvent.obtain(time + 10, time + 10, MotionEvent.ACTION_UP, 0f, 0f, 0))
            }, 100)

        }

        binding.swRemember.visibility = when (control.readOnly) {
            true -> View.GONE
            else -> View.VISIBLE
        }


        binding.swRemember.isChecked = remembered?.let { true } ?: false


        mTextControlValidator = TextControlValidator(control)
        controlBinding.control = control
        controlBinding.model = mTextControlValidator


        controlBinding.etInput.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                return@setOnEditorActionListener onConfirmed()
            }
            false
        }

        if (control.dataType == Constants.DATATYPE_DATE) {

            controlBinding.textInputLayout.hint = "Answer: (Long Press to clear)"

            val date = control.answer()?.let {
                try {
                    DateTime.parse(it) ?: DateTime.now()
                } catch (e: Exception) {
                    Timber.e(e)
                    DateTime.now()
                }
            } ?: DateTime.now()
            textControlViewModel.updateDate(date.toString("yyyy-MM-dd"))


            val listener = object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                    textControlViewModel.dateLiveData.observe(this@TextControlFragment, dateObserver)
                    when (control.appearanceHint) {
                        "month-year" -> showMonthPicker()
                        "year" -> showYearPicker()
                        else -> showDatePicker()
                    }

                    return true
                }

                override fun onLongPress(e: MotionEvent?) {
                    controlBinding.etInput.setText("")
                    onConfirmed()
                }
            }

            val detector = GestureDetectorCompat(activity!!, listener)

            controlBinding.etInput.setOnTouchListener { v, event ->
                detector.onTouchEvent(event)
                true
            }
        }


        controlBinding.invalidateAll()
    }


    private val webAnswerObserver = ConsumableObserver<String> { str: String?, consumed: Boolean ->
        if (consumed || str == null) {
            return@ConsumableObserver
        }


        val control = this.control ?: return@ConsumableObserver

        control.answer = control.answer.create(str, control.answer.anon)
        controlBinding.invalidateAll()
    }

    private fun onConfirmed(): Boolean {
        Timber.d("done pressed")
        val text = controlBinding.etInput.text.toString().trim()
        val control = this.control ?: return false


        val valueToRemember = if (binding.swRemember.isChecked) text else null
        app().storeRemember(control.dataName, valueToRemember)

        control.answer.let {
            if (it.answer == text) {
                Timber.d("same sate, not updating")
                return false
            }
        }

        val newAnswer = control.answer.create(text, control.answer.anon)
        control.answer = newAnswer



        Timber.d("new answer is: %s, anon: %s", control.answer, control.answer.anon)
        textControlViewModel.saveSurvey()
        controlBinding.invalidateAll()

        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        Timber.d("activity instance %s", activity)
        activity.allowOverview(true)
        activity.unlockMenus()
        // activity.lockMenus()

        binding.btNext.setOnClickListener {

            onConfirmed()

            if (textControlViewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        idx = arguments?.getInt("idx", 0) ?: 0

        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)
        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_text
        binding.viewStubControl.viewStub?.setOnInflateListener { _, view ->
            controlBinding = ControlTextBinding.bind(view)
            textControlViewModel.loadControlLiveData.observe(this, textControlObserver)
            textControlViewModel.loadControl(idx, TextControl::class.java)

            app().webAnswerData.observe(this, webAnswerObserver)

            controlBinding.btQrcode.setOnClickListener {
                app().navigationManager.navigateToScanner()
            }

            scannerViewModel.mCurrentQrValue.let {
                if (it.isEmpty()) return@let
                controlBinding.etInput.setText(it)
                onConfirmed()
            }
            scannerViewModel.mCurrentQrValue = ""
            binding.swAnon.visibility = View.GONE
            binding.swAnon.setOnCheckedChangeListener { _, checked ->
                if (!checkFromUi.getAndSet(true)) {
                    return@setOnCheckedChangeListener
                }

                Timber.d("on check changed triggered")

                control?.let {
                    app().setRememberAnon(it.dataName, checked)
                }


                onConfirmed()
            }
            checkFromUi.set(true)
        }

        binding.viewStubControl.viewStub?.inflate()
        return binding.root
    }


    open class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            // Use the current date as the default date in the picker
            val viewModel = ViewModelProviders.of(this, app().viewModelFactory).get(TextControlViewModel::class.java)

            val date = viewModel.dateLiveData.value?.let {
                DateTime.parse(it)
            }

            val c = Calendar.getInstance()
            val year = date?.year ?: c.get(Calendar.YEAR)
            val month = date?.monthOfYear ?: c.get(Calendar.MONTH) + 1
            val day = date?.dayOfMonth ?: c.get(Calendar.DAY_OF_MONTH)

            // Create a new instance of DatePickerDialog and return it
            val activity = activity as? BaseActivity
                    ?: return DatePickerDialog(activity, this, year, month - 1, day)
            return DatePickerDialog(activity, R.style.CalendarDialogTheme, this, year, month - 1, day)

        }

        override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
            val viewModel = ViewModelProviders.of(this, app().viewModelFactory).get(TextControlViewModel::class.java)
            viewModel.updateDate(DateTime(year, month + 1, day, 0, 0).toString("yyyy-MM-dd"))
        }
    }

    fun showDatePicker() {
        val newFragment = DatePickerFragment()
        newFragment.show(activity.supportFragmentManager, "datePicker")
    }


    var currentDialog: MonthPickerDialog? = null

    fun showMonthPicker() {
        val activity = activity as? BaseActivity ?: return
        val today = Calendar.getInstance()
        val date = textControlViewModel.dateLiveData.value?.let {
            DateTime.parse(it)
        }

        val year = date?.year ?: today.get(Calendar.YEAR)
        val month = date?.monthOfYear ?: today.get(Calendar.MONTH) + 1

        val builder = MonthPickerDialog.Builder(activity,
                { selectedMonth, selectedYear ->
                    val selected = DateTime(selectedYear, selectedMonth + 1, 1, 0, 0)
                    textControlViewModel.updateDate(selected.toString("yyyy-MM"))
                }, year, month - 1)

        currentDialog = builder
                .setOnYearChangedListener {
                    currentDialog?.onClick(null, 0)
                    currentDialog?.dismiss()
                }
                .build()

        currentDialog?.show()
    }


    fun showYearPicker() {
        val activity = activity as? BaseActivity ?: return
        val today = Calendar.getInstance()
        val date = textControlViewModel.dateLiveData.value?.let {
            DateTime.parse(it)
        }

        val year = date?.year ?: today.get(Calendar.YEAR)

        val builder = MonthPickerDialog.Builder(activity, { _, selectedYear ->
            val selected = DateTime(selectedYear, 1, 1, 0, 0)
            textControlViewModel.updateDate(selected.toString("yyyy"))
        }, year, 0).showYearOnly()

        currentDialog = builder
                .setOnYearChangedListener {
                    currentDialog?.onClick(null, 0)
                    currentDialog?.dismiss()
                }
                .build()

        currentDialog?.show()
    }


}