package org.oursci.android.common.ui.survey.list

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.databinding.ItemSurveyBinding
import org.oursci.android.common.vo.survey.SurveyListItem

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyListAdapter(
        private val onClick: (View, SurveyListItem) -> Unit
) : RecyclerView.Adapter<SurveyListAdapter.ViewHolder>() {

    var items: ArrayList<SurveyListItem> = ArrayList()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.root.tag = position

        items.getOrNull(position)?.let {
            holder.binding.item = it
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSurveyBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.item_survey,
                parent, false)

        binding.root.setOnClickListener { view ->
            val idx = view.tag as Int?

            idx?.let {
                items.getOrNull(it)?.let { onClick(binding.root, it) }
            }
        }

        return ViewHolder(binding)
    }

    class ViewHolder(val binding: ItemSurveyBinding) : RecyclerView.ViewHolder(
            binding.root)
}