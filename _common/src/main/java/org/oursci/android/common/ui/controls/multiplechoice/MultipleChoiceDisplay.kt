package org.oursci.android.common.ui.controls.multiplechoice

import android.text.Spanned
import org.oursci.android.common.html

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceDisplay(val control: MultipleChoiceControl) {

    fun text(): Spanned {
        return html("<font color='black'>${control.label}</font><br>${control.hint}")
    }
}