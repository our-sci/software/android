package org.oursci.android.common.ui.controls.text

import android.text.Spanned
import android.view.View
import org.oursci.android.common.html
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.text.TextControlAnswer

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */

data class TextControl(
        override var answer: TextControlAnswer = TextControlAnswer(),
        val hasLength: Boolean = false,
        val minimumLength: Int = -1,
        val maximumLength: Int = -1,
        val invalidText: String = "",
        val relevance: String = "",
        val constraint: String = "",
        val calculate: String = "",
        val dataType: Int = 1,
        val appearanceHint: String = "",
        override val hint: String = "",
        override val defaultValue: String = "",
        override val label: String = "",
        override val dataName: String = "",
        override val readOnly: Boolean = false,
        override val required: Boolean = false,
        override val requiredText: String = "",
        override val group: Group? = null) : BaseControl<TextControlAnswer>() {

    override fun answer(): String? {
        return answer.answer
    }

    fun header(): Spanned {
        return html("${group?.description?.let { "<small><font color='black'>$it</small><br>" }
                ?: ""}<font color='black'>$label</font><br>$hint")
    }

    // ideally we would have a "note widget"
    // https://docs.opendatakit.org/form-question-types/#note-widget
    // however, for now we use readonly text controls to just show a message to the user
    fun visibility(): Int {
        return when {
            readOnly -> View.GONE
            else -> View.VISIBLE
        }
    }
}