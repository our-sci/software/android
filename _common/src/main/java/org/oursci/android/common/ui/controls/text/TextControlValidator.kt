package org.oursci.android.common.ui.controls.text

import android.text.InputType
import org.javarosa.core.model.Constants
import org.oursci.android.common.databinding.ControlTextBinding
import org.oursci.android.common.ui.controls.BaseControlModel
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */

class TextControlValidator(
        private val textControl: TextControl) : BaseControlModel() {

    override fun errorMessage(): String {
        Timber.d("edit text value: %s", text())

        if (textControl.required && text().isEmpty()) {
            return textControl.requiredText
        }

        if (textControl.hasLength &&
                (text().length > textControl.maximumLength || text().length < textControl.minimumLength)) {
            return textControl.invalidText
        }

        return ""
    }

    override fun valid(): Boolean {
        if (errorMessage().isEmpty()) {
            return true
        }

        return false
    }

    fun text(): String {
        if (textControl.answer.answer == null) {
            return textControl.defaultValue
        }

        return textControl.answer.answer!!
    }

    fun inputType(): Int = when (textControl.dataType) {
        Constants.DATATYPE_DECIMAL -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        Constants.DATATYPE_INTEGER -> InputType.TYPE_CLASS_NUMBER
        else -> InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
    }


}