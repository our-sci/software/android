package org.oursci.android.common.survey

import androidx.room.*
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.webapi.Api

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 17.05.18.
 */

@Dao
interface SurveyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveys(surveys: List<Api.SurveyBody>)

    @Query("delete from online_surveys")
    fun deleteSurveys()


    @Query("select * from online_surveys")
    fun getAllSurveys(): List<Api.SurveyBody>
}

class SurveyDb(app: BaseApplication) {

    val db by lazy {
        Room.databaseBuilder(app, SurveyDatabase::class.java,
                "survey_db")
                .fallbackToDestructiveMigration()
                .build()
    }

    companion object {

        private val lock = Object()

        private var sInstance: SurveyDb? = null

        fun of(app: BaseApplication): SurveyDb {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = SurveyDb(app)
                }

                return sInstance!!
            }
        }
    }

}


@Database(
        entities = [(Api.SurveyBody::class)],
        version = 3,
        exportSchema = false
)

abstract class SurveyDatabase : RoomDatabase() {
    abstract fun surveyInfoDao(): SurveyDao
}