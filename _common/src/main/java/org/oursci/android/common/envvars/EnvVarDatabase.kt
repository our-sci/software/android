package org.oursci.android.common.envvars

import androidx.room.Database
import androidx.room.RoomDatabase
import org.oursci.android.common.organization.OrganizationDao
import org.oursci.android.common.vo.Organization

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */

@Database(
        entities = [EnvVar::class],
        version = 1,
        exportSchema = false
)
abstract class EnvVarDatabase : RoomDatabase() {
    abstract fun envVarDao(): EnvVarDao
}