package org.oursci.android.common.device.persist

import androidx.room.Room
import org.oursci.android.common.BaseApplication

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */
class DevicesDb(val app: BaseApplication) {
    val db by lazy {
        Room.databaseBuilder(app, DevicesDatabase::class.java,
                "devices_database")
                .allowMainThreadQueries()
                .build()
    }


    companion object {

        private val lock = Object()

        private var sInstance: DevicesDb? = null

        fun of(app: BaseApplication): DevicesDb {
            synchronized(lock) {
                val instance = sInstance ?: DevicesDb(app)
                sInstance = instance
                return instance
            }
        }
    }
}