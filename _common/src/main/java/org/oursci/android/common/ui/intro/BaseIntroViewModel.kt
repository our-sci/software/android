package org.oursci.android.common.ui.intro

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.vo.IntroSlide

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */
abstract class BaseIntroViewModel(app: BaseApplication) : BaseViewModel(app) {
    val slides: ConsumableLiveData<Array<IntroSlide>> = ConsumableLiveData()
    var currentPosition: Int = 0
}