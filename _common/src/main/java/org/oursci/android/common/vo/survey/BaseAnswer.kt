package org.oursci.android.common.vo.survey

import org.joda.time.DateTime

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 19.09.17.
 */
abstract class BaseAnswer<T>(
        val answered: DateTime = DateTime(),
        val modified: DateTime = DateTime(),
        var anon: Boolean = false) {
    abstract val answer: T?

    abstract fun create(answer: T?, anon: Boolean = false): BaseAnswer<T>
}