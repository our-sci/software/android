package org.oursci.android.common.ui.survey.loading

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.core.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentSurveyLoadingBinding
import org.oursci.android.common.logSurveyEvent
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.vo.survey.SurveyListItem
import java.net.URLDecoder

/**
 * Created by Manuel Di Cerbo on 27.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */


class LoadingFragment : BaseFragment() {

    lateinit var binding: FragmentSurveyLoadingBinding
    var surveyName: String = ""
    var surveyId: String = ""

    private val loadingViewModel: LoadingViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(LoadingViewModel::class.java)
    }


    private var allowRecentProgress = true
    private var allowRecentDone = true

    private val loadControlObserver = ConsumableObserver<Float> { progress, consumed ->
        if (consumed && !allowRecentProgress) {
            allowRecentProgress = false
            return@ConsumableObserver
        }

        progress?.let {
            binding.tvProgress.text = "Loading Survey, Progress ${it} %"
            binding.progressLoading.progress = it.toInt()
        }


    }

    private val doneObserver = ConsumableObserver<Void> { _, consumed ->

        if (consumed && !allowRecentDone) {
            allowRecentDone = false
            return@ConsumableObserver
        }

        binding.tvProgress.text = "Survey successfully loaded"
        binding.btDone.isEnabled = true
        binding.progressLoading.progress = 100

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title = arguments?.getString("title")
        val desc = arguments?.getString("description")
        surveyName = arguments?.getString("survey_name") ?: ""
        surveyId = arguments?.getString("survey_id") ?: ""


        binding.btDone.isEnabled = false

        loadingViewModel.loadControlLiveData.observe(this, loadControlObserver)
        loadingViewModel.loadDoneLiveData.observe(this, doneObserver)


        allowRecentProgress = true
        allowRecentDone = true

        loadingViewModel.loadSurveyImages(surveyId)

        binding.layoutHead?.item = SurveyListItem(title ?: "sample title",
                desc ?: "sample desc", "soil_carbon", true, surveyId = surveyId)
        ViewCompat.setTransitionName(binding.layoutHead?.root, "heading")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSurveyLoadingBinding.inflate(inflater, container, false)

        binding.btDone.setOnClickListener {
            // app().navigationManager.listSurveys()
            logSurveyEvent(surveyId)
            loadingViewModel.createSurveyInstance(surveyId)
            val answers = arguments?.getString("answers")

            val map = mutableMapOf<String, String>()
            answers?.split("&")?.map {
                val (key, value) = it.split("=")
                map.put(URLDecoder.decode(key, "utf-8"), URLDecoder.decode(value, "utf-8"))
            }
            app().navigationManager.startSurvey(answers = map)
        }


        return binding.root
    }


}