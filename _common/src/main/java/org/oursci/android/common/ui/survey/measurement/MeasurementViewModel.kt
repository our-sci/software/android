package org.oursci.android.common.ui.survey.measurement

import android.content.Intent
import android.net.Uri
import android.text.Spanned
import android.webkit.JavascriptInterface
import com.crashlytics.android.Crashlytics
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.squareup.moshi.JsonAdapter
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONArray
import org.json.JSONObject
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.btlib.BluetoothService
import org.oursci.android.common.btlib.BluetoothStatus
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.envvars.EnvVar
import org.oursci.android.common.envvars.EnvVarDb
import org.oursci.android.common.html
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.nextScript
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.survey.ControlViewModel
import org.oursci.android.common.vo.survey.measurement.MeasurementAnswer
import org.oursci.android.common.webapi.FarmOSClient
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


/**
 * Created by Manuel Di Cerbo on 29.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementViewModel(app: BaseApplication) : ControlViewModel<MeasurementControl>(app) {

    val sensorResult = ConsumableLiveData<Void>()
    val sensorProgressResult = ConsumableLiveData<Float>()
    val sensorDataAvailable = ConsumableLiveData<Void>()
    val storeSuccessResult = ConsumableLiveData<Boolean>()
    val resultLoaded = ConsumableLiveData<Void>()
    val saveRequired = ConsumableLiveData<Void>()
    val deviceConnectedResult = ConsumableLiveData<String>()
    val cookieResult = ConsumableLiveData<Pair<String, String>>()
    val autoRunResult = ConsumableLiveData<String>()

    var controlIdx: Int = -1

    var currentMeasurementScript: MeasurementScript? = null

    val sensorInterface: SensorInterface = SensorInterface()
    val processorInterface: ProcessorInterface = ProcessorInterface()
    val applicationInterface: ApplicationInterface = ApplicationInterface()


    val consoleLog = StringBuffer(1000)


    var result: String? = null
    val resultLock = Object()
    var exportObject = JSONObject()
    var state: String? = null

    var dataBuffer = StringBuffer(1000)
    val dataLock = Object()

    val webInputData = StringBuffer(1000)


    inner class SensorInterface {
        @JavascriptInterface
        fun serialRead(): String {
            // Timber.d("serial read called of script")

            var toSubmit = ""
            synchronized(dataLock) {
                toSubmit = dataBuffer.toString()
                dataBuffer.setLength(0)
            }
            return toSubmit
        }

        @JavascriptInterface
        fun serialWrite(data: String) {
            Timber.d("writing data: %s", data)
            if (BtManager.of(app).connected.get()) {
                BtManager.of(app).write(data)
            } else if (UManager.of(app).connected.get()) {
                UManager.of(app).write(data)
            }
        }
    }

    inner class ProcessorInterface {
        @JavascriptInterface
        fun getResult(): String {
            Timber.d("getResult called")
            synchronized(resultLock) {
                return result ?: ""
            }
        }
    }

    inner class ApplicationInterface {
        @JavascriptInterface
        fun result(result: String) {
            Timber.d("result called")
            synchronized(resultLock) {
                this@MeasurementViewModel.result = result
            }

            if (controlIdx != -1) {
                saveResultSync(controlIdx)
            }

            sensorResult.postValue(null)
        }

        @JavascriptInterface
        fun progress(progress: Float) {
            Timber.d("progress %f", progress)
            sensorProgressResult.postValue(progress)
        }

        @JavascriptInterface
        fun measurementPath(): String {
            return currentMeasurementScript?.dir?.absolutePath ?: ""
        }

        @JavascriptInterface
        fun onSerialDataIntercepted(data: String) {

        }

        @JavascriptInterface
        fun csvExport(key: String, value: String) {
            Timber.d("csvExport %s => %s", key, value)
            exportObject.put(key, value)
        }

        /*
            Setting the state is optional.

            When a state is set, it will show up in the answer's meta object,
            and the measurement is considered "answered",
            if state is "success" (case insensitive).

            When no state is set, the meta object does not contain a state key,
            and is considered answered, when the answer is not blank. (legacy)

         */
        @JavascriptInterface
        fun setState(state: String?) {
            this@MeasurementViewModel.state = state
        }


        @JavascriptInterface
        fun csvExport(value: String) {
            Timber.e("Dont use this anymore!")
            //exportBuffer.append("$value; ")
        }

        @JavascriptInterface
        fun save() {
            Timber.d("save called from script")
            saveRequired.postValue(null)
        }

        @JavascriptInterface
        fun getAnswer(dataName: String): String? = SurveyManager.of(app).currentSurvey?.controls?.find {
            val prefix = dataName.split(".").first()
            it.dataName == "/data/%s:label".format(prefix)
        }?.let {
            when (it) {
                is MultipleChoiceControl -> it.answer.answer?.joinToString(" ") { input ->
                    it.options.first { it.value == input }.value
                }
                is MeasurementControl -> {

                    val scriptApi = try {
                        currentMeasurementScript?.manifestWithExtras?.optInt("apiVersion", 1) ?: 1
                    } catch (e: Exception) {
                        Timber.e(e)
                        1
                    }

                    if (scriptApi <= 1) {
                        return@let it.answer()
                    }


                    val part = dataName.split(".").getOrNull(1)
                    val idx = dataName.split(".", limit = 3).last()

                    // dataName: 'scan_1.data.device_battery'
                    val answer = it.answer() ?: return@let ""
                    val json = try {
                        JSONObject(answer)
                    } catch (e: Exception) {
                        return@let ""
                    }

                    val inner = json.optJSONObject(part) ?: return@let ""
                    return@let inner.optString(idx, "")
                }
                else -> it.answer()
            }
        }

        @JavascriptInterface
        fun getCredentials(id: String): String = try {
            var token: String? = null
            FirebaseAuth.getInstance()?.currentUser?.getIdToken(false)?.let {
                token = Tasks.await(it, 10, TimeUnit.SECONDS).token
            }

            val t = token ?: throw Exception("token for Oursci unavailable")

            val r = app.api.fetchTpCredentials(t).execute()
            val credentials = r.body() ?: throw Exception("error parsing body")
            val cred = credentials.find {
                "${it.id}" == id
            } ?: throw Exception("unable to find credential with ID: $id")


            val (farmosToken, farmosCookie) = when (cred.type) {
                "FARMOS" -> {
                    val farmOSClient = FarmOSClient(cred.url, cred.username, cred.password)
                    val c = farmOSClient.authenticate(cred.username, cred.password)
                    c?.let { it.token to it.cookie } ?: "" to ""
                }
                else -> "" to ""
            }


            cookieResult.postValue(cred.url to farmosCookie)

            Thread.sleep(500) // settle until cookie is accepted

            val json = JSONObject()
            json.apply {
                put("id", cred.id)
                val user = JSONObject()
                user.apply {
                    put("id", cred.user.id)
                    put("username", cred.user.username)
                    put("displayName", cred.user.displayName)
                }
                put("url", cred.url)
                put("username", cred.username)
                put("token", cred.token)
                put("parameters", cred.parameters)
                put("node", cred.note)
                put("type", cred.type)
                put("password", cred.password)
                put("farmosToken", farmosToken)
                put("farmosCookie", farmosCookie)
            }
            json.toString()
        } catch (e: Exception) {
            JSONObject().apply {
                put("exception", e.message)
            }.toString()
        }

        @JavascriptInterface
        fun getMeta() = SurveyManager.of(app).currentSurvey?.let {
            val json = JSONObject()
            json.put("started", it.startTime)
            json.put("formId", it.formId)
            json.put("instanceId", it.instanceID)
            json.put("description", it.description)
            json.put("organizationId", it.organizationId)
            json.put("name", it.name)
            json.put("uploaded", it.uploaded)
            val controls = JSONArray()

            it.controls.forEach { control ->
                val obj = JSONObject()
                obj.put("name", control.dataName)
                obj.put("readOnly", control.readOnly)
                obj.put("required", control.required)
                obj.put("label", control.label)
                obj.put("hint", control.hint)
                obj.put("class", control::class.java.simpleName)
                when (control) {
                    is MultipleChoiceControl -> {
                        val arr = JSONArray()
                        control.options.forEach {
                            val opt = JSONObject().apply {
                                put("value", it.value)
                                put("text", it.text)
                            }
                            arr.put(opt)
                        }
                        obj.put("options", arr)
                    }
                    is ExternalControl -> {
                        val arr = JSONArray()
                        if (control.params.type == ExternalControl.Type.ONTOLOGY) {
                            app.getAllOntologies().find { ont ->
                                ont.id == control.params.url
                            }?.let { ont -> ont.items }?.forEach { ontItem ->
                                val opt = JSONObject().apply {
                                    put("value", ontItem.id)
                                    put("text", ontItem.name)
                                }
                                arr.put(opt)
                            }
                        }
                    }
                }
                controls.put(obj)
            }
            json.put("controls", controls)
            Timber.d(json.toString(4))
            json.toString()
        } ?: ""

        @JavascriptInterface
        fun getEnvVar(key: String?, default: String?): String? {
            key ?: return default


            return try {
                EnvVarDb.of(app).db.envVarDao().getEnvVar(key)?.value
            } catch (e: Exception) {
                e.printStackTrace()
                default
            }
        }

        @JavascriptInterface
        fun setEnvVar(key: String?, value: String?, description: String?): Boolean {
            key ?: return false

            return try {
                value?.let {
                    EnvVarDb.of(app).db.envVarDao().let {
                        try {
                            it.insert(EnvVar(key, value, description ?: "", DateTime().millis))
                        } catch (e: Exception) {

                        }
                        true
                    }
                } ?: EnvVarDb.of(app).db.envVarDao().let {
                    it.deleteEnvVar(EnvVar(key, "", "", 0))
                    true
                }
            } catch (e: Exception) {
                Crashlytics.setString("error", "problem setting environment variable")
                Crashlytics.logException(e)
                false
            }
        }

        @JavascriptInterface
        fun getWebInput(): String {
            val ret = webInputData.toString()
            webInputData.setLength(0)
            return ret
        }

        @JavascriptInterface
        fun email(to: String?, subject: String?, text: String?, cc: String?) {
            if (to == null) {
                return
            }

            val intent = Intent(Intent.ACTION_SEND).apply {
                data = Uri.parse("mailto:")
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, to.split(';').toTypedArray())
                subject?.let { putExtra(Intent.EXTRA_SUBJECT, it) }
                text?.let { putExtra(Intent.EXTRA_TEXT, it) }
                cc?.let { putExtra(Intent.EXTRA_CC, it) }
            }

            app.startActivity(Intent.createChooser(intent, "Send E-Mail ...").apply { addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) })
        }

        @JavascriptInterface
        fun currentId(): String {
            if (controlIdx < 0) {
                return ""
            }

            return SurveyManager.of(app).currentSurvey?.controls?.get(controlIdx)?.dataName?.let {
                it.substring("/data/".length, it.length - ":label".length)
            } ?: ""
        }
    }

    fun createMeasurementControl(id: String) {
        Timber.d("loading control for measurement with id ${id}")

        currentMeasurementScript = MeasurementLoader.of(app).fromId(id)

        registerCallback()

        currentMeasurementScript?.let {
            loadControlLiveData.postValue(MeasurementControl(
                    id = it.id,
                    label = it.name,
                    hint = it.description,
                    required = false
            ))
        } ?: loadControlLiveData.postValue(null)
    }

    fun registerCallback() {
        BtManager.of(app).registerCallback({ _ ->

        }, { status ->
            when (status) {
                BluetoothStatus.CONNECTED -> {
                    Timber.d("status connected")
                    deviceConnectedResult.postValue("CONNECTED")
                }
                BluetoothStatus.CONNECTING -> {
                    Timber.d("status connecting")
                    deviceConnectedResult.postValue("CONNECTING")
                }
                BluetoothStatus.NONE -> {
                    Timber.d("status none")
                    deviceConnectedResult.postValue("NONE")
                }
            }
        }, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
            }
            sensorDataAvailable.postValue(null)
        })

        UManager.of(app).registerCallback({ _ ->
        }, {}, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)

            }
            sensorDataAvailable.postValue(null)
        })
    }

    override fun loadControl(idx: Int, cls: Class<MeasurementControl>) {
        val v = SurveyManager.of(app).currentSurvey?.controls?.get(idx) as MeasurementControl
        currentMeasurementScript = MeasurementLoader.of(app).fromId(v.id)
        registerCallback()
        super.loadControl(idx, cls)
    }

    fun redoMeasurement(measurementId: String, idx: Int = -1) {
        if (idx != -1) {
            SurveyManager.of(app).currentSurvey?.let {
                thread {
                    (it.controls[idx] as? MeasurementControl)?.let {
                        it.answer = MeasurementAnswer(
                                anon = it.answer.anon
                        )
                        SurveyManager.of(app).saveCurrentSurvey()
                    }
                    app.navigationManager.rebuildSurveyStack(idx, pop = true)
                }
            }
        } else {
            app.navigationManager.pop()
        }
    }

    fun isConnected(): Boolean = if (UManager.of(app).hasDevice()) {
        UManager.of(app).connected.get()
    } else BluetoothService.getDefaultInstance().status == BluetoothStatus.CONNECTED


    fun connectToLastDevice(): Boolean {

        if (UManager.of(app).hasDevice()) {
            Timber.d("Trying to connect to USB device...")
            UManager.of(app).reconnect()
            return true
        } else if (BtManager.of(app).hasDevice()) {
            Timber.d("Trying to connect to Bluetooth device...")
            BtManager.of(app).reconnect()
            return true
        }

        return false
    }

    fun deviceName(): String? {
        return when {
            BtManager.of(app).hasDevice() -> BtManager.of(app).currentDevice?.name ?: "???"
            UManager.of(app).hasDevice() -> "USB device"
            else -> "???"
        }
    }

    fun cancelConnect() {
        UManager.of(app).clearDevice()
        BtManager.of(app).cancelConnect(null)
    }

    fun clearResult() {
        Timber.d("clear result called")

        if (controlIdx == -1) {
            return;
        }

        synchronized(resultLock) {
            result = null
            exportObject = JSONObject()
            state = null
        }
    }

    fun clearBuffer() {
        dataBuffer.setLength(0)
    }

    fun saveResult(idx: Int) {
        Timber.d("called saveResult...")
        try {
            if (idx == -1) {
                thread {
                    storeSuccessResult.postValue(saveResultSync(idx))
                    autoRunResult.postValue("")
                }
            } else {
                SurveyManager.of(app).currentSurvey?.controls?.get(idx)?.answer()?.let {
                    val obj = JSONObject(it)
                    if (obj.optJSONObject("data")?.length() ?: 0 == 0) {
                        state = obj.optJSONObject("meta")?.optString("state")

                        thread {
                            storeSuccessResult.postValue(saveResultSync(idx))
                            autoRunResult.postValue("")
                        }
                    } else {
                        autoRunResult.postValue("")
                    }
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun saveResultSync(idx: Int): Boolean {
        var copyA = ""

        synchronized(resultLock) {
            copyA = result + ""
        }

        val copy = copyA
        result?.let {
            currentMeasurementScript?.let { script ->
                val triggerFarmosSync = try {
                    script.manifestWithExtras.optBoolean("triggerFarmOsSync", false)
                } catch (e: Exception) {
                    Timber.e(e)
                    false
                }

                val scriptApi = try {
                    script.manifestWithExtras.optInt("apiVersion", 1)
                } catch (e: Exception) {
                    Timber.e(e)
                    1
                }

                if (triggerFarmosSync) {
                    app.refreshFarmOs()
                }

                app.publishRecentResult(copy);
                val file = MeasurementLoader.of(app).saveMeasurement(script, copy)

                val resultObject = JSONObject()
                val metaObject = JSONObject()


                if (idx != -1 && file != null) {
                    Timber.d("saving measurement path to survey")
                    SurveyManager.of(app).currentSurvey?.controls?.get(idx)?.let {
                        Timber.d("creating answer for measurement")
                        metaObject.put("filename", file.name)
                        metaObject.put("scriptID", script.id)
                        metaObject.put("date", ISODateTimeFormat.dateTime().print(DateTime()))
                        state?.let {
                            metaObject.put("state", state)
                        }

                        resultObject.put("meta", metaObject)

                        resultObject.put("data", exportObject)
                        (it as MeasurementControl).answer = it.answer.create(resultObject.toString(2), it.answer.anon)
                        Timber.d("answer created ${it.answer()}")
                    }
                    SurveyManager.of(app).saveCurrentSurvey()
                }
                return file != null
            }

        } ?: return false
    }

    fun loadResultFromPath(filePath: String) {
        Timber.d("Load result from path");
        thread {
            val res = MeasurementLoader.of(app).loadMeasurementResult(filePath) ?: ""
            synchronized(resultLock) {
                result = res + ""
            }
            resultLoaded.postValue(null)
        }
    }

    fun appendLog(text: String) {
        consoleLog.append(text)
    }

    fun consoleLog(): Spanned {
        return html(consoleLog.toString())
    }

    fun requireDevice() = currentMeasurementScript?.requireDevice ?: true

    fun loadAllAnswersAsResult() {
        Timber.d("loadAllAnswersAsResult");

        if (controlIdx == -1) {
            return;
        }
        result = ""
        val json = JSONObject()
        SurveyManager.of(app).currentSurvey?.controls?.forEach {
            val rawName = it.dataName
            val k = rawName.substring("/data/".length, rawName.length - ":label".length)
            val v = when (it) {
                is MultipleChoiceControl -> it.answer.answer?.joinToString("/") { input ->
                    it.options.first { it.value == input }.value
                }
                else -> it.answer()
            }
            json.put(k, v)
        }
        json.put("metaInstanceID", SurveyManager.of(app).currentSurvey?.instanceID)

        result = json.toString(2)
        Timber.d("setting result:\n%s", result)
    }

    fun autoRunNext(currentIdx: Int) {
        val survey = SurveyManager.of(app).currentSurvey ?: return
        val next = try {
            survey.nextScript()
        } catch (e: Exception) {
            Timber.e(e)
            app.navigationManager.finalOverview(autoRun = true)
            return
        }

        val nextIdx = survey.controls.indexOf(next)

        if (currentIdx < nextIdx) {
            app.navigationManager.rebuildSurveyStack(
                    nextIdx,
                    moveToFirstControlInGroup = true,
                    autoRun = true)
        } else {
            app.navigationManager.finalOverview(autoRun = true)
        }
    }


}