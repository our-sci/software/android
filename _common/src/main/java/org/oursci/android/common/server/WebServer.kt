package org.oursci.android.common.server

import android.content.res.AssetManager
import fi.iki.elonen.NanoHTTPD
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.survey.SurveyLoader
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.utils.Compression
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.StringWriter
import java.nio.charset.Charset

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 31.07.17.
 */
class WebServer(val app: BaseApplication) : NanoHTTPD("0.0.0.0", app.WEB_SERVER_PORT) {

    private var mMostRecentResult: String = ""

    companion object {
        var instance: WebServer? = null
        fun of(app: BaseApplication): WebServer {
            if (instance == null) {
                instance = WebServer(app)
            }

            return instance!!
        }
    }


    override fun serve(session: IHTTPSession?): Response {
        if (session == null) {
            return super.serve(session)
        }

        if (session.uri.endsWith("/upload_measurement")) {

            val tmpDir = File(app.cacheDir, "upload_measurement")
            if (!tmpDir.deleteRecursively()) {
                Timber.e("error removing tmpdir recursively ${tmpDir.absolutePath}")
            }


            if (!tmpDir.exists()) {
                if (!tmpDir.mkdirs()) {
                    Timber.e("unable to create directory %s", tmpDir.absolutePath)
                    return fail()
                }
            }

            if (!tmpDir.isDirectory) {
                Timber.e("not a directory %s", tmpDir.absolutePath)
                return fail()
            }


            try {
                val tmpFile = File(app.cacheDir, "upload_file.zip")
                tmpFile.deleteOnExit()

                if (tmpFile.exists()) {
                    if (!tmpFile.delete()) {
                        val reason = "unable to delete temp zip file ${tmpFile.absolutePath}"
                        Timber.e(reason)
                        app.onMeasurementLoadError(reason)
                        return fail(reason)
                    }
                }

                var remain: Int = session.headers["content-length"]?.toIntOrNull() ?: -1
                if (remain == -1) {
                    val reason = "unable to parse content length"
                    Timber.e(reason)
                    app.onMeasurementLoadError(reason)
                    return fail(reason)
                }

                val block = 4096
                val buffer = ByteArray(block)

                Timber.d("reading len bytes: %d", remain)

                try {
                    session.inputStream.use {
                        tmpFile.outputStream().use {
                            while (true) {
                                val next = if (remain >= block) block else remain
                                val len = session.inputStream.read(buffer, 0, next)
                                remain -= len
                                it.write(buffer, 0, len)

                                if (remain <= 0) {
                                    break
                                }
                            }
                        }
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    val reason = "error saving stream to zip file ${tmpFile.absolutePath}, ${e.message}"
                    app.onMeasurementLoadError(reason)
                    return fail(reason)
                }

                Timber.d("tmp file size: ${tmpFile.length()}")


                Compression().unzip(tmpFile.inputStream(), tmpDir)
                MeasurementLoader.of(app).load(tmpDir, { measurement ->
                    app.onMeasurementScriptUploaded(measurement)
                }, { error ->
                    app.onMeasurementLoadError(error)
                })
            } catch (e: Exception) {
                Timber.e(e)
                app.onMeasurementLoadError("unable to decompress http stream: ${e.message}")
                return fail("error extracting zip")
            }

            val resp = newFixedLengthResponse("ok")
            resp.addHeader("Content-Type", "text/plain")
            return resp
        } else if (session.uri.endsWith("/upload_survey")) {

            val tmpDir = File(app.cacheDir, "upload_survey")
            if (!tmpDir.deleteRecursively()) {
                Timber.e("error removing tmpdir recursively ${tmpDir.absolutePath}")
            }


            if (!tmpDir.exists()) {
                if (!tmpDir.mkdirs()) {
                    Timber.e("unable to create directory %s", tmpDir.absolutePath)
                    return fail()
                }
            }

            if (!tmpDir.isDirectory) {
                Timber.e("not a directory %s", tmpDir.absolutePath)
                return fail()
            }


            try {
                val tmpFile = File(app.cacheDir, "survey.xml")
                tmpFile.deleteOnExit()

                if (tmpFile.exists()) {
                    if (!tmpFile.delete()) {
                        val reason = "unable to delete temp xml file ${tmpFile.absolutePath}"
                        Timber.e(reason)
                        app.onSurveyLoadError(reason)
                        return fail(reason)
                    }
                }

                var remain: Int = session.headers["content-length"]?.toIntOrNull() ?: -1
                if (remain == -1) {
                    val reason = "unable to parse content length"
                    Timber.e(reason)
                    app.onSurveyLoadError(reason)
                    return fail(reason)
                }

                val block = 4096
                val buffer = ByteArray(block)

                Timber.d("reading len bytes: %d", remain)

                try {
                    session.inputStream.use {
                        tmpFile.outputStream().use {
                            while (true) {
                                val next = if (remain >= block) block else remain
                                val len = session.inputStream.read(buffer, 0, next)
                                remain -= len
                                it.write(buffer, 0, len)

                                if (remain <= 0) {
                                    break
                                }
                            }
                        }
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    val reason = "error saving stream to xml file ${tmpFile.absolutePath}, ${e.message}"
                    app.onSurveyLoadError(reason)
                    return fail(reason)
                }

                Timber.d("tmp file size: ${tmpFile.length()}")


                val survey = SurveyManager.of(app).createAndLoadSurvey(tmpFile)
                if (survey != null) {
                    app.onSurveyLoaded(survey)
                } else {
                    app.onSurveyLoadError("unable to load survey")
                }
            } catch (e: Exception) {
                Timber.e(e)
                app.onMeasurementLoadError("unable to decompress http stream: ${e.message}")
                return fail("error saving xml")
            }

            val resp = newFixedLengthResponse("ok")
            resp.addHeader("Content-Type", "text/plain")
            return resp
        } else if (session.uri.endsWith("/input.html")) {
            val t = app.assets.open("webserver/input.html").bufferedReader().use { it.readText() }
            return newFixedLengthResponse(t)
        } else if (session.uri.endsWith("/answer")) {
            val map = HashMap<String, String>()

            session.parseBody(map)
            map["postData"]?.let(app::answerFromWebserver)


            /*


            var remain: Int = session.headers["len"]?.toIntOrNull()
                    ?: return newFixedLengthResponse("error")


            val block = 4096
            val buffer = ByteArray(block)
            val out = ByteArrayOutputStream(remain)

            session.inputStream.use {
                while (true) {
                    val next = if (remain >= block) block else remain
                    val len = session.inputStream.read(buffer, 0, next)
                    remain -= len
                    out.write(buffer, 0, len)

                    if (remain <= 0) {
                        break
                    }
                }
            }
            app.answerFromWebserver(out.toString("utf-8"))
            */
            return newFixedLengthResponse("ok")
        } else if (session.uri.endsWith("/measurement")) {
            val resp = newFixedLengthResponse(mMostRecentResult)
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Max-Age", "3628800");
            resp.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
            resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
            resp.addHeader("Access-Control-Allow-Headers", "Authorization");
            return resp
        }




        return newFixedLengthResponse("<html><body>Hello World!</body></html>\n")
    }

    private fun fail(reason: String = ""): Response {
        val resp = newFixedLengthResponse(
                if (reason.isEmpty()) "error" else "error: $reason")
        resp.addHeader("Content-Type", "text/plain")
        return resp
    }

    fun publishResult(result: String) {
        mMostRecentResult = result
    }
}