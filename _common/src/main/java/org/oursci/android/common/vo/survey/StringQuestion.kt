package org.oursci.android.common.vo.survey

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 31.08.17.
 */
data class StringQuestion(val caption: String, val hint: String, val defaultValue: String)