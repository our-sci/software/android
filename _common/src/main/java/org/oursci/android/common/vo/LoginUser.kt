package org.oursci.android.common.vo

import android.net.Uri
import java.security.SecureRandom

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 27.07.17.
 */
data class LoginUser(val username: String, val imageUri: Uri, val userID: String) {

    private val greetings = arrayOf(
            "Hi", "Hello", "Good day", "Guten Tag", "Grüezi", "Bonjour", "Ciao",
            "Dobar dan", "jó napod", "Howdy", "Heya", "Nice to have you", "No time like the present",
            "Buenos días", "Hellas"
    )

    fun loggedIn(): Boolean {
        return !username.isEmpty()
    }


    fun userName(): String {
        val g = greeting()
        if (username.isNotEmpty()) {
            return "$g $username"
        }

        return "$g scientist, press here to log in"
    }

    private fun greeting() = greetings.let {
        it[SecureRandom().nextInt(it.size)]
    }
}
