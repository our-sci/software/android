const express = require("express");
const app = express();
const PORT = 9091;
const vm = require('vm');
const util = require('util');
const bodyParser = require('body-parser');


app.use(bodyParser.text());
app.use(express.static("../"));


app.get('/files', function (req, res) {
  res.send(["temps_sensor.js", "temp_survey.js"]);
})


app.post('/run', function (req, res) {
    console.log("ran script:\n"+util.inspect(req.body));
    try {
        var script = new vm.Script(req.body, {
            displayErrors: true,
            filename: "script.js"
        });
        script.runInThisContext();
        console.log("ran script:\n"+util.inspect(script));
    }catch(err){
        console.log(util.inspect(err));
        var b64 = new Buffer(util.inspect(err)).toString("base64");
        console.log(b64);
        res.send({"status": "error", "error" : b64});
        return
    }

    res.send({status: "success"});
})

app.listen(PORT, function () {
  console.log('Example app listening on port '+PORT+'!')
})