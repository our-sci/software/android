package org.oursci.android.common

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.joda.time.DateTime
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.oursci.android.common.device.persist.DevicesDatabase
import org.oursci.android.common.device.persist.KnownBluetooth
import org.oursci.android.common.device.persist.KnownBluetoothDao
import timber.log.Timber
import java.io.IOException

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */
@RunWith(AndroidJUnit4::class)
class KnownDevicesTest {
    private lateinit var dao: KnownBluetoothDao
    private lateinit var db: DevicesDatabase

    @Before
    fun setupDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
                context, DevicesDatabase::class.java).build()
        dao = db.knownBluetoothDao()
        Timber.tag("KnownDevicesTest")
        Timber.plant(Timber.DebugTree())
    }


    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    fun printAll(){
        val devices = dao.getAll()
        devices.forEach {
            Timber.d("device: %s, %s, %s", it.mac, it.name, it.lastConnected.timeAgo())
        }
    }

    @Test
    @Throws(Exception::class)
    fun testDb() {

        Timber.d("Test Start")
        printAll()
        dao.insertKnownBluetooth(KnownBluetooth(
                "1245",
                "reflectometer",
                DateTime().minusHours(2).millis,
                "no info available"
        ))


        dao.insertKnownBluetooth(KnownBluetooth(
                "1245",
                "reflectometer 2",
                DateTime().minusHours(2).millis,
                "new info"
        ))


        printAll()
    }


}