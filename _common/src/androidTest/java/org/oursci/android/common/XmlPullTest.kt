package org.oursci.android.common

import android.Manifest
import android.os.Environment
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.oursci.android.common.survey.SurveyLoader
import java.io.File


/**
 * Created by Manuel Di Cerbo on 27.04.18.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */


@RunWith(AndroidJUnit4::class)
class XmlPullTest {

    @Rule @JvmField
    public val mRuntimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)


    @Test
    fun hello() {
        println("Hello world")
        File(Environment.getExternalStorageDirectory(), "our_scikit/surveys/build_anon-survey-w-geoloc-Survey-for-testing-anon-with-geolocation-picker_1525105867/instances").listFiles()?.forEach{
            if(it.isDirectory) {
                return@forEach
            }

            println("anon for file %s".format(it.name))
            SurveyLoader.AnonReader(it).read()?.joinToString(", ")?.let { print(it) }
        } ?: println("error, no files found")
    }

}