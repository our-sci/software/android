package org.oursci.android.common

import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
abstract class BaseTest {

    private val logTag = this::class.java.simpleName
    init {
        Timber.plant(object: Timber.Tree() {
            override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
                println("$message")
            }
        })
    }
}