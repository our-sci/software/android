package org.oursci.android.common

import org.junit.Assert
import org.junit.Test
import org.oursci.android.common.ui.controls.external.ExternalControlAdapter

/**
 * Created by Manuel Di Cerbo on 16.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class ExternalControlTest {


    @Test
    fun testExternalControlParser() {
        val assertMap = mapOf(
                "farmos-area;Pick FarmOS Area" to ExternalControlAdapter.Info(
                        hint = "Pick FarmOS Area",
                        name = "",
                        type = "farmos-area"
                ),
                "Simple hint field" to ExternalControlAdapter.Info(
                        hint = "Simple hint field",
                        name = "",
                        type = ""
                ),
                "csv;https://app.our-sci.net/api;Pick FarmOS Area" to ExternalControlAdapter.Info(
                        hint = "Pick FarmOS Area",
                        name = "https://app.our-sci.net/api",
                        type = "csv"
                ),
                ";;;;;;" to ExternalControlAdapter.Info(
                        hint = ";;;;;;",
                        name = "",
                        type = ""
                ),
                "" to ExternalControlAdapter.Info(
                        hint = "",
                        name = "",
                        type = ""
                )
        )

        assertMap.forEach { hint, info ->
            println(hint)
            val parsed = ExternalControlAdapter.parseParams(hint)
            println(parsed)
            Assert.assertEquals(info, parsed)
        }

    }


    @Test
    fun testQualify() {
        val assertMap = mapOf(
                null to false,
                "hello world" to false,
                "farmos-area;Pick FarmOS Area" to true,
                "Simple hint field" to false,
                "csv;https://app.our-sci.net/api;Pick FarmOS Area" to true,
                ";" to true,
                ";;" to true,
                ";;;" to false,
                "" to false
                )

        assertMap.forEach { hint, qualifies ->
            println(hint)
            Assert.assertEquals(ExternalControlAdapter.qualifies(hint), qualifies)
        }
    }
}