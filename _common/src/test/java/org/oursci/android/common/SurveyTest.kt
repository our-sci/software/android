package org.oursci.android.common

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.oursci.android.common.survey.SurveyLoader
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.vo.survey.Survey
import org.w3c.dom.Text
import timber.log.Timber
import java.io.File
import java.lang.AssertionError
import kotlin.Exception

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class SurveyTest : BaseTest() {

    val surveyLoader: SurveyLoader = SurveyLoader(
            File("../mock/forms")
    ) { "uid-oursci" }


    @Before
    fun clearInstances() {
        val forms = surveyLoader.listAllSurveyFiles()
        forms.forEach {
            val instances = File(it.parent, "instances")
            instances.deleteRecursively()
        }
    }

    @Test
    fun listForms() {
        val forms = surveyLoader.listAllSurveyFiles()
        Timber.d("form list:")
        Timber.d("forms:")
        Timber.d(forms.joinToString("\n") { it.path })

        assertEquals(forms.size, 2)
        assertEquals(forms[0].path, "../mock/forms/Example Post-planting form/survey.xml")
        assertEquals(forms[1].path, "../mock/forms/Example weekly management survey/survey.xml")
    }


    fun loadFormPair(name: String, instanceFile: File? = null) = {
        val forms = surveyLoader.listAllSurveyFiles()
        val form = forms.find { it.path == "../mock/forms/$name/survey.xml" }
                ?: throw AssertionError("unable to find form")

        val loadedForm = surveyLoader.loadFormByPath(form.path, surveyLoader.newInstancePath(form.path))
        surveyLoader.fromFormEntryController(loadedForm, instanceFile)
    }

    fun createSurveyInstance(name: String): Survey? {
        return surveyLoader.createNewSurvey(name)
    }

    fun loadPostPlantingForm(): Survey {
        val pair = loadFormPair("Example Post-planting form")()
        return pair.first
    }


    @Test
    fun testLoadPostPlantingForm() {
        val survey = loadPostPlantingForm()
        Timber.d("number of controls: %d", survey.controls.size)
        Timber.d("number of groups: %d", survey.groups.size)

        assertEquals(survey.controls.size, 54)
        assertEquals(survey.groups.size, 6)
    }

    @Test
    fun testPostPlantingRelevance() {
        val survey = createSurveyInstance("Example Post-planting form")
                ?: throw Exception("can't create survey")

        val model = survey.fec.model


        for ((group, formIndex) in survey.groupIndexMap) {
            Timber.d("isIndexRelevant: ${group.id} => ${model.isIndexRelevant(formIndex)}")
        }


        for ((control, formIndex) in survey.indexMap) {
            Timber.d("isIndexRelevant: ${control.dataName} => ${model.isIndexRelevant(formIndex)}")
        }

        val landPrepControl = survey.controls.find { it.dataName == "/data/land_prep_method:label" }
                as? MultipleChoiceControl
                ?: throw Exception("can't find land pre method question")

        landPrepControl.answer = landPrepControl.answer.create(arrayOf("Tillage"))
        surveyLoader.persist(survey, true)

        for ((group, formIndex) in survey.groupIndexMap) {
            Timber.d("isIndexRelevant: ${group.id} => ${model.isIndexRelevant(formIndex)}")
        }

        for ((control, formIndex) in survey.indexMap) {
            Timber.d("isIndexRelevant: ${control.dataName} => ${model.isIndexRelevant(formIndex)}")
        }
    }

    @Test
    fun testExternalControl() {
        val verifyMap = mapOf(
                "/data/Field_ID" to ExternalControl::class,
                "/data/cover_crop_group/cover_crop_plant_month-1" to TextControl::class,
                "/data/cover_crop_group/cover_crop_plant_month-2" to TextControl::class,
                "/data/crop_history_group/crop_history-1" to TextControl::class,
                "/data/crop_history_group/crop_history_plant-1" to TextControl::class,
                "/data/crop_history_group/crop_history_harvest-1" to TextControl::class,
                "/data/crop_history_group/crop_history-2" to TextControl::class,
                "/data/crop_history_group/crop_history_plant-2" to TextControl::class,
                "/data/crop_history_group/crop_history_harvest-2" to TextControl::class,
                "/data/tillage_group/tillage_date-1" to TextControl::class,
                "/data/tillage_group/tillage_date-2" to TextControl::class,
                "/data/tillage_group/tillage_date-3" to TextControl::class,
                "/data/mulch_group/mulch_date" to TextControl::class,
                "/data/planting_date" to TextControl::class,
                "/data/seed_source" to TextControl::class,
                "/data/transplant_group/potting_soil_brand" to TextControl::class,
                "/data/transplant_group/propagation_location" to TextControl::class,
                "/data/transplant_group/seedling_treatment_date" to TextControl::class,
                "/data/transplant_group/seedling_treatment_name" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_name-1" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_source-1" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_trace" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_trace_rate-1" to TextControl::class
        )

        val form = surveyLoader.loadForm("Example Post-planting form")
                ?: throw Exception("unable to load form")


        verifyMap.forEach { id, type ->
            val control = form.controls.find {
                "$id:label" == it.dataName
            } ?: throw Exception("unable to find control with id $id")

            assertEquals(type, control::class)
        }
    }


    @Test
    fun testOntologyControl() {
        val verifyMap = mapOf(
                "/data/Field_ID" to ExternalControl::class,
                "/data/cover_crop_group/cover_crop_plant_month-1" to TextControl::class,
                "/data/cover_crop_group/cover_crop_plant_month-2" to TextControl::class,
                "/data/crop_history_group/crop_history-1" to TextControl::class,
                "/data/crop_history_group/crop_history_plant-1" to TextControl::class,
                "/data/crop_history_group/crop_history_harvest-1" to TextControl::class,
                "/data/crop_history_group/crop_history-2" to TextControl::class,
                "/data/crop_history_group/crop_history_plant-2" to TextControl::class,
                "/data/crop_history_group/crop_history_harvest-2" to TextControl::class,
                "/data/tillage_group/tillage_type-1" to ExternalControl::class,
                "/data/tillage_group/tillage_date-1" to TextControl::class,
                "/data/tillage_group/tillage_date-2" to TextControl::class,
                "/data/tillage_group/tillage_date-3" to TextControl::class,
                "/data/mulch_group/mulch_date" to TextControl::class,
                "/data/planting_date" to TextControl::class,
                "/data/seed_source" to TextControl::class,
                "/data/transplant_group/potting_soil_brand" to TextControl::class,
                "/data/transplant_group/propagation_location" to TextControl::class,
                "/data/transplant_group/seedling_treatment_date" to TextControl::class,
                "/data/transplant_group/seedling_treatment_name" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_name-1" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_source-1" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_trace" to TextControl::class,
                "/data/Fertilizer_group/fertilizer_trace_rate-1" to TextControl::class
        )

        val form = surveyLoader.loadForm("From with Ontology")
                ?: throw Exception("unable to load form")


        verifyMap.forEach { id, type ->
            val control = form.controls.find {
                "$id:label" == it.dataName
            } ?: throw Exception("unable to find control with id $id")

            assertEquals(type, control::class)
            when (control) {
                is ExternalControl -> {
                    Timber.d(control.params.type.toString())
                }
                else -> {
                }
            }
        }
    }
}