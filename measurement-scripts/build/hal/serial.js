var port = null
SerialPort = true

serialCallback = null;


function dataAvailable() {
    if (serialCallback != null) {
        serialCallback(sensor.serialRead());
    }
}


if (!isInBrowser()) {
    SerialPort = require('serialport');
    if (typeof serialDevice == "undefined") {
        console.log("create device config under ./config/port.json, see readme, using /dev/ttyACM0")

        serialDevice = {
            "serialport":
            "/dev/ttyACM0"
        }
    }

    port = new SerialPort(serialDevice.serialport, {
        baudRate: 115200,
        dataBits: 8,
        stopBits: 1,
        parity: "none"
    }, function (err) {
        if (err != null) {
            console.log("error init");
            console.log('Error: ', err);
            port = null;
            return
        }
    });

    port.pipe(new SerialPort.parsers.Readline())
    console.log("port initialized");
}

if (typeof _ANDROID === "undefined") {
    console.log("android is not defined");

    /*
        sensor = {
            serialRead: function () {
                return "{ 'test': 'some data from serial'}";
            },
            serialWrite: function (data) {
                console.log("writing to serial: " + data);
            }
        }
        */
} else {
    console.log("android is defined, sensor is " + JSON.stringify(sensor));
    console.log("callbacks " + JSON.stringify(android) + " other " + JSON.stringify(processor));
}


function isInBrowser() {
    if (typeof window === 'undefined') {
        return false;
    }
    return true
}


module.exports.read = function (callback) {
    if (isInBrowser()) {
        console.log("read called from inside android webview");
        serialCallback = callback;
    } else {
        console.log("read called from within nodejs");
        port.on("data", function (data) {
            callback(data);
        })
    }
}

module.exports.write = function (data) {
    if (isInBrowser()) {
        sensor.serialWrite(data);
    } else {
        console.log("writing to hw serial");
        port.write(data);
    }
}

module.exports.onDataAvailable = function () {
    dataAvailable();
}



