var webpack = require('webpack');

module.exports = {
  externals: {
    // require("jquery") is external and available
    //  on the global var jQuery
    "serialport": "SerialPort",
    "fs": "fs",
    "./data/result-prototype.json": "result",
    "./data/result.json" : "result"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          "style-loader",
          "css-loader"
        ]
      }
    ]
  },
  resolve: {
    modules: ["./build/node_modules"]
  },
  resolveLoader: {
    modules: ["./build/node_modules"]
  }
};
