// run this file with nodejs for debugging
// $ node sensor.js

// export it with buildscripts/build/sensor.sh or build/project.sh
// and run it on your android device with
// curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip
// make sure the web dev server in the android application is running (see android settings)


try {
    serialDevice = require("./config/port.json")
} catch (e) {
    console.log("please check the readme and create a ./conifg/port.json file with proper contents such as");
    serialDevice = {
        serialport: "/dev/ttyACM0"
    }
    console.log(JSON.stringify(serialDevice));
}

device = require("./lib/protocol-device.js");

app = require("../../build/hal/app.js");
serial = require("../../build/hal/serial.js");
reflectometer = require("../lib-reflectometer/reflectometer.js")

oboe = require('oboe');

run();

async function run() {


    // 2 Nov 2017
    // @greg
    // this is the new function call for accessing prior data

    var m1 = app.getAnswer("measurement_1");
    var m2 = app.getAnswer("measurement_2");
    if (m1 != null) {
        console.log("m1 available");
        try {
            var p = JSON.parse(m1);
            console.log("absorbance for of measurement_1 is: " + p.absorbance_4);
        } catch (e) {
            console.log("error: " + e)
        }
    } else {
        console.log("getting answer measurement_1 returned null");
    }

    try { 
        var info = await device.readJson(); 
        console.log(JSON.stringify(info, null, 2)); 
 
        if (info.device_name == "MultispeQ") { 
            console.log("it's a multispeq") 
            var p = reflectometer.pulse({ 
                pulses: 60, 
                pulse_length: 7, 
                pulse_distance: 1500, 
                brightness: [200, 200, 200, 200, 200, 150, 150, 10, 10, 10], 
                detectors: [0, 0, 0, 0, 0, 0, 0, 1, 1, 1] 
            }); 
        } else { 
            console.log("not a multispeq") 
        } 
    } catch (err) { 
        console.log("there was a problem: " + err); 
        app.result({})
    } 
 
    await sleep(500); 

    var calibration_save = // ADD IN HOW TO DO THIS HERE

    /////////////////////////////////////////////////////////
    // Call protocol here
    serial.write(calibration_save);
//    serial.write(JSON.stringify(testJSON));
    /////////////////////////////////////////////////////////



    var count = 0;
    try {
        var res = await device.readJson("data_raw[*]", function (part) {
            count += 1;
            app.progress(0.1 + (count / 600.0 * 100.0 * .9));
        });

        app.progress(100);
        console.log("received serial data " + JSON.stringify(res));
        app.result(res);
    } catch (err) {
        console.log("there was a problem: " + err);
    }


}

function sleep(duration) {
    return new Promise((resolve) => setTimeout(function () {
        resolve();
    }, duration));
}



