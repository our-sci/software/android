
var $ = require('jquery')
var Plotly = require('plotly.js/lib/core');
var Math = require('mathjs');   // javascript math library
var MathMore = require("./lib/math.js");  // specialty functions library
app = require("../../build/hal/app.js");


// add styles to ../css/style.css, imports will be resolved
require("./css/style.css");


// Create functions for displaying individual values text or numbers
// info(), warning(), and error()

$(".plot").css("margin-bottom", "16px");


function csv(key, value) {
    app.csvExport(key, value)
}

function info(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}


function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}



/////////////////////////////////////////////////////////
// result is set on the android device
// depending on the success of the reading it could still
// be empty, however it will always be defined
if (typeof result == "undefined") {

    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("./data/result.json");
    } catch (err) {
        result = require("./data/result-prototype.json");
    }
}
// 



//////////////// NOTES ABOUT FUNCTIONALITY ///////////////
/*
FIXED! JSON.stringify() You also cannot string values together into a single 'info' over time (as you could with output)... we may need this
Use the toString() function to print an array - like this < info("this output: " + anArray.toString())
FIXED! The div needs to overflow in case things are  really long... is let's make some div options which overflow

What about sending a command, receiving results, sending new command based on results, etc.?
Definitely important for calibration protocols (calibrate+43.23+23432 ... )


////////////////////  CREATE MACRO  //////////////////////
/*
The data is contained in result.sample.  Each protocol is a separate element in the result.sample array
for example, to access the first protocol you ran, use result.sample[0], the second is result.sample[1], etc.
is it possible to prompt the user with a question based

This macro does a few things:
1) it separates out each wavelength based on the # of pulses per wavelength
2) it then subtracts the high 
2) it addresses LED heating by ignoring the first 2/3 of the pulses to reduce standard deviation caused by heating
3) it then tries to flatten the remining 1/3 of pulses
4) then it chooses the median, calculates standard deviation, # of bits for those pulses. 
5a) then saves the raw median to the device for the shiney (0 - 9) or black (10 - 19) values in the calibration version
5b) then it calculates the median, stdev and bits in the normal measurement version
6) note that bits_actual is the bits accounting for the actual range (max reflectance - min reflectance) as compared to bits which assumes the full 16 bit range.  bits_actual is a more accurate representation of the quality of the overall setup (hardware + case + optics)
*/


/////////////////////////////////////////////////////////
// Set up HTML objects (div's) to display macro outputs in HTML
// accessible in the /out folder as processor.html
var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));
// Make easy to remember names for frequently called variables from the json
json = result.sample[0];
data = result.sample[0].data_raw;
/////////////////////////////////////////////////////////

info("later")

/////////////////////////////////////////////////////////
// Make sure the required parameters about the device and firmware are there
// create a "warning", "error" and "date" field in the CSV.  If it's empty, enter 0.  Otherwise, enter warning text
csv("warning", 0)
csv("error", 0)
csv("date",new Date().toISOString())

if (typeof result.device_name == "undefined") {
    var warning_text = "\"device_name\" is expected from the device, but I don't see it in the output JSON.  Check the protocol and ensure it is requested";
    warning(warning_text);    
    csv("warning",warning_text)
}
if (result.device_name.toString() != "Reflectometer") {
    var warning_text = "This script was intended to be used on the reflectometer!";
    warning(warning_text);    
    csv("warning",warning_text)
}
if (typeof result.device_version == "undefined") {
    var warning_text = "\"device_version\" is expected from the device, but I don't see it in the output JSON.  Check the protocol and ensure it is requested";
    warning(warning_text);    
    csv("warning",warning_text)
}
if (typeof result.device_id == "undefined") {
    var warning_text = "\"device_id\" is expected from the device, but I don't see it in the output JSON.  Check the protocol and ensure it is requested";
    warning(warning_text);    
    csv("warning",warning_text)
}
if (typeof result.device_firmware == "undefined") {
    var warning_text = "\"device_firmware\" is expected from the device, but I don't see it in the output JSON.  Check the protocol and ensure it is requested";
    warning(warning_text);    
    csv("warning",warning_text)
}
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// Determine if this is a calibration measurement or not.  
// create toDevice string which will contain the calibration data to be sent back to the device if its a calibration measurement

var toDevice = "";
if (typeof json.calibration == "undefined") {
    var calibration = 0;
}
else if (json.calibration.toString() == "white") {
    calibration = 1;
    toDevice = "set_user_defined+";
    info("calibration: white")
}
else if (json.calibration.toString() == "black") {
    calibration = 2;
    toDevice = "set_user_defined+";
    info("calibration: black")
}

/////////////////////////////////////////////////////////



/*
/////////////////////////////////////////////////////////
// now determine what type of object it is
// flat or solid object
if (json.object_type.toString() == "object") {
    object_type = 1;
}
// bulk solid or liquid in the cuvette
else if (json.object_type.toString() == "cuvette") {
    object_type = 2;
}
// droplet of water
else if (json.object_type.toString() == "droplet") {
    object_type = 3;
}
// TEST
info("object_type: " + json.object_type.toString())
/////////////////////////////////////////////////////////
*/


/////////////////////////////////////////////////////////
// Set the conditions of the measurement - which lights, # pulses, # of pulses to ignore due to heating. etc.
var wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
var pulses = 60;
var pulse_distance = 1.5;
var partPulses = (2 / 3);
var raw_array = [];
var raw_array_flat = [];
var trans_array = [];
var spad_array = [];
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
/* need to add once converting to calibration script
if (calibration === 0) {
    // get calibration information (min and max reflectance) from device

    var ir_high = json.shiney_ir;
    var ir_low = json.black_ir;

    var vis_high = json.shiney_vis;
    var vis_low = json.black_vis;

    var ir_blank = json.blank_ir;
    var vis_blank = json.blank_vis;
}
*/

/////////////////////////////////////////////////////////
// get the sample values from the raw trace, put them into an array
var sample_values_raw = [];
for (var j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = data.slice(pulses * j, pulses * (j + 1));
}
// TEST
// info("sample_values_raw: " + sample_values_raw[0].toString())
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// Then, we choose the last few pulses to use to avoid the heating effect
for (var j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = sample_values_raw[j].slice(pulses * partPulses, pulses);
}
// TEST
// info("sample_values_raw: " + JSON.stringify(sample_values_raw));
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// And finally, we're going to straighten out the pulses to reduce our standard deviation using a linear regression and correction
// first we need a time array to plug into the regression formula (the x of y = mx + b)
var timeArray = [];
for (var z = 0; z < pulses - pulses * partPulses; z++) {
    timeArray[z] = z * pulse_distance;
}
// Then we need to create the new variable to store the flattened values
var sample_values_flat = [];
for (var j = 0; j < wavelengths.length; j++) {
    var tmp_array = [];
    sample_values_flat[j] = tmp_array;
}

// now we can do the straightening via regression + correction
for (var j = 0; j < wavelengths.length; j++) {
    var reg = MathMore.MathLINREG(timeArray, sample_values_raw[j]);
    // what is the center point of rotation for the line (halfway through the array) - that's the value from which we will adjust other values
    var centerPoint = reg.m * (pulses - pulses * partPulses) / 2 + reg.b;
    for (var i = 0; i < sample_values_raw[j].length; i++) {
        var adjustment = centerPoint - (reg.m * timeArray[i] + reg.b);
        sample_values_flat[j][i] = sample_values_raw[j][i] + adjustment;
        // TEST //    output["adjustment_"+wavelengths[j]+"_"+i] = adjustment;
    }
}
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// now convert the data into a percentage (0 - 100) based on the min and max reflectance saved in the device.  (if it's a calibration, don't do that!)
var sample_values_perc = sample_values_raw;
/* not necessary when outputting the percentage (0 - 100) directly from device, for ease of use set sample_value_raw == sample_values_perc
var sample_values_perc = [];
for (var j = 0; j < wavelengths.length; j++) {
    var tmp_array = [];
    sample_values_perc[j] = tmp_array;
}
for (var j = 0; j < wavelengths.length; j++) {
    for (var i = 0; i < sample_values_flat[j].length; i++) {
        if (calibration > 0) {
            sample_values_perc[j][i] = sample_values_flat[j][i];
        }
        else {
            sample_values_perc[j][i] = 100 * (sample_values_flat[j][i] - max_reflectance_Low[j]) / (max_reflectance_High[j] - max_reflectance_Low[j]);
        }
    }
}
*/
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// TEST // Compare flattened, unflattened, and percentage values
//info("sample_values_flat: " + JSON.stringify(sample_values_flat[0]));
//info("sample_values_raw: " + JSON.stringify(sample_values_raw[0]));
//info("sample_values_perc: " + JSON.stringify(sample_values_perc[0]));
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// Now we can pull the median, standard deviation, spad, and bits from these adjusted + corrected values
// Note - to calculate bits, we need to convert the median value back up to a 16 bit value to calculate bits, so there's some math to convert it back to a raw value there
var median = [];
var median_raw = [];
var spad = [];
var absorbance = [];
var stdev = [];
var stdev_raw = [];
var three_stdev = [];
var three_stdev_raw = [];
var bits = [];
var bits_actual = [];

for (var j = 0; j < wavelengths.length; j++) {
    median[j] = MathMore.MathMEDIAN(sample_values_perc[j]);
    median_raw[j] = MathMore.MathMEDIAN(sample_values_flat[j]);
    absorbance[j] = -1 * Math.log(median[j] / 100);
    stdev[j] = MathMore.MathSTDEV(sample_values_perc[j]);
    stdev_raw[j] = MathMore.MathSTDEV(sample_values_flat[j]);
    three_stdev[j] = 3 * stdev[j];
    three_stdev_raw[j] = 3 * stdev_raw[j];
    bits[j] = (15 - MathMore.MathLOG(stdev_raw[j] * 2) / MathMore.MathLOG(2));
    if (calibration === 0) {
//        bits_actual[j] = (15 - MathMore.MathLOG((65536 / max_reflectance[j]) * stdev_raw[j] * 2) / MathMore.MathLOG(2));
    }
}
for (var j = 0; j < wavelengths.length; j++) {
    spad[j] = 100 * Math.log((median[9]) / (median[j]));  // because we've already normalized values from 0 (black) to 100 (shiney), the max value is always 100 so just divide by 100
}
/////////////////////////////////////////////////////////

/*
info(median)
info(median_raw)
info(absorbance)
info(stdev)
info(stdev_raw)
info(three_stdev)
info(three_stdev)
info(bits)
*/

/////////////////////////////////////////////////////////
// Time to check for errors and send warnings if values are too close to max 64435 or zero or too much noise
for (var i = 0; i < median.length; i++) {
    if (median_raw[i] < 100 && calibration != 0) {
        var error_text = "error: LED number " + i + " at wavelength " + wavelengths[i] + "is causing a very low response ( " + median_raw[i] + " ) from the detector.  Something is probably wrong with the protocol or the device";
        error(error_text)
        csv("error", error_text)
    }
    if (median_raw[i] > 64434 && calibration != 0) {
        var error_text = "error: LED number " + i + " at wavelength " + wavelengths[i] + "is causing a very high response ( " + median_raw[i] + " ) from the detector.  Something is wrong with the protocol or the device";
        error(error_text)
        csv("error", error_text)
    }
    if (median[i] < 0 && calibration == 0) {
        var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is causing a very low response ( " + median[i] + " ) from the detector.  This is either a very transparent sample, or something is probably wrong with the protocol or the device";
        error(error_text)
        csv("error", error_text)
    }
    if (median[i] > 100 && calibration == 0) {
        var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is causing a very high response ( " + median[i] + " ) from the detector.  This is either a very shiney sample, or something is wrong with the protocol or the device";
        error(error_text)
        csv("error", error_text)
    }
    if (three_stdev[i] > .15 && calibration == 0) {
        var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is very noisy, with three standard deviations of ( " + three_stdev[i] + " ).  Stabilize the sample, check for sources of noise, or check the equipment for problems."
        warning(error_text)
        csv("warning", error_text)
    }
    if (three_stdev_raw[i] > 70 && calibration == 0) {
        var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is very noisy, with three standard deviations of ( " + three_stdev[i] + " ).  Stabilize the sample, check for sources of noise, or check the equipment for problems."
        warning(error_text)
        csv("warning", error_text)
    }
    /*
    if (bits_actual[i] < 7) {
        var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is very noisy, with actual bits less than 8 ( " + bits[i] + " ).  Stabilize the sample, check for sources of noise, or check the equipment for problems."
        warning(error_text)
        csv("warning", error_text)
    }
    */
}



/////////////////////////////////////////////////////////
// Now we can optionally print graphs...
// for all of the final average values at each wavelength...
var all_outputs = [median, median_raw, absorbance, stdev, stdev_raw, three_stdev, three_stdev_raw, bits] // removed  bits_actual
var all_outputs_names = ["median", "median_raw", "absorbance", "stdev", "stdev_raw", "three_stdev", "three_stdev_raw", "bits"]  // removed "bits_actual"
var all_outputs_cal = [median_raw, stdev_raw, three_stdev_raw, bits]
var all_outputs_cal_names = ["median_raw", "stdev_raw", "three_stdev_raw", "bits"]


/////////////////////////////////////////////////////////
// save values to CSV
var medianList = [];
if (calibration == 0) {
    for (var i = 0; i < wavelengths.length; i++) {
        csv("median_" + i, median[i]);
        medianList.push(MathMore.MathROUND(median[i],3));
    }        
    for (var i = 0; i < wavelengths.length; i++) {
        csv("median_raw_" + i, median_raw[i]);
    }        
}



//info("median" + ": " + medianList.toString())

if (calibration == 0) {
    for (var i = 0; i < wavelengths.length; i++) {
        csv("absorbance_" + i, absorbance[i]);
    }        
}
if (calibration == 0) {
    for (var i = 0; i < wavelengths.length; i++) {
        csv("stdev_" + i, stdev[i]);
//        csv("reflectance_range_" + i, max_reflectance[i]);
//        csv("bits_actual_" + i, bits_actual[i])
    }
}


// DELETE //info("ratios: " + ratios.toString())
//csv("ratios", ratios.toString())


// DELETE
/////////////////////////////////////////////////////////
// graph the ratios of each median value to each other median value
/*
if (calibration == 0) {
    var plot = document.createElement("div")
    plot.setAttribute("class", "plot");
    document.body.appendChild(plot)
    
    var layout = {
        title: "Ratios",
        yaxis: { title: "reflectance" }
    }
    var trace = {
        y: ratios,
        mode: "lines"
    }
    var plotData = [trace]
    Plotly.plot(plot, plotData, layout);    
}
*/


/////////////////////////////////////////////////////////
// graph the raw trace for the entire measurement (detector response by time)...
var fullTime = [];
for (var i = 0; i < data.length; i++) {
    fullTime[i] = i * pulse_distance;
}
var layout = {
    title: "Detector response",
    yaxis: { title: "raw counts (16 bit)" },
    xaxis: { title: "time (milliseconds)" }
}
var trace = {
    x: fullTime,
    y: data,
    mode: "lines"
}
var plotData = [trace]
var plot = document.createElement("div")
plot.setAttribute("class", "plot");
document.body.appendChild(plot)
Plotly.plot(plot, plotData, layout);


/////////////////////////////////////////////////////////
// Finally, graph the transmittance values at each wavelength the raw trace for the entire measurement (detector response by time)...
var layout = {
    title: "Reflectance by Wavelength",
    yaxis: { title: "reflectance (0 - 100)" },
    xaxis: { title: "wavelength" }
}
if (calibration == 0) {
    var trace = {
        x: wavelengths,
        y: median,
        mode: "lines"
    }
}
else {
    var trace = {
        x: wavelengths,
        y: median_raw,
        mode: "lines"
    }    
}
var plotData = [trace]
var plot = document.createElement("div")
plot.setAttribute("class", "plot");
document.body.appendChild(plot)
Plotly.plot(plot, plotData, layout);



/////////////////////////////////////////////////////////
// Finally, graph the standard deviation at each wavelength the raw trace for the entire measurement (detector response by time)...
var layout = {
    title: "Standard Deviation by Wavelength",
    yaxis: { title: "3 Std Deviations" },
    xaxis: { title: "wavelength" }
}
if (calibration == 0) {
        var trace = {
        x: wavelengths,
        y: three_stdev,
        mode: "lines"
    }
}
else {
    var trace = {
        x: wavelengths,
        y: three_stdev_raw,
        mode: "lines"
    }    
}
var plotData = [trace]
var plot = document.createElement("div")
plot.setAttribute("class", "plot");
document.body.appendChild(plot)
Plotly.plot(plot, plotData, layout);


/////////////////////////////////////////////////////////
// REALLY IMPORTANT!  save the resulting information to csv
save();
