// run this file with nodejs for debugging
// $ node sensor.js

// export it with buildscripts/build/sensor.sh or build/project.sh
// and run it on your android device with
// curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip
// make sure the web dev server in the android application is running (see android settings)


try {
    serialDevice = require("./config/port.json")
} catch (e) {
    console.log("please check the readme and create a ./conifg/port.json file with proper contents such as");
    serialDevice = {
        serialport: "/dev/ttyACM0"
    }
    console.log(JSON.stringify(serialDevice));
}

device = require("./lib/protocol-device.js");

app = require("../../build/hal/app.js");
serial = require("../../build/hal/serial.js");

oboe = require('oboe');


run();
var progress = 0
setTimeout(function () {
    progress += 10;
    app.progress(progress);
}, 110);

async function run() {
    await sleep(500);
    serial.write("healloa!\n");
    await sleep(500);

    try {
        console.log("device is " + device);
        var info = await device.readJson();
        console.log("done reading" + JSON.stringify(info, null, 2));
        app.result(info);
    } catch (err) {
        console.log("there was a problem: " + JSON.stringify(err, null, 2));
        app.result("[{}]");
        return;
    }
}

function sleep(duration) {
    return new Promise((resolve) => setTimeout(function () {
        resolve();
    }, duration));
}



