const fs = require('fs');
const prompt = require('prompt');
const path = require('path');
const http = require('http');

var baseDir = path.join(__dirname, "../");
var zip = path.basename(baseDir) + ".zip";
var zip_file = path.join(baseDir, "dist", zip);

var ip;
var prop_file = path.join(path.resolve(__dirname), "./.defaults.json");


if (!fs.existsSync(zip_file)) {
    console.log("file not found: " + zip_file);
    return;
}


try {
    ip = require(prop_file).ip_address;
} catch (e) {
    console.log(e);
    ip = "192.168.1.2";
}


console.log("open our-scikit android application, go to settings and enable \"Webserver\"");

prompt.start();
prompt.get({
    properties: {
        ip_address: {
            name: "enter IP address displayed",
            default: ip
        },
    }
}, function (err, result) {
    console.log('Command-line input received:');
    console.log('  connecting to ip: ' + result.ip_address);

    fs.writeFile(prop_file, JSON.stringify(result), function (err) {
        if (err) {
            return console.log("unable to save file: " + err);
        }
        upload(result.ip_address);        
    });

});

function upload(ip) {
    var len =  fs.statSync(zip_file).size;
    var req = http.request({
        host: ip,
        port: '9095',
        path: '/upload_measurement',
        method: 'POST',
        headers: {
            'Content-Type': 'application/binary',
            'Content-Length': len
        }
    }, function (res) {

    });

    req.write(fs.readFileSync(zip_file));
    req.end();
}