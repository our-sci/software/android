const path = require('path');
const webpack = require('webpack');
const fs = require('fs-cli');


module.exports.pack = function (done) {
    webpack({
            entry: path.resolve(__dirname, "../src/sensor.js"),
            output: {
                path: target.build,
                filename: target.filename
            },
            externals: {
                "serialport": "SerialPort",
                "fs": "fs"
            },
            module: {
                rules: [{
                        test: /\.css$/,
                        use: [
                            "style-loader",
                            "css-loader"
                        ],
                    },
                    {
                        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                        use: [
                            "url-loader?limit=10000&mimetype=application/font-woff"
                        ]
                    },
                    {
                        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                        use: [
                            'file-loader'
                        ]
                    }
                ]
            }
        },
        (err, stats) => {
            if (err || stats.hasErrors()) {
                console.log(stats);
            } else {
                fs.mv(path.join(target.build, target.filename), path.join(target.dist, target.filename));
                console.log("success, sensor script: " + path.join(target.dist, target.filename));
                done();
            }
            // Done processing
        }
    )
};


filename = "sensor.js";


target = {
    filename: filename,
    dist: path.resolve(__dirname, "../dist"),
    build: path.resolve(__dirname, "../build")
};



module.exports.pack(function(){
    console.log("sensor packed");
});