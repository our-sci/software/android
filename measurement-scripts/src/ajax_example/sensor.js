app = require("../../build/hal/app.js");
http = require('http');

http.get("http://app.nexus-computing.com/oursci/data.json", (resp) => {
    var data = '';    

    resp.on('data', (chunk) => {
        data += chunk;
    });

    resp.on('end', () => {
        console.log(data);
        app.result(data)
    });
}).on("error", (err) => {
    console.log("Error: " + err.message);
    app.result("error")
});

var r = {
    "status": "success"
}

