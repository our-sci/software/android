app = require("../../build/hal/app.js");

var $ = require('jquery')
var Plotly = require('plotly.js/lib/core');

require("./css/style.css");


csv("temp", "18 C");
csv("hum", "12 pct");
save();

var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));

function csv(key, value) {
    app.csvExport(key, value);
}

function save(){
    app.save()
}

function info(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}


function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");

    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}


if (typeof result == "undefined") {
    result = {
        "status" : "success"
    };
}




info("result is: "+result);
console.log("result is "+result);
