// run this file with nodejs for debugging
// $ node sensor.js

// export it with buildscripts/build/sensor.sh or build/project.sh
// and run it on your android device with
// curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip
// make sure the web dev server in the android application is running (see android settings)

serialDevice = require("./config/port.json")
device = require("./lib/protocol-device.js");

app = require("../../build/hal/app.js");
serial = require("../../build/hal/serial.js");

oboe = require('oboe');
math = require("mathjs");



run();

async function run() {

    var p = pulse({
        pulses: 60,
        pulse_length: 7,
        pulse_distance: 1501
    });

    serial.write("1000+\n");

    try {
        var res = await device.readLine();
        console.log("received newline terminated data\n" + res);
    } catch (err) {
        console.log("there was a problem: " + err);
    }

    await sleep(500);

    serial.write(JSON.stringify(p));

    var count = 0;
    try {
        var res = await device.readJson("data_raw[*]", function (part) {
            count += 1;
            app.progress(count / 600.0 * 100.0);
        });

        console.log("received serial data " + JSON.stringify(res));
        app.progress(100);
        app.result(res);
    } catch (err) {
        console.log("there was a problem: " + err);
    }


}

function sleep(duration) {
    return new Promise((resolve) => setTimeout(function () {
        resolve();
    }, duration));
}


function pulse(options) {

    if (options == null) {
        options = {};
    }

    var args = {
        pulses: options.pulses || 60,
        pulse_length: options.pulse_length | 7,
        pulse_distance: options.pulse_distance || 1500,
        order: options.order || [3, 4, 5, 9, 6, 7, 8, 1, 2, 10],
        brightness: options.brightness || [700, 700, 700, 700, 700, 650, 650, 55, 55, 55],
        detectors: options.detectors || [1, 1, 1, 1, 1, 1, 1, 3, 3, 3],
        dac_lights: options.dac_lights || 1,
        averages: options.averages || 5,
        protocols: options.protocols || 1,
        recall: options.recall || [
            "userdef[0]",
            "userdef[1]",
            "userdef[2]",
            "userdef[3]",
            "userdef[4]",
            "userdef[5]",
            "userdef[6]",
            "userdef[7]",
            "userdef[8]",
            "userdef[9]",
            "userdef[10]",
            "userdef[11]",
            "userdef[12]",
            "userdef[13]",
            "userdef[14]",
            "userdef[15]",
            "userdef[16]",
            "userdef[17]",
            "userdef[18]",
            "userdef[19]"
        ]
    }


    var proto = {
        pulses: [],
        pulse_length: [],
        pulse_distance: [],
        pulsed_lights: [],
        pulsed_lights_brightness: [],
        detectors: [],
        dac_lights: 1,
        averages: 5,
        protocols: 1,
        recall: []
    }

    for (var i = 0; i < 10; i++) {
        proto.pulses.push(args.pulses);
        proto.pulse_length.push([args.pulse_length]);
        proto.pulse_distance.push(args.pulse_distance);
        proto.pulsed_lights.push([args.order[i]])
        proto.pulsed_lights_brightness.push([args.brightness[i]])
        proto.detectors.push([args.detectors[i]])
    }

    proto.dac_lights = args.dac_lights
    proto.averages = args.averages
    proto.protocols = args.protocols
    proto.recall = args.recall

    console.log(JSON.stringify([proto], null, 2));

    return [proto];
}