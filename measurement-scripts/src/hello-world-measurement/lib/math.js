//------------------------------------------------------------------------------
// Predefined functions to be used in macros
//------------------------------------------------------------------------------

// Calculate SUM: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathSUM(values) {
    var sum = false;
    if (values) {
        for (var i = 0, len = values.length; i < len; i++)
            sum += values[i];
    }
    return parseFloat(sum);
}

// Round Number: Input-> Value (float) and number of decimal spaces (2 are standard)
//------------------------------------------------------------------------------
function MathROUND(value, digets) {
    digets = typeof digets !== 'undefined' ? digets : 2;
    var val = false;

    if (value == 0) {
        return 0;
    }
    if (value && parseInt(digets) >= 0) {
        val = Math.round(value * Math.pow(10, digets)) / Math.pow(10, digets);
    }
    return parseFloat(val);
}

// Calculate mean: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathMEAN(values) {
    var mean = false;
    if (values) {
        var sum = MathSUM(values);
        mean = sum / values.length;
    }
    return parseFloat(mean);
}

// Calculate Standard Deviation: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathSTDEV(values) {
    var stdev = 0;
    if (values) {
        if (values.length > 2) {
            var mean = MathMEAN(values, false);
            var tmp = [];
            for (var i = 0, len = values.length; i < len; i++)
                tmp.push(Math.pow((values[i] - mean), 2));
            stdev = Math.sqrt(MathSUM(tmp) / values.length);
        }
    }
    return parseFloat(stdev);
}

// Calculate Standard Deviation Sample: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathSTDEVS(values) {
    var stdevs = false;
    if (values) {
        if (values.length > 2) {
            var mean = MathMEAN(values, false);
            var tmp = [];
            for (var i = 0, len = values.length; i < len; i++)
                tmp.push(Math.pow((values[i] - mean), 2));
            stdevs = Math.sqrt(MathSUM(tmp) / (values.length - 1));
        }
    }
    return parseFloat(stdevs);
}

// Calculate Standard Error: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathSTDERR(values) {
    var stderr = false;
    if (values) {
        if (values.length > 2) {
            stderr = MathSTDEV(values) / Math.sqrt(values.length);
        }
    }
    return parseFloat(stderr);
}

// Calculate Variance: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathVARIANCE(values) {
    if (values) {
        var mean = MathMEAN(values);
        var variance = false;
        for (i = 0; i < values.length; i++) {
            variance += Math.pow((values[i] - mean), 2)
        }

        variance = (1 / (values.length - 1)) * variance

    }
    return parseFloat(variance);
}

// Get Maximum: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathMAX(values) {
    var max = false;
    if (values) {
        for (i = 0; i < values.length; i++) {
            if (!max)
                max = values[i];
            else if (values[i] > max)
                max = values[i];
        }
    }
    return parseFloat(max);
}

// Get Minimum: Input-> Array with values (float or int)
//------------------------------------------------------------------------------
function MathMIN(values) {
    var min = false;
    if (values) {
        for (i = 0; i < values.length; i++) {
            if (!min)
                min = values[i];
            else if (values[i] < min)
                min = values[i];
        }
    }
    return parseFloat(min);
}

// LogBase10 Input-> Value (float)
//------------------------------------------------------------------------------
function MathLOG(value) {
    var val = false;
    if (value) {
        val = Math.log(value) / Math.LN10;
    }
    return parseFloat(val);
}

// Natural log Input-> Value (float)
//------------------------------------------------------------------------------
function MathLN(value) {
    var val = false;
    if (value) {
        val = Math.log(value);
    }
    return parseFloat(val);
}

// Median Input-> array (float)
//------------------------------------------------------------------------------
function MathMEDIAN(values) {
    var val = false;
    if (values) {
        // Sort values
        values.sort(function (a, b) {
            if (a < b) //sort string ascending
                return -1;
            if (a > b) return 1;
            return 0; //default return value (no sorting)
        });

        var n = values.length;
        // Even
        if ((n % 2 == 0)) {
            val = (values[(n / 2) - 1] + values[(n / 2)]) / 2;
        }
        // Odd
        if ((Math.abs(n) % 2 == 1)) {
            val = values[Math.floor((n / 2))];
        }
    }
    return parseFloat(val);
}

// Calculate Standard Error: Input-> Array with values (float or int)
//------------------------------------------------------------------------------------------------------------------
function MathLINREG(x, y) {
    var regression = false;

    // calculate number of points
    var xn = x.length;
    var yn = y.length;

    if (xn == yn) {
        // Calculate Sums
        var xSum = MathSUM(x);
        var ySum = MathSUM(y);

        var xxSum = 0;
        var xySum = 0;
        var yySum = 0;

        for (var i = 0; i < xn; i++) {
            xySum += (x[i] * y[i]);
            xxSum += (x[i] * x[i]);
            yySum += (y[i] * y[i]);
        }

        // calculate slope
        var m = ((xn * xySum) - (xSum * ySum)) / ((xn * xxSum) - (xSum * xSum));

        // calculate intercept
        var b = (ySum - (m * xSum)) / xn;

        // calculate r
        var r = (xySum - ((1 / xn) * xSum * ySum)) / Math.sqrt(((xxSum) - ((1 / xn) * (Math.pow(xSum, 2)))) * ((yySum) - ((1 / xn) * (Math.pow(ySum, 2)))));

        regression = { 'm': m, 'b': b, 'r': r, 'r2': (r * r) }
    }
    return regression;
}

//Multiple Linear Regression: Input -> Array with values [predictor 1, predictor 2, ...etc, response] (float or int)
//------------------------------------------------------------------------------------------------------------------
//source: numericjs
function MathMULTREG(input_raw) {

    //do some basic transforms on the data to get it to useable form
    var numPredictors = input_raw[0].length - 1;
    var input = [];

    for (var i = 0; i < input_raw[0].length; i++) {
        var temp = [];
        for (var j = 0; j < input_raw.length; j++) {
            temp.push(input_raw[j][i]);
        }
        input.push(temp);
    }


    var x = [];
    var ones = [];
    for (i = 0; i < input[0].length; i++) {
        ones.push(1);
    }

    x.push(ones);
    var retX = [];
    for (i = 0; i < numPredictors; i++) {
        x.push(input[i]);
        retX.push(input[i]);
    }

    var y = input[numPredictors];

    //do the actual fit
    x = transpose(x);

    var qr = QRDecomp(x);

    var results = dot(inv(qr.R), dot(transpose(qr.Q), y));

    //calculate the rsquared value
    var sum = 0;
    for (i = 0; i < y.length; i++) {
        sum += y[i];
    }

    var responseAvg = sum / y.length;

    var yHat = [];

    for (i = 0; i < y.length; i++) {
        var yTemp = 0;
        for (j = 0; j < numPredictors; j++) {
            yTemp += results[j + 1] * x[i][j + 1];
        }
        yTemp += results[0];
        yHat.push(yTemp);
    }

    var SSM = 0;
    var SSTO = 0;

    for (i = 0; i < y.length; i++) {
        SSM += ((yHat[i] - responseAvg) * (yHat[i] - responseAvg));
        SSTO += ((y[i] - responseAvg) * (y[i] - responseAvg));
    }

    var rSq = SSM / SSTO;

    return { "rsquared": rSq, "slopes": results, "points": [retX, yHat] };

}

//Fit to Y = Y0 + Ae^(-x/t), where A and t are the fitted variables, takes an array of floats or ints
//Format of input should be [predictor, response]
//------------------------------------------------------------------------------------------------------------------
function MathEXPINVREG(input_raw) {

    //calculate the approximate asymptote
    var y = [];
    for (i = 0; i < input_raw.length; i++) {
        y.push(input_raw[i][1]);
    }

    //trapezoidal riemann sum assuming spaced evenly
    var riemann = 0;
    var riemannSq = 0;
    for (i = 0; i < y.length - 1; i++) {
        temp = (y[i] + y[i + 1]) / 2;
        riemann += temp;
        temp = (Math.pow(y[i], 2) + Math.pow(y[i + 1], 2)) / 2;
        riemannSq += temp;
    }

    var asymptote = (riemannSq - riemann * (y[0] + y[y.length - 1]) / 2)
        / (riemann - y.length * (y[0] + y[y.length - 1]) / 2);

    //calculate with linear regression on the linear equation ln(Y) = ln(A) - x/t
    var input_transformed = clone(input_raw);
    for (var i = 0; i < input_raw.length; i++) {
        temp = input_raw[i][1] - asymptote;
        if (temp < 0) {
            temp = -1 * temp;
        }
        input_transformed[i][1] = MathLN(temp);
    }



    var constants = [0.5];

    var t2 = 50;
    var t2_old;
    var t, A;
    for (i = 0; i < 10; i++) {

        if (t2 < 2) {
            t = -999999;
            A = 0;
            break;
        }

        t2_old = t2;
        var linReg = MathMULTREG(input_transformed.slice(0, t2));

        t2 = MathROUND((-1 / linReg.slopes[1]) * constants[0], 0);
        if (i == 9) {
            t2 = (t2 > t2_old) ? t2 : t2_old;
            linReg = MathMULTREG(input_transformed.slice(0, t2));
        }

        t = linReg.slopes[1];
        A = Math.pow(Math.E, linReg.slopes[0]);

    }



    var points = [];
    for (i = 0; i < input_raw.length; i++) {
        var temp = [];
        temp.push(input_raw[i][0]);
        temp.push(A * Math.pow(Math.E, input_raw[i][0] * t) + asymptote);
        points.push(temp);
    }

    var yError = 0;
    for (i = 0; i < input_raw.length; i++) {
        yError += Math.pow(A * Math.pow(Math.E, input_raw[i][0] * t) - input_raw[i][1] + asymptote, 2);
    }

    yError /= input_raw.length - 1;

    var lifetime = (-1 / t);
    var slope = -1 * A * t;

    return { points: points, results: [A, t], error: yError, asymptote: asymptote, rsquared: linReg.rsquared, lifetime: lifetime, slope: slope };
}

//Polynomial fit - fit to y = a0 + a1x + a2x^2 + a3x^3....
//Format of input_raw is [predictor, response], degree must be an int
//------------------------------------------------------------------------------------------------------------------
function MathPOLYREG(input_raw, degree) {
    var transformed_nums = [];
    for (var i = 0; i < input_raw.length; i++) {
        var temp = [];
        for (var j = 1; j < degree + 1; j++) {
            temp.push(Math.pow(input_raw[i][0], j))
        }
        temp.push(input_raw[i][1]);
        transformed_nums.push(temp);
    }

    var polyReg = MathMULTREG(transformed_nums);

    var slopes = polyReg.slopes;

    var points = [];
    var yError = 0;
    for (i = 0; i < input_raw.length; i++) {
        temp = [];
        var yHat = 0;
        temp.push(input_raw[i][0]);
        for (j = 0; j < degree + 1; j++) {
            yHat += (Math.pow(input_raw[i][0], degree) * slopes[j]);
        }
        temp.push(yHat);
        points.push(temp);

        yError += Math.pow((yHat - input_raw[i][1]), 2);
    }

    yError /= input_raw.length - 1;

    return { "points": points, "slopes": slopes, "error": yError };

}


//Calculate the angular difference between the device and the sun given roll, pitch, compass, azimuth, altitude
//------------------------------------------------------------------------------------------------------------------
function calcSunAngle(roll, pitch, compass, azimuth, altitude) {
    var cos = Math.cos;
    var sin = Math.sin;
    var pi = Math.PI;
    var acos = Math.acos;

    roll *= pi / 180;
    pitch *= pi / 180;
    compass *= pi / 180;
    azimuth *= pi / 180;
    altitude *= pi / 180;

    //rotate by roll and pitch
    var deviceVector = [cos(roll) * sin(pitch), -1 * sin(roll), cos(roll) * cos(pitch)];

    //rotate by compass
    deviceVector[0] = cos(compass) * deviceVector[0] - sin(compass) * deviceVector[1];
    deviceVector[1] = sin(compass) * deviceVector[0] - cos(compass) * deviceVector[1];

    var solarVector = [cos(azimuth) * cos(altitude), sin(azimuth), -1 * cos(azimuth) * sin(altitude)];

    var percent = solarVector[0] * deviceVector[0] + solarVector[1] * deviceVector[1] + solarVector[2] * deviceVector[2];

    return { "angle": acos(percent) * 180 / pi, "percent": percent };
}
/*
//Nonlinear Least Squares for all fits
//------------------------------------------------------------------------------------------------------------------
function MathNLS(){

	//set up stuff, first iteration
	var errorMax = 10e-6;
	var errorCurrent = 1;
	var r = [];
	var j = [];
	var beta = [A, t];

	for(i = 0; i < input_raw.length; i++){
		r.push(input_raw[i][1] - A * Math.pow(Math.E, -1 * input_raw[i][0] * t));

		var temp = [];
		temp.push(Math.pow(Math.E, -1 * input_raw[i][0] * t));
		temp.push(Math.pow(Math.E, -1 * input_raw[i][0] * t) * input_raw[i][0] * A * -1);
		j.push(temp);
	}

	//iterative bit utilizing the Gauss-Newton method - if calculation times become exceptionally long consider
	//switching this algorithm to Levenberg-Marquardt method
	while(errorCurrent > errorMax){

		var betaNew = subVV(beta, dot(inv(dot(transpose(j), j)), dot(transpose(j), r)));
		var betaError = dot(inv(dot(transpose(j), j)), dot(transpose(j), r));
		errorCurrent = Math.abs((betaError[0] + betaError[1]) / 2);
		console.log(errorCurrent);
		beta = betaNew;
		A = beta[0];
		t = beta[1];

		r = [];
		j = [];

		for(i = 0; i < input_raw.length; i++){
			r.push(input_raw[i][1] - A * Math.pow(Math.E, -1 * input_raw[i][0] * t));

			temp = [];
			temp.push(Math.pow(Math.E, -1 * input_raw[i][0] * t));
			temp.push(Math.pow(Math.E, -1 * input_raw[i][0] * t) * input_raw[i][0] * A * -1);
			j.push(temp);
		}
	}
}*/

//intended for vectors of equal size
function subVV(vec1, vec2) {
    var ret = [];
    for (var i = 0; i < vec1.length; i++) {
        ret.push(vec1[i] - vec2[i]);
    }

    return ret;
}

function proj(vec1, vec2) {
    var denom = innerProd(vec1, vec1);
    var numer = innerProd(vec1, vec2);

    var vec3 = [];

    for (var i = 0; i < vec1.length; i++) {
        vec3[i] = (numer / denom) * vec1[i];
    }

    return vec3;
}
function innerProd(vec1, vec2) {
    if (vec1.length == vec2.length) {
        var ans = 0;
        for (var i = 0; i < vec1.length; i++) {
            ans += vec1[i] * vec2[i];
        }

        return ans;
    }
}
function normal(vec) {
    var norm = 0;
    for (var i = 0; i < vec.length; i++) {
        norm += vec[i] * vec[i];
    }
    norm = Math.sqrt(norm);

    return norm;
}

//source: http://www.learninglover.com/examples.php?id=79
function QRDecomp(A) {
    var aVectors = transpose(A);
    var uVector = [];
    var eVector = [];
    var eVectorTxt = [];
    var sum = [];

    var testTest = [];

    for (var i = 0; i < aVectors.length; i++) {
        uVector[i] = [];
        for (var j = 0; j < aVectors[i].length; j++) {
            sum[j] = 0;
        }

        for (j = 0; j < i; j++) {
            var temp = proj(eVector[j], aVectors[i]);
            for (k = 0; k < temp.length; k++) {
                sum[k] += temp[k];
            }
        }

        for (j = 0; j < aVectors[i].length; j++) {
            uVector[i][j] = aVectors[i][j] - sum[j];
        }

        var norm = normal(uVector[i]);

        eVector[i] = [];
        eVectorTxt[i] = [];

        for (j = 0; j < aVectors[i].length; j++) {
            eVector[i][j] = uVector[i][j] / norm;
            eVectorTxt[i][j] = uVector[i][j] + " / " + norm;
        }


    }

    for (i = 0; i < aVectors.length; i++) {
        testTest[i] = [];
        for (j = 0; j < aVectors[i].length; j++) {
            testTest[i][j] = 0;
        }

        for (j = 0; j <= i; j++) {
            var tempVec = innerProd(aVectors[i], eVector[j]);

            for (var k = 0; k < eVector[i].length; k++) {
                testTest[i][k] += eVector[j][k] * tempVec;
            }
        }


    }

    eVector = transpose(eVector);
    eVectorTxt = transpose(eVectorTxt);
    var decomp = {};
    decomp.Q = [];

    for (i = 0; i < eVector.length; i++) {
        decomp.Q[i] = eVectorTxt[i];
    }

    for (i = 0; i < eVector.length; i++) {
        decomp.Q[i] = eVector[i];
    }

    decomp.R = dot(transpose(decomp.Q), A);

    return decomp;
}


//helper functions from numeric.js library
function transpose(x) {
    var i, j, m = x.length, n = x[0].length, ret = new Array(n), A0, A1, Bj;
    for (j = 0; j < n; j++) ret[j] = new Array(m);
    for (i = m - 1; i >= 1; i -= 2) {
        A1 = x[i];
        A0 = x[i - 1];
        for (j = n - 1; j >= 1; --j) {
            Bj = ret[j]; Bj[i] = A1[j]; Bj[i - 1] = A0[j];
            --j;
            Bj = ret[j]; Bj[i] = A1[j]; Bj[i - 1] = A0[j];
        }
        if (j === 0) {
            Bj = ret[0]; Bj[i] = A1[0]; Bj[i - 1] = A0[0];
        }
    }
    if (i === 0) {
        A0 = x[0];
        for (j = n - 1; j >= 1; --j) {
            ret[j][0] = A0[j];
            --j;
            ret[j][0] = A0[j];
        }
        if (j === 0) { ret[0][0] = A0[0]; }
    }
    return ret;
}
function _getCol(A, j, x) {
    var n = A.length, i;
    for (i = n - 1; i > 0; --i) {
        x[i] = A[i][j];
        --i;
        x[i] = A[i][j];
    }
    if (i === 0) x[0] = A[0][j];
}
function dotVV(x, y) {
    var i, n = x.length, i1, ret = x[n - 1] * y[n - 1];
    for (i = n - 2; i >= 1; i -= 2) {
        i1 = i - 1;
        ret += x[i] * y[i] + x[i1] * y[i1];
    }
    if (i === 0) { ret += x[0] * y[0]; }
    return ret;
}

function dotMMbig(x, y) {
    var gc = _getCol, p = y.length, v = new Array(p);
    var m = x.length, n = y[0].length, A = new Array(m), xj;
    var VV = dotVV;
    var i, j;
    --p;
    --m;
    for (i = m; i !== -1; --i) A[i] = new Array(n);
    --n;

    for (i = n; i !== -1; --i) {
        gc(y, i, v);
        for (j = m; j !== -1; --j) {
            xj = x[j];
            A[j][i] = VV(xj, v);
        }
    }

    return A;
}

function _dim(x) {
    var ret = [];
    while (typeof x === "object") { ret.push(x.length); x = x[0]; }
    return ret;
}

function dim(x) {
    var y, z;
    if (typeof x === "object") {
        y = x[0];
        if (typeof y === "object") {
            z = y[0];
            if (typeof z === "object") {
                return _dim(x);
            }
            return [x.length, y.length];
        }
        return [x.length];
    }
    return [];
}

function clone(input) {
    var ret = [];
    for (var i = 0; i < input.length; i++) {
        var temp = [];
        for (var j = 0; j < input[0].length; j++) {
            temp.push(input[i][j]);
        }
        ret.push(temp);
    }

    return ret;
}
function diag(d) {
    var i, i1, j, n = d.length, A = new Array(n), Ai;
    for (i = n - 1; i >= 0; i--) {
        Ai = new Array(n);
        i1 = i + 2;
        for (j = n - 1; j >= i1; j -= 2) {
            Ai[j] = 0;
            Ai[j - 1] = 0;
        }
        if (j > i) { Ai[j] = 0; }
        Ai[i] = d[i];
        for (j = i - 1; j >= 1; j -= 2) {
            Ai[j] = 0;
            Ai[j - 1] = 0;
        }
        if (j === 0) { Ai[0] = 0; }
        A[i] = Ai;
    }
    return A;
}

function rep(s, v, k) {
    if (typeof k === "undefined") { k = 0; }
    var n = s[k], ret = new Array(n), i;
    if (k === s.length - 1) {
        for (i = n - 2; i >= 0; i -= 2) { ret[i + 1] = v; ret[i] = v; }
        if (i === -1) { ret[0] = v; }
        return ret;
    }
    for (i = n - 1; i >= 0; i--) { ret[i] = rep(s, v, k + 1); }
    return ret;
}

function identity(n) { return diag(rep([n], 1)); }

function inv(x) {
    var s = dim(x), abs = Math.abs, m = s[0], n = s[1];
    var A = clone(x), Ai, Aj;
    var I = identity(m), Ii, Ij;
    var i, j, k;
    for (j = 0; j < n; ++j) {
        var i0 = -1;
        var v0 = -1;
        for (i = j; i !== m; ++i) { k = abs(A[i][j]); if (k > v0) { i0 = i; v0 = k; } }
        Aj = A[i0]; A[i0] = A[j]; A[j] = Aj;
        Ij = I[i0]; I[i0] = I[j]; I[j] = Ij;
        x = Aj[j];
        for (k = j; k !== n; ++k)    Aj[k] /= x;
        for (k = n - 1; k !== -1; --k) Ij[k] /= x;
        for (i = m - 1; i !== -1; --i) {
            if (i !== j) {
                Ai = A[i];
                Ii = I[i];
                x = Ai[j];
                for (k = j + 1; k !== n; ++k)  Ai[k] -= Aj[k] * x;
                for (k = n - 1; k > 0; --k) { Ii[k] -= Ij[k] * x; --k; Ii[k] -= Ij[k] * x; }
                if (k === 0) Ii[0] -= Ij[0] * x;
            }
        }
    }
    return I;
}

function dotMV(x, y) {
    var p = x.length, i;
    var ret = new Array(p);
    var VV = dotVV;
    for (i = p - 1; i >= 0; i--) { ret[i] = VV(x[i], y); }
    return ret;
}

function dotVM(x, y) {
    var j, k, p, q, ret, woo, i0;
    p = x.length; q = y[0].length;
    ret = new Array(q);
    for (k = q - 1; k >= 0; k--) {
        woo = x[p - 1] * y[p - 1][k];
        for (j = p - 2; j >= 1; j -= 2) {
            i0 = j - 1;
            woo += x[j] * y[j][k] + x[i0] * y[i0][k];
        }
        if (j === 0) { woo += x[0] * y[0][k]; }
        ret[k] = woo;
    }
    return ret;
}

function dotMMsmall(x, y) {
    var i, j, k, p, q, r, ret, foo, bar, woo, i0;
    p = x.length; q = y.length; r = y[0].length;
    ret = new Array(p);
    for (i = p - 1; i >= 0; i--) {
        foo = new Array(r);
        bar = x[i];
        for (k = r - 1; k >= 0; k--) {
            woo = bar[q - 1] * y[q - 1][k];
            for (j = q - 2; j >= 1; j -= 2) {
                i0 = j - 1;
                woo += bar[j] * y[j][k] + bar[i0] * y[i0][k];
            }
            if (j === 0) { woo += bar[0] * y[0][k]; }
            foo[k] = woo;
        }
        ret[i] = foo;
    }
    return ret;
}

function mulVS(x, y) {
    for (var i = 0; i < x.length; i++) {
        x[i] = x[i] * y;
    }

    return x;
}

function mulSV(x, y) {
    for (var i = 0; i < y.length; i++) {
        y[i] = y[i] * x;
    }

    return y;
}

function dot(x, y) {
    var d = dim;
    switch (d(x).length * 1000 + d(y).length) {
        case 2002:
            if (y.length < 10) return dotMMsmall(x, y);
            else return dotMMbig(x, y);
        case 2001: return dotMV(x, y);
        case 1002: return dotVM(x, y);
        case 1001: return dotVV(x, y);
        case 1000: return mulVS(x, y);
        case 1: return mulSV(x, y);
        case 0: return x * y;
        default: throw new Error('numeric.dot only works on vectors and matrices');
    }
}

// Non-linear fitting 
//------------------------------------------------------------------------------
function NonLinearRegression(data, options) {

    // initial settings
    var presets = {
        iterations: (options.iterations === undefined || options.iterations > 200) ? 200 : options.iterations,
        cPts: data.length || 0,             // Number of datapoints
        cVar: options.variables || 1, 		// Number of independant variables
        cPar: options.initial.length || 0, 		// Number of parameters
        equation: options.equation || '',      // Function
        RelaxF: options.RelaxF || 1.0, 		// 0.5, 0.2, 0.1, 0.05
        SEy: options.SEy || 1, 			    //[1, 'Y','Sqrt(Y)','w','Rep'],
        yTrans: 'Y',		                //['Y', 'LN(Y)','LN(Y)', 'SQRT(Y)', '1/Y'],
        x1Trans: 'X', 		                //['X', 'LN(X)', 'SQRT(X)', '1/X'],
        x2Trans: 'X2',                      //['X2', 'LN(X2)', 'SQRT(X2)', '1/X2'],
        x3Trans: 'X3',                      //['X3', 'LN(Y)', 'SQRT(Y)', '1/X3'],
        x4Trans: 'X4',                      //['X4', 'LN(X4)', 'SQRT(X4)', '1/X4'],
        x5Trans: 'X5',                      //['X5', 'LN(X5)', 'SQRT(X5)', '1/X5'],
        x6Trans: 'X6',                      //['X6', 'LN(X6)', 'SQRT(X6)', '1/X6'],
        x7Trans: 'X7',                      //['X7', 'LN(X7)', 'SQRT(X7)', '1/X7'],
        x8Trans: 'X8',                      //['X8', 'LN(X8)', 'SQRT(X8)', '1/X8'],
        Centered: options.Centered || false,
        LeastAbs: options.LeastAbs || false,
        cPctile: options.Percentile || 50   // Percentile
    }

    // initial parameters
    var parameters = {
        ca: options.initial[0] || 0,
        cb: options.initial[1] || 0,
        cc: options.initial[2] || 0,
        cd: options.initial[3] || 0,
        ce: options.initial[4] || 0,
        cf: options.initial[5] || 0,
        cg: options.initial[6] || 0,
        ch: options.initial[7] || 0,
        sa: 1.0, sb: 1.0, sc: 1.0, sd: 1.0, se: 1.0, sf: 1.0, sg: 1.0, sh: 1.0,
        pva: 0, pvb: 0, pvc: 0, pvd: 0, pve: 0, pvf: 0, pvg: 0, pvh: 0
    }

    // All functins and parameters
    function STUDT(r, t) { r = Math.abs(r); var n = r / Math.sqrt(t), a = Math.atan(n); if (1 == t) return 1 - a / (Math.PI / 2); var e = Math.sin(a), u = Math.cos(a); return t % 2 == 1 ? 1 - (a + e * u * STATCOM(u * u, 2, t - 3, -1)) / (Math.PI / 2) : 1 - e * STATCOM(u * u, 1, t - 3, -1) } function STATCOM(r, t, n, a) { for (var e = 1, u = e, c = t; c <= n;)e = e * r * c / (c - a), u += e, c += 2; return u } function ASTUDT(r, t) { for (var n = .5, a = .5, e = 0; a > 1e-15;)e = 1 / n - 1, a /= 2, STUDT(e, t) > r ? n -= a : n += a; return e } function MAX(r, t) { return r > t ? r : t } function MIN(r, t) { return r < t ? r : t } function Fmt(r) { var t; return Math.abs(r) < 5e-5 && (r = 0), t = r >= 0 ? " " + (r + 5e-5) : " " + (r - 5e-5), t = t.substring(0, t.indexOf(".") + 5), t.substring(t.length - 10, t.length) } function vFmt(r) { var t; return Math.abs(r) < 5e-7 && (r = 0), t = r >= 0 ? " " + (r + 5e-7) : " " + (r - 5e-7), t = t.substring(0, t.indexOf(".") + 7), t.substring(t.length - 18, t.length) } function ix(r, t) { return r * (presets.cPar + 1) + t } var Par = [0, 0, 0, 0, 0, 0, 0, 0], SEP = [1, 1, 1, 1, 1, 1, 1, 1], Der = [0, 0, 0, 0, 0, 0, 0, 0, 0], Arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], Cov = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], xArr = [0, 0, 0, 0, 0, 0, 0, 0, 0], i = 0, j = 0, k = 0, l = 0, m = 0, cPar = 0, cVar = 0, cPts = 0, ccSW = 1, ccAv = 0, ccSD = 1;

    // Iterating function
    function Iterate(r, a, P) { var r = r, t = 0, c = 0, x = 0; dgfr = a.cPts - a.cPar; var s = ASTUDT(.05, dgfr), A = a.cPctile / 100, e = P.ca; Par[0] = e; var S = P.cb; Par[1] = S; var T = P.cc; Par[2] = T; var n = P.cd; Par[3] = n; var o = P.ce; Par[4] = o; var M = P.cf; Par[5] = M; var f = P.cg; Par[6] = f; var h = P.ch; Par[7] = h; var E = "Y = " + a.equation + "\n"; for (i = 1; i <= a.cVar; i++)E += " x" + i + " "; E += " Y yc Y-yc SEest YcLo YcHi \n"; var m = 0; for (b = 0; b < a.cPar * (a.cPar + 1); b++)Arr[b] = 0; for (i = 1; i <= a.cPts; i++) { for (b = 0; b < a.cVar; b++)xArr[b] = r[i - 1][0], v = r[i - 1][1]; var g = xArr[0]; "LN(X2)" == a.x1Trans && (g = Math.log(xArr[0])), "SQRT(X2)" == a.x2Trans && (g = Math.sqrt(xArr[0])), "1/X2" == a.x2Trans && (g = 1 / xArr[0]); var q = g, D = xArr[1]; "LN(X2)" == a.x2Trans && (D = Math.log(xArr[1])), "SQRT(X2)" == a.x2Trans && (D = Math.sqrt(xArr[1])), "1/X2" == a.x2Trans && (D = 1 / xArr[1]); var X = xArr[2]; "LN(X3)" == a.x3Trans && (X = Math.log(xArr[2])), "SQRT(X3)" == a.x3Trans && (X = Math.sqrt(xArr[2])), "1/X3" == a.x3Trans && (X = 1 / xArr[2]); var F = xArr[3]; "LN(X4)" == a.x4Trans && (F = Math.log(xArr[3])), "SQRT(X4)" == a.x4Trans && (F = Math.sqrt(xArr[3])), "1/X4" == a.x4Trans && (F = 1 / xArr[3]); var d = xArr[4]; "LN(X5)" == a.x5Trans && (d = Math.log(xArr[4])), "SQRT(X5)" == a.x5Trans && (d = Math.sqrt(xArr[4])), "1/X5" == a.x5Trans && (d = 1 / xArr[4]); var p = xArr[5]; "LN(X6)" == a.x6Trans && (p = Math.log(xArr[5])), "SQRT(X6)" == a.x6Trans && (p = Math.sqrt(xArr[5])), "1/X6" == a.x6Trans && (p = 1 / xArr[5]); var R = xArr[6]; "LN(X7)" == a.x7Trans && (R = Math.log(xArr[6])), "SQRT(X7)" == a.x7Trans && (R = Math.sqrt(xArr[6])), "1/X7" == a.x7Trans && (R = 1 / xArr[6]); var C = xArr[7]; "LN(X8)" == a.x8Trans && (C = Math.log(xArr[7])), "SQRT(X8)" == a.x8Trans && (C = Math.sqrt(xArr[7])), "1/X8" == a.x8Trans && (C = 1 / xArr[7]); var l = Equation(a.equation, e, S, T, n, o, M, f, h, g, q); if (Y = r[i - 1][1], "Rep" == a.SEy) { var y = 1, L = Y, I = Y * Y; y += 1, L += Y, I += Y * Y, Y = L / y; var N = Math.sqrt(Math.abs(I / y - Y * Y) / y) } yo = Y; var U = 1; "w" == a.SEy && (U = v), "Rep" == a.SEy && (U = N), "Sqrt(Y)" == a.SEy && (U = Math.sqrt(Y)), 0 == U && (U = .001); var u = a.yTrans.toUpperCase(), Q = Y; "LN(Y)" == u && (Q = Math.log(Y)), "LN(SQRT)" == u && (Q = Math.sqrt(Y)), "1/Y" == u && (Q = 1 / Y), "LN(Y)" == u && (U /= Y), "SQRT(Y)" == u && (U /= Q), "1/Y" == u && (U = U / Y * Y), Y = Q, a.LeastAbs && (U *= Y < l ? MAX(Math.sqrt(A * Math.abs(Y - l)), .001 * Y) : MAX(Math.sqrt((1 - A) * Math.abs(Y - l)), .001 * Y)), t += 1 / (U * U), c += l / (U * U), x += (Y - ccAv) * (Y - ccAv) / (U * U); for (var b = 0; b < a.cPar; b++) { var W = Par[b]; if (0 == Par[b]) var B = 1e-4; else var B = Par[b] / 1e3; Par[b] += B, e = Par[0], S = Par[1], T = Par[2], n = Par[3], o = Par[4], M = Par[5], f = Par[6], h = Par[7], ycIncr = Equation(a.equation, e, S, T, n, o, M, f, h, g, q), Der[b] = (ycIncr - l) / (B * U), a.Centered && (Par[b] -= B, e = Par[0], S = Par[1], T = Par[2], n = Par[3], o = Par[4], M = Par[5], f = Par[6], h = Par[7], ycDecr = Equation(a.equation, e, S, T, n, o, M, f, h, g, q), Der[b] = (ycIncr - ycDecr) / (2 * B * U)), Par[b] = W, e = Par[0], S = Par[1], T = Par[2], n = Par[3], o = Par[4], M = Par[5], f = Par[6], h = Par[7] } for (Der[a.cPar] = (Y - l) / U, m += Der[a.cPar] * Der[a.cPar], b = 0; b < a.cPar; b++)for (k = 0; k <= a.cPar; k++)Arr[ix(b, k)] = Arr[ix(b, k)] + Der[b] * Der[k]; var V = 0; for (b = 0; b < a.cPar; b++)for (V += Cov[ix(b, b)] * Der[b] * Der[b], k = b + 1; k < a.cPar; k++)V += 2 * Cov[ix(b, k)] * Der[b] * Der[k]; V = U * Math.sqrt(V); var _ = l, w = l - s * V, H = l + s * V; "LN(Y)" == u && (_ = Math.exp(l), w = Math.exp(w), H = Math.exp(H)), "SQRT(Y)" == u && (_ = l * l, w *= w, H *= H), "1/Y" == u && (_ = 1 / l, w = 1 / w, H = 1 / H), E += Fmt(yo) + Fmt(_) + Fmt(yo - _) + Fmt(V) + Fmt(w) + Fmt(H) + "\n" } ccSW = t, ccAv = c / ccSW, ccSD = x / ccSW; var j = (ccSD - m / ccSW) / ccSD, z = Math.sqrt(j); for (E += "\nCorr. Coeff. = " + vFmt(z) + "; r*r = " + vFmt(j), RMS = Math.sqrt(m / MAX(1, dgfr)), E += "\nRMS Error = " + vFmt(RMS) + "; d.f = " + dgfr + "; SSq = " + vFmt(m) + "\n", AIC = a.cPts * Math.log(m / a.cPts) + 2 * (a.cPar + 1), a.cPts >= a.cPar + 2 ? AICc = AIC + 2 * (a.cPar + 1) * (a.cPar + 1 + 1) / (a.cPts - (a.cPar + 1) - 1) : AICc = AIC, E += "AIC = " + vFmt(AIC) + "; AIC(corrected) = " + vFmt(AICc) + "\n", i = 0; i < a.cPar; i++) { var G = Arr[ix(i, i)]; for (Arr[ix(i, i)] = 1, k = 0; k <= a.cPar; k++)Arr[ix(i, k)] = Arr[ix(i, k)] / G; for (b = 0; b < a.cPar; b++)if (i != b) for (G = Arr[ix(b, i)], Arr[ix(b, i)] = 0, k = 0; k <= a.cPar; k++)Arr[ix(b, k)] = Arr[ix(b, k)] - G * Arr[ix(i, k)] } var J = "\nParameter Estimates...\n", K = []; for (i = 0; i < a.cPar; i++)Par[i] = Par[i] + a.RelaxF * Arr[ix(i, a.cPar)], SEP[i] = RMS * Math.sqrt(Arr[ix(i, i)]), J += "p" + (i + 1) + "=" + vFmt(Par[i]) + " +/- " + vFmt(SEP[i]) + "; p=" + Fmt(STUDT(Par[i] / SEP[i], dgfr)) + "\n", K.push({ name: "P" + (i + 1), value: Par[i], sd_error: SEP[i], p: STUDT(Par[i] / SEP[i], dgfr) }); var O = "\nCovariance Matrix Terms and Error-Correlations...\n"; for (b = 0; b < a.cPar; b++)for (k = b; k < a.cPar; k++)Cov[ix(b, k)] = Arr[ix(b, k)] * RMS * RMS, v = Arr[ix(b, k)] / Math.sqrt(Arr[ix(b, b)] * Arr[ix(k, k)]), O += "B(" + (b + 1) + "," + (k + 1) + ")=", b != k && (O += "B(" + (k + 1) + "," + (b + 1) + ")="), O += " ", O += Cov[ix(b, k)], O += "; r=" + v + "\n"; return { text: E, ParameterEstimates: J, CovarianceMatrix: O, par: { ca: Par[0], sa: SEP[0], pva: vFmt(STUDT(Par[0] / SEP[0], dgfr)), cb: Par[1], sb: SEP[1], pvb: vFmt(STUDT(Par[1] / SEP[1], dgfr)), cc: Par[2], sc: SEP[2], pvc: vFmt(STUDT(Par[2] / SEP[2], dgfr)), cd: Par[3], sd: SEP[3], pvd: vFmt(STUDT(Par[3] / SEP[3], dgfr)), ce: Par[4], se: SEP[4], pve: vFmt(STUDT(Par[4] / SEP[4], dgfr)), cf: Par[5], sf: SEP[5], pvf: vFmt(STUDT(Par[5] / SEP[5], dgfr)), cg: Par[6], sg: SEP[6], pvg: vFmt(STUDT(Par[6] / SEP[6], dgfr)), ch: Par[7], sh: SEP[7], pvh: vFmt(STUDT(Par[7] / SEP[7], dgfr)) }, parameters: K, RMS_error: RMS, presets: presets } }

    // Function library
    function Equation(eqtn, a, b, c, d, e, f, g, h, x, t) {
        if (eqtn == 'b + a * e(- x / c)')
            return b + a * Math.exp(- x / c)
        if (eqtn == '( a - c ) * e( - b * t ) + c')
            return (a - c) * Math.exp(- b * t) + c
        return null;
    }

    // iteration loop
    var output = {};
    var convergence;
    for (it = 0; it < presets.iterations; it++) {
        output = Iterate(data, presets, parameters);
        parameters = output.par;
        if (convergence <= output.RMS_error && it > 1)
            break;
        convergence = output.RMS_error;
    }
    output.iterations = it
    delete output.par
    return output;
}

// Adding user defined info message
//------------------------------------------------------------------------------
function info(msg, output) {
    if (output['messages'] === undefined)
        output['messages'] = {}
    if (output['messages']['info'] === undefined)
        output['messages']['info'] = []
    output['messages']['info'].push(msg)
}

// Adding user defined warning message
//------------------------------------------------------------------------------
function warning(msg, output) {
    if (output['messages'] === undefined)
        output['messages'] = {}
    if (output['messages']['warning'] === undefined)
        output['messages']['warning'] = []
    output['messages']['warning'].push(msg)
}

// Adding user defined error message
//------------------------------------------------------------------------------
function danger(msg, output) {
    if (output['messages'] === undefined)
        output['messages'] = {}
    if (output['messages']['danger'] === undefined)
        output['messages']['danger'] = []
    output['messages']['danger'].push(msg)
}


module.exports.MathSUM = MathSUM
module.exports.MathROUND = MathROUND
module.exports.MathMEAN = MathMEAN
module.exports.MathSTDEV = MathSTDEV
module.exports.MathSTDEVS = MathSTDEVS
module.exports.MathSTDERR = MathSTDERR
module.exports.MathVARIANCE = MathVARIANCE
module.exports.MathMAX = MathMAX
module.exports.MathMIN = MathMIN
module.exports.MathLOG = MathLOG
module.exports.MathLN = MathLN
module.exports.MathMEDIAN = MathMEDIAN
module.exports.MathLINREG = MathLINREG
module.exports.MathMULTREG = MathMULTREG
module.exports.MathEXPINVREG = MathEXPINVREG
module.exports.MathPOLYREG = MathPOLYREG
module.exports.calcSunAngle = calcSunAngle
module.exports.subVV = subVV
module.exports.proj = proj
module.exports.innerProd = innerProd
module.exports.normal = normal
module.exports.QRDecomp = QRDecomp
module.exports.transpose = transpose
module.exports._getCol = _getCol
module.exports.dotVV = dotVV
module.exports.dotMMbig = dotMMbig
module.exports._dim = _dim
module.exports.dim = dim
module.exports.clone = clone
module.exports.diag = diag
module.exports.rep = rep
module.exports.identity = identity
module.exports.inv = inv
module.exports.dotMV = dotMV
module.exports.dotVM = dotVM
module.exports.dotMMsmall = dotMMsmall
module.exports.mulVS = mulVS
module.exports.mulSV = mulSV
module.exports.dot = dot
module.exports.NonLinearRegression = NonLinearRegression