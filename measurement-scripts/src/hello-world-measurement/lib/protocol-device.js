serialDevice = require("../config/port.json")


app = require("../../../build/hal/app.js");
serial = require("../../../build/hal/serial.js");

oboe = require('oboe');
math = require("mathjs");
const EventEmitter = require('events');

var streamParser = null;
hwSerial = false;
singleLineEmitter = null;

try {
    serial.read(readCallback);
    hwSerial = true;
} catch (e) {
    console.log("unable to read from serial, " + hwSerial);
}


function readCallback(data) {
    if (streamParser != null) {
        streamParser.emit('data', data + "");
    }

    if (singleLineEmitter != null) {
        singleLineEmitter.emit("data", data + "");
    }
}


function serialReadLine(eventCallback) {
    singleLineEmitter = new EventEmitter();

    if (!hwSerial) {
        var mock = "this is just some mock data\n";
        console.log("emitting mock data: " + mock);
        singleLineEmitter.emit('data', mock);
    }

    return new Promise((resolve, reject) => {
        singleLineEmitter.on("data", function (data) {
            singleLineEmitter = null;
            resolve(data);
        });
    });

}


function serialJson(nodeSpec, eventCallback) {

    streamParser = oboe();

    if (nodeSpec != null && eventCallback != null) {
        console.log("listening for node " + nodeSpec);
        streamParser.node(nodeSpec, function (data) {
            eventCallback(data);
        })
    }

    if (!hwSerial) {
        var d = require("../data/result-prototype.json");
        console.log("emitting mock data: " + mock);
        streamParser.emit('data', mock);
    }


    console.log("returning promise");
    return new Promise((resolve, reject) => {
        streamParser.on("done", function (json) {
            console.log("done");
            console.log(JSON.stringify(json) + "\n\n");
            streamParser.abort();
            streamParser = null;
            resolve(json);
        });

        streamParser.on("fail", function (error) {
            console.log("fail");
            console.log("error\n" + JSON.stringify(error));
            streamParser = null;
            reject(error);

        })
    });
}



module.exports = {
    readJson: function (nodespec, cb) {
        return serialJson(nodespec, cb);
    },
    readLine: serialReadLine
}
