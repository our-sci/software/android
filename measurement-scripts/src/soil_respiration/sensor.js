// run this file with nodejs for debugging
// $ node sensor.js

// export it with buildscripts/build/sensor.sh or build/project.sh
// and run it on your android device with
// curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip
// make sure the web dev server in the android application is running (see android settings)


try {
    serialDevice = require("./config/port.json")
} catch (e) {
    console.log("please check the readme and create a ./conifg/port.json file with proper contents such as");
    serialDevice = {
        serialport: "/dev/ttyACM0"
    }
    console.log(JSON.stringify(serialDevice));
}

device = require("./lib/protocol-device.js");

app = require("../../build/hal/app.js");
serial = require("../../build/hal/serial.js");
reflectometer = require("../lib-reflectometer/reflectometer.js")

oboe = require('oboe');

run();

async function run() {


    // 2 Nov 2017
    // @greg
    // this is the new function call for accessing prior data
    // demoing this on "Carrot Sample Survey 1: Using frozen, shredded samples from freezer"
/*
    var m1 = app.getAnswer("measurement_1");
    if (m1 != null) {
        console.log("m1 available");
        try {
            var p = JSON.parse(m1);
            console.log("absorbance for of measurement_1 is: " + p.absorbance_4);
        } catch (e) {
            console.log("error: " + e)
        }
    } else {
        console.log("getting answer measurement_1 returned null");
    }
*/

    // uncomment for serial terminal mode (use ctrl c to exit and restart)
    /*
//    serial.write("configure_bluetooth+Reflectometer+115200+");
// object (flat or 3D.  Examples: leaves, apples)
 serial.write("set_user_defined+0+57994.35+1+57943.88+2+57939.05+3+57924.32+4+57929.68+5+56291.62+6+51706.41+7+52037.84+8+52253.84+9+52369.36+-1+");
 serial.write("set_user_defined+10+14249.52+11+14234+12+14236.56+13+14229.86+14+14226.13+15+13628.27+16+12205.15+17+10045.43+18+10304.78+19+10143.34+-1+");
// cuvette (liquid or solid.  Examples: dirt, water)
 serial.write("set_user_defined+20+56673.85+21+56541.86+22+56512.28+23+56857.52+24+59326.27+25+51086.92+26+58215.29+27+57896.67+28+57572.85+29+58064.09+-1+");
 serial.write("set_user_defined+30+26396.91+31+26340.26+32+26318.46+33+26401.77+34+27021.13+35+21599.73+36+22313.27+37+18850.65+38+19225.4+39+18971.43+-1++");
// droplet (Examples: plant extract for Brix)
 serial.write("set_user_defined+40+22916.29+41+22869.35+42+22858.46+43+22861.04+44+22999.74+45+21799.08+46+23714.94+47+38783.22+48+38143.6+49+39120.15+-1+");
 serial.write("set_user_defined+50+18116.52+51+18073.59+52+18060.88+53+18059.81+54+18123.1+55+15420.07+56+15196.68+57+15591.41+58+15784.68+59+15725.22+-1+");
 serial.write("print_memory+");
 
 for(;;){
     try {
         var res = await device.readLine();
         console.log(res);
     } catch (err) {
         console.log("there was a problem: " + err);
     }
 }
*/

    /////////////////////////////////////////////////////////
    // Create protocols here
    // Standard protocols for object, cuvette, and droplet object types
    var soil_c_mineralization = [{"_protocol_set_":[{"environmental":[["co2"]],"protocols":24,"protocols_delay":5000}]}] 
    
    /////////////////////////////////////////////////////////
    // Call protocol here
    serial.write(JSON.stringify(soil_c_mineralization));
    //    serial.write(JSON.stringify(testJSON));
    /////////////////////////////////////////////////////////

    var count = 0;
    try {   
        var res = await device.readJson("data_raw[*]", function (part) {
            count += 1;
            app.progress(0.1 + (count / 600.0 * 100.0 * .9));
        });

        app.progress(100);
        console.log("received serial data " + JSON.stringify(res));
        app.result(res);
    } catch (err) {
        console.log("there was a problem: " + err);
    }


}

function sleep(duration) {
    return new Promise((resolve) => setTimeout(function () {
        resolve();
    }, duration));
}



