## Basic Features

- [ ] add mark to send to survey
- [ ] show most recent version of form
- [ ] searchbar to find survey
- [ ] lock menu bar when uploading surveys
- [ ] show done surveys at the top of the active list
- [ ] needs review screen for single survey
- [ ] run now & submit uploads even if there is an error
