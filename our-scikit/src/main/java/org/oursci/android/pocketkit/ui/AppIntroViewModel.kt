package org.oursci.android.soilsci.ui

import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.ui.intro.BaseIntroViewModel
import org.oursci.android.common.vo.IntroSlide
import org.oursci.android.pocketkit.App
import org.oursci.android.pocketkit.R

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */

class AppIntroViewModel(app: App) : BaseIntroViewModel(app) {

    val loginLiveData = ConsumableLiveData<(Boolean) -> Unit>()

    var loginSlide = IntroSlide(R.drawable.ic_scikit_white
            , "Connect", "In order to store measurements use an existing Social Network account " +
            "or Sign up to the SoilSci and OurSci network", "Log in or Sign up", {

        loginLiveData.value = {
            success ->
            if (success) {
                val v = slides.value
                v?.let {
                    v[3] = loginSuccessSlide()
                    slides.value = v
                }
            }
        }
    })

    fun loginSuccessSlide() =
            IntroSlide(R.drawable.ic_scikit_white,
                    "Greetings ${app.user.username}", "you are successfully Logged in, continue to get started.")


    init {
        val slideArray = arrayOf(
                IntroSlide(R.drawable.ic_scikit_white, "Our Scikit",
                        "Welcome to the Our Scikit!\n\n" +
                                "This Intro will show you the basics you need to know for getting started."),
                IntroSlide(R.drawable.ic_carrot
                        , "Research at its finest", "Our Scikit brings research to your hands and lets you participate at a global scale.\n\n" +
                        "Agriculture, Soil, Food, Health can be all made better with your help!"),
                IntroSlide(R.drawable.user
                        , "Collecting as Collective", "Our Soilkit brings research back to where it belongs, the community." +
                        "\n\n" +
                        "You help others research and others help you"),
                if (loggedIn()) loginSuccessSlide() else loginSlide,
                IntroSlide(R.drawable.ic_scikit_white
                        , "Supercharge the App", "Mark your data with Geo Location information for later evaluation.\n\n" +
                        "The App will ask you for permission improve your data."))

        slides.value = slideArray
    }

    private fun loggedIn(): Boolean {
        return !app.user.username.isEmpty()
    }
}