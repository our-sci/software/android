package org.oursci.android.pocketkit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.BaseNavigationManager
import org.oursci.android.soilsci.ui.AppIntroViewModel

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
class App : BaseApplication() {
    override val filesDirName = "our_scikit"


    override val WEB_SERVER_PORT = 9095
    override val navigationManager: BaseNavigationManager = NavigationManager(R.id.content_main)
    override val TAG = "Scikit"

    override val viewModelFactory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AppIntroViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return AppIntroViewModel(this@App) as T
            }

            return super@App.createViewModel(modelClass)
        }
    }



}


