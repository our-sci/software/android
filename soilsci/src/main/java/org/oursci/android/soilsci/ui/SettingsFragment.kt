package org.oursci.android.soilsci.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v14.preference.SwitchPreference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.app
import org.oursci.android.common.booleanPref
import org.oursci.android.common.ipAddresses
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.server.WebServer
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseSettingsFragment
import org.oursci.android.soilsci.BuildConfig
import org.oursci.android.soilsci.MainActivity
import org.oursci.android.soilsci.R
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 28.07.17.
 */

class SettingsFragment : BaseSettingsFragment() {

    fun toast(text: String) {
        activity?.runOnUiThread {
            Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("ApplySharedPref")
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)


        val versionCode = BuildConfig.VERSION_CODE
        val versionName = BuildConfig.VERSION_NAME

//        findPreference(getString(R.string.update_measurement_scripts)).setOnPreferenceClickListener {
//
//            toast("Downloading survey and scripts")
//            thread {
//                SurveyManager.of(app).downloadSurvey("http://app.nexus-computing.com/oursci/bfa.xml")
//                toast("downloaded survey")
//                MeasurementLoader.of(app).downloadMeasurement("http://app.nexus-computing.com/oursci/reflectometer_object.zip")
//                toast("downloaded script for object")
//                MeasurementLoader.of(app).downloadMeasurement("http://app.nexus-computing.com/oursci/reflectometer_droplet.zip")
//                toast("downloaded script for droplet")
//                MeasurementLoader.of(app).downloadMeasurement("http://app.nexus-computing.com/oursci/reflectometer_cuvette.zip")
//                toast("downloaded script for cuvette, done")
//            }
//            true
//        }

        findPreference(getString(R.string.check_update)).setOnPreferenceClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://app.nexus-computing.com/oursci/" +
                    "version.php?application=BFA&version=$versionCode")
            activity?.startActivity(intent)
            true
        }

        findPreference("version").summary = "$versionName (code $versionCode)"

        findPreference("timestamp").summary =
                DateTime(BuildConfig.TIMESTAMP).toString("yyyy-MM-dd HH:mm:ss")

        findPreference("intro")?.setOnPreferenceClickListener {

            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                    .putBoolean(MainActivity.KEY_INTRO_DONE, false).commit()
            (activity as MainActivity).showIntro()
            true
        }

        findPreference(resources.getString(R.string.refresh_media_cache))?.setOnPreferenceClickListener {
            (activity as BaseActivity).showClearMediaDialog()
            true
        }

        findPreference(resources.getString(R.string.enable_webserver))?.setOnPreferenceChangeListener { _, value ->
            if (value as Boolean) {
                try {
                    WebServer.of(app()).start()
                    updateWebserverSummary(true)
                } catch (e: Exception) {
                    Timber.e(e)
                }
            } else {
                try {
                    WebServer.of(app()).stop()
                    updateWebserverSummary(false)
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            true
        }

        updateWebserverSummary(app().booleanPref(R.string.enable_webserver))


    }

    private fun updateWebserverSummary(enabled: Boolean) {
        val stringToDisplay = if (enabled) {
            resources.getString(R.string.webserver_enabled_summary,
                    app().WEB_SERVER_PORT, app().ipAddresses().joinToString(),
                    app().ipAddresses().joinToString(), app().WEB_SERVER_PORT)
        } else {
            resources.getString(R.string.webserver_disabled_summary)
        }

        findPreference(resources.getString(R.string.enable_webserver)).summary = stringToDisplay
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? BaseActivity)?.lockMenus()
    }
}