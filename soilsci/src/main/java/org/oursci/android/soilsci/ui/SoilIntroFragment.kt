package org.oursci.android.soilsci.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.ui.intro.IntroFragment

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.07.17.
 */
class SoilIntroFragment : IntroFragment() {

    override val viewModel: SoilIntroViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(SoilIntroViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.loginLiveData.observe(this, ConsumableObserver {
            callback, consumed ->
            if(consumed){
                return@ConsumableObserver
            }

            callback?.let {
                activity.login {
                    success ->
                    it(success)
                }
            }
        })
    }

}