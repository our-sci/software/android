package org.oursci.android.soilsci

import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.ui.intro.IntroFragment
import org.oursci.android.common.ui.profile.ProfileFragment
import org.oursci.android.soilsci.databinding.NavHeaderMainBinding
import timber.log.Timber


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    override val defaultSubtitle: String = "Our Soilkit"


    companion object {
        val KEY_INTRO_DONE = "intro_done"
    }

    override val loginLogo: Int = R.drawable.bfa
    override val alertDialogTheme = R.style.AppThemeDialog
    override val loginTheme: Int? = R.style.FirebaseUIThemeBFA

    override val drawerLayout: DrawerLayout by lazy {
        drawer_layout
    }

    override val toolbar: Toolbar by lazy {
        findViewById<Toolbar>(R.id.toolbar)
    }

    override var openDrawerContentDesc = R.string.navigation_drawer_open
    override var closeDrawerContentDesc = R.string.navigation_drawer_close


    lateinit var navigationManager: NavigationManager
    lateinit var navHeaderBinding: NavHeaderMainBinding


    override val contentView = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        navigationManager = NavigationManager.from(app)
        navHeaderBinding = NavHeaderMainBinding.bind(nav_view.getHeaderView(0))

        navHeaderBinding.imageView.setOnClickListener {
            if (navigationManager.currentFragment() is ProfileFragment) {
                return@setOnClickListener
            }

            if (!app.user.loggedIn()) {
                login { success ->
                    Timber.d("login success: %s", success)

                    if (success) {
                        navHeaderBinding.user = app.user
                        navHeaderBinding.invalidateAll()
                    }
                }
                return@setOnClickListener
            }

            drawer_layout.closeDrawer(GravityCompat.START)
            navigationManager.profile()
        }

        fab_button.setOnClickListener { view ->
            view.visibility = if (view.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            // navigationManager.deviceInfo()
        }



        if (savedInstanceState == null) {

            val introDone = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(KEY_INTRO_DONE, false)

            // add to back stack for sure
            navigationManager.intro(false)

            if (!introDone && navigationManager.currentFragment() !is IntroFragment) {
                Timber.d("reinstatiating intro fragment")
            } else {
                navigationManager.main()
            }
        }


        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.subtitle = getString(R.string.app_sub_title)

        lockMenus()

        updateNavUser()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        // no call to super
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (super.onOptionsItemSelected(item)) {
            return true
        }

        when (item?.itemId) {
            R.id.action_settings -> {
                navigationManager.settings()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.list_surveys -> {
                navigationManager.navigateToSurveyList()
            }
            R.id.list_survey_results -> {
                navigationManager.navigateToSurveyResultsList(true)
            }
            R.id.list_measurement_scripts -> {
                navigationManager.navigateToMeasurementScriptsList()
            }
            R.id.list_measurement_results -> {
                navigationManager.navigateToMeasurementResultsList()
            }
            R.id.browse -> {
                navigationManager.navigateToSurveyBrowser()
            }
        }

        FirebaseAuth.getInstance().currentUser?.getIdToken(false)

        FirebaseAuth.getInstance().addIdTokenListener { firebaseAuth ->
            firebaseAuth.currentUser
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    override fun introDone() {
        supportActionBar?.show()
        unlockMenus()
        updateNavUser()

        PreferenceManager.getDefaultSharedPreferences(this)
                .edit().putBoolean(KEY_INTRO_DONE, true).apply()

        Handler().postDelayed({
            NavigationManager.from(app).main()
        }, 200)
    }


    fun updateNavUser() {
        navHeaderBinding.user = app.user
        navHeaderBinding.invalidateAll()
    }


    override fun onLoggedOut() {
        updateNavUser()
    }


    fun showIntro(): Unit {
        navigationManager.intro(true)
    }

    override fun isInIntro(): Boolean = true

}
